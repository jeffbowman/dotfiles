(define-module (herbstluftwm)
  #:export (hc add attr command keybind mousebind range rule set set-attr))

(define hc
  (lambda (command)
    (let ((cmd (string-append "herbstclient " command)))
      (display (format #f "~a~%" cmd))
      (system cmd))))

(define command
  (lambda (action . params)
    (string-append action " "
                   (if (list? params)
                       (string-join params " ")
                       (string-join (list params) " ")))))

(define keybind
  (lambda (key action)
    (command "keybind" key action)))

(define mousebind
  (lambda (key action)
    (command "mousebind" key action)))

(define attr
  (lambda (key value)
    (command "attr" key value)))

(define set
  (lambda (key value)
    (command "set" key value)))

(define rule
  (lambda (key . params)
    (command "rule" key (string-join params " "))))

(define add
  (lambda (param)
    (command "add" param)))

(define set-attr
  (lambda (key param)
    (command "set_attr" key param)))

(define range
  (lambda (start finish)
    (let ((lst '()))
      (do ((i start (1+ i)))
          ((> i finish))
        (set! lst (cons i lst)))
      (reverse lst))))
