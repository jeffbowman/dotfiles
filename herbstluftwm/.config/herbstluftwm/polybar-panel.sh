#!/usr/bin/env bash

hc() {
    herbstclient "$@"
}

for m in $(polybar --list-monitors | cut -d: -f1); do
    MONITOR=$m polybar --reload 2>&1 | tee /tmp/polybar_$m.log & 
done

# panel=polybar
# for monitor in $(hc list_monitors | cut -d: -f1) ; do
#     # start it on each monitor
#     "$panel" "$monitor" &
# done
