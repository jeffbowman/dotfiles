#!/usr/bin/env bash

# this is a simple config for herbstluftwm

hc() {
    herbstclient "$@"
}

hc emit_hook reload

xsetroot -solid '#5A8E3A'
$HOME/.fehbg
xmodmap $HOME/.xmodmap.swapcaps
autorandr home

hc set auto_detect_monitors on
hc set swap_monitors_to_get_tag off

# remove all existing keybindings
hc keyunbind --all

# keybindings
# if you have a super key you will be much happier with Mod set to Mod4
#Mod=Mod1    # Use alt as the main modifier
Mod=Mod4   # Use the super key as the main modifier

hc keybind $Mod-Shift-q quit
hc keybind $Mod-Shift-r reload
hc keybind $Mod-Shift-c close
hc keybind $Mod-Return spawn "${TERMINAL:-kitty}" # use your $TERMINAL
                                                  # with xterm as
                                                  # fallback

# basic movement in tiling and floating mode
# focusing clients
hc keybind $Mod-Left  focus left
hc keybind $Mod-Down  focus down
hc keybind $Mod-Up    focus up
hc keybind $Mod-Right focus right
hc keybind $Mod-h     focus left
hc keybind $Mod-j     focus down
hc keybind $Mod-k     focus up
hc keybind $Mod-l     focus right

# moving clients in tiling and floating mode, arrow left/right to
# shift to other monitors
hc keybind $Mod-Shift-Left \
   chain , lock , shfit_to_monitor -d , focus_monitor -d , unlock
hc keybind $Mod-Shift-Right \
   chain , lock , shfit_to_monitor -r , focus_monitor -r , unlock
hc keybind $Mod-Shift-h     shift left
hc keybind $Mod-Shift-j     shift down
hc keybind $Mod-Shift-k     shift up
hc keybind $Mod-Shift-l     shift right

hc keybind $Mod-Control-k  cycle_monitor

# splitting frames
# create an empty frame at the specified direction
hc keybind $Mod-u       split   bottom  0.5
hc keybind $Mod-o       split   right   0.5
# let the current frame explode into subframes
hc keybind $Mod-Control-space split explode

# resizing frames and floating clients
resizestep=0.02
hc keybind $Mod-Control-h       resize left +$resizestep
hc keybind $Mod-Control-j       resize down +$resizestep
hc keybind $Mod-Control-k       resize up +$resizestep
hc keybind $Mod-Control-l       resize right +$resizestep
hc keybind $Mod-Control-Left    resize left +$resizestep
hc keybind $Mod-Control-Down    resize down +$resizestep
hc keybind $Mod-Control-Up      resize up +$resizestep
hc keybind $Mod-Control-Right   resize right +$resizestep

# tags
mon1_names=( Main Web Emacs Term 5 )         # tag names for monitor 1
mon2_names=( Comms Web Emacs 4 5 )           # tag names for monitor 2
fallback_names=( {1..5} )       # tag names for all other monitors
tag_keys=( {1..9} 0)

hc rename default "${mon1_names[0]}" || true
for tag in "${mon1_names[@]}" "${mon2_names[@]}" "${fallback_names[@]}"; do
    hc try silent add "$tag"
done

for i in ${!tag_keys[@]}; do
    key="${tag_keys[$i]}"
    # the keybinding works as follows: the or executes the commands separated by
    # CASE and stops executing them after the first of those succeeds.
    # the first command is: check that we are on monitor 3 and can switch to tag "${mon1_names[$i]}"
    # if only one of these two steps fail, try the second one (and so one).
    # The setup for two monitors is as follows:
    hc keybind "$Mod-$key" \
       or CASE and . compare monitors.focus.index = 0 . use "${mon1_names[$i]}" \
       CASE and . compare monitors.focus.index = 1 . use "${mon2_names[$i]}" \
       CASE use "${fallback_names[$i]}"
    hc keybind "$Mod-Shift-$key" \
       or CASE and . compare monitors.focus.index = 0 . move "${mon1_names[$i]}" \
       CASE and . compare monitors.focus.index = 1 . move "${mon2_names[$i]}" \
       CASE move "${fallback_names[$i]}"
done

# cycle through tags
# add additional information in order to cycle only through a monitors tags
# and not through all tags
define-tag-cycle() {
    local n=$#
    local tags=( "$@" )
    for i in "${!tags[@]}"; do
        local t="${tags[$i]}"
        hc chain , new_attr string tags.by-name."$t".my_previous_tag \
           , set_attr tags.by-name."$t".my_previous_tag "${tags[$(((i - 1 + n) % n))]}" \
           , new_attr string tags.by-name."$t".my_next_tag \
           , set_attr tags.by-name."$t".my_next_tag "${tags[$(((i + 1) % n))]}"
    done
}

define-tag-cycle "${mon1_names[@]}"
define-tag-cycle "${mon2_names[@]}"
define-tag-cycle "${fallback_names[@]}"

# cycle through tags
# check whether the current tag has a custom "next tag" configured
# if yes, jump to that one, otherwise fall back to the ordinary use_index +1
hc keybind $Mod-period or , substitute NEXT tags.focus.my_next_tag use NEXT \
   , use_index +1 --skip-visible
hc keybind $Mod-comma  or , substitute PREV tags.focus.my_previous_tag use PREV \
   , use_index -1 --skip-visible

# layouting
hc keybind $Mod-r remove
hc keybind $Mod-s floating toggle
hc keybind $Mod-f fullscreen toggle
hc keybind $Mod-Shift-f set_attr clients.focus.floating toggle
hc keybind $Mod-Shift-d set_attr clients.focus.decorated toggle
hc keybind $Mod-Shift-m set_attr clients.focus.minimized true
hc keybind $Mod-Control-m jumpto last-minimized
hc keybind $Mod-p pseudotile toggle
# The following cycles through the available layouts within a frame, but skips
# layouts, if the layout change wouldn't affect the actual window positions.
# I.e. if there are two windows within a frame, the grid layout is skipped.
hc keybind $Mod-space                                                           \
   or , and . compare tags.focus.curframe_wcount = 2                   \
   . cycle_layout +1 vertical horizontal max vertical grid    \
   , cycle_layout +1

# mouse
hc mouseunbind --all
hc mousebind $Mod-Button1 move
hc mousebind $Mod-Button2 zoom
hc mousebind $Mod-Button3 resize

# focus
hc keybind $Mod-BackSpace   cycle_monitor
hc keybind $Mod-Tab         cycle_all +1
hc keybind $Mod-Shift-Tab   cycle_all -1
hc keybind $Mod-c cycle
hc keybind $Mod-i jumpto urgent

# apps
hc keybind $Mod-d spawn $HOME/.config/herbstluftwm/dmenu_run_hlwm
hc keybind $Mod-Shift-d spawn $HOME/.config/herbstluftwm/rofi_run_hlwm
hc keybind $Mod-Shift-e spawn emacs --with-profile=mine

# theme
hc attr theme.tiling.reset 1
hc attr theme.floating.reset 1
hc set frame_border_active_color '#222222cc'
hc set frame_border_normal_color '#101010cc'
hc set frame_bg_normal_color '#565656aa'
hc set frame_bg_active_color '#345F0Caa'
hc set frame_border_width 1
hc set always_show_frame on
hc set frame_bg_transparent on
hc set frame_transparent_width 5
hc set frame_gap 4

hc attr theme.title_height 15
hc attr theme.title_when always
hc attr theme.title_font 'Dejavu Sans:pixelsize=12'  # example using Xft
# hc attr theme.title_font '-*-fixed-medium-r-*-*-13-*-*-*-*-*-*-*'
hc attr theme.title_depth 3  # space below the title's baseline
hc attr theme.active.color '#345F0Cef'
hc attr theme.title_color '#ffffff'
hc attr theme.normal.color '#323232dd'
hc attr theme.urgent.color '#7811A1dd'
hc attr theme.tab_color '#1F1F1Fdd'
hc attr theme.active.tab_color '#2B4F0Add'
hc attr theme.active.tab_outer_color '#6C8257dd'
hc attr theme.active.tab_title_color '#ababab'
hc attr theme.normal.title_color '#898989'
hc attr theme.inner_width 1
hc attr theme.inner_color black
hc attr theme.border_width 3
hc attr theme.floating.border_width 4
hc attr theme.floating.outer_width 1
hc attr theme.floating.outer_color black
hc attr theme.active.inner_color '#789161'
hc attr theme.urgent.inner_color '#9A65B0'
hc attr theme.normal.inner_color '#606060'
# copy inner color to outer_color
for state in active urgent normal ; do
    hc substitute C theme.${state}.inner_color \
       attr theme.${state}.outer_color C
done
hc attr theme.tiling.outer_width 1
hc attr theme.background_color '#141414'

hc set window_gap 0
hc set frame_padding 0
hc set smart_window_surroundings off
hc set smart_frame_surroundings on
hc set mouse_recenter_gap 0

# rules
hc unrule -F
#hc rule class=XTerm tag=3 # move all xterms to tag 3
hc rule focus=on # normally focus new clients
hc rule floatplacement=smart
#hc rule focus=off # normally do not focus new clients
# give focus to most common terminals
#hc rule class~'(.*[Rr]xvt.*|.*[Tt]erm|Konsole)' focus=on
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(DIALOG|UTILITY|SPLASH)' floating=on
hc rule windowtype='_NET_WM_WINDOW_TYPE_DIALOG' focus=on
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(NOTIFICATION|DOCK|DESKTOP)' manage=off
hc rule fixedsize floating=on

hc set tree_style '╾│ ├└╼─┐'

# unlock, just to be sure
hc unlock

# do multi monitor setup here, e.g.:
# hc set_monitors 1280x1024+0+0 1280x1024+1280+0
# or simply:
# hc detect_monitors

# find the panel
# exec ~/.config/herbstluftwm/polybar-panel.sh &
panel=~/.config/herbstluftwm/panel.sh
[ -x "$panel" ] || panel=/etc/xdg/herbstluftwm/panel.sh
for monitor in $(hc list_monitors | cut -d: -f1) ; do
    # start it on each monitor
    "$panel" "$monitor" &
done
