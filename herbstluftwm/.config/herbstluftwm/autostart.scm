#! /usr/bin/guile -s
!#
(add-to-load-path (dirname (current-filename)))
(use-modules (herbstluftwm))

(hc (command "emit_hook" "reload"))

(system "xsetroot -solid '#5A8E3A'")
(system "$HOME/.fehbg")

;; remove all existing keybindings
(hc (command "keyunbind" "--all"))

;;  keybindings
;;  if you have a super key you will be much happier with Mod set to Mod4
;; Mod=Mod1    # Use alt as the main modifier
(define Mod "Mod4")           ; Use the super key as the main modifier

(hc (keybind (string-append Mod "-Shift-q") "quit"))
(hc (keybind (string-append Mod "-Shift-r") "reload"))
(hc (keybind (string-append Mod "-Shift-c") "close"))
(hc (keybind (string-append Mod "-Return") "spawn ${TERMINAL:-kitty}")) ; use your $TERMINAL with xterm as fallback

;; basic movement in tiling and floating mode
;; focusing clients
(hc (keybind (string-append Mod "-Left") "focus left"))
(hc (keybind (string-append Mod "-Down") "focus down"))
(hc (keybind (string-append Mod "-Up")   "focus up"))
(hc (keybind (string-append Mod "-Right") "focus right"))
(hc (keybind (string-append Mod "-h")     "focus left"))
(hc (keybind (string-append Mod "-j")     "focus down"))
(hc (keybind (string-append Mod "-k")     "focus up"))
(hc (keybind (string-append Mod "-l")     "focus right"))

;; moving clients in tiling and floating mode
(hc (keybind (string-append Mod "-Shift-Left")  "shift left"))
(hc (keybind (string-append Mod "-Shift-Down")  "shift down"))
(hc (keybind (string-append Mod "-Shift-Up")    "shift up"))
(hc (keybind (string-append Mod "-Shift-Right") "shift right"))
(hc (keybind (string-append Mod "-Shift-h")     "shift left"))
(hc (keybind (string-append Mod "-Shift-j")     "shift down"))
(hc (keybind (string-append Mod "-Shift-k")     "shift up"))
(hc (keybind (string-append Mod "-Shift-l")     "shift right"))

;; splitting frames
;; create an empty frame at the specified direction
(hc (keybind (string-append Mod "-u") "split bottom 0.5"))
(hc (keybind (string-append Mod "-o") "split right 0.5"))
;; let the current frame explode into subframes
(hc (keybind (string-append Mod "-Control-space") "split explode"))

;; resizing frames and floating clients
(define resizestep (number->string 0.02))
(hc (keybind (string-append Mod "-Control-h")     (string-append "resize left +"  resizestep)))
(hc (keybind (string-append Mod "-Control-j")     (string-append "resize down +"  resizestep)))
(hc (keybind (string-append Mod "-Control-k")     (string-append "resize up +"    resizestep)))
(hc (keybind (string-append Mod "-Control-l")     (string-append "resize right +" resizestep)))
(hc (keybind (string-append Mod "-Control-Left")  (string-append "resize left +"  resizestep)))
(hc (keybind (string-append Mod "-Control-Down")  (string-append "resize down +"  resizestep)))
(hc (keybind (string-append Mod "-Control-Up")    (string-append "resize up +"    resizestep)))
(hc (keybind (string-append Mod "-Control-Right") (string-append "resize right +" resizestep)))

;; tags
(define tag-names (append '("main" "web" "dev") (map number->string (range 4 9))))
(define tag-keys (append (range 1 9) '(0)))

;; hc rename default "${tag_names[0]}" || true
(hc (command "rename" "default" (car tag-names)))

(do ((i 0 (1+ i)))
    ((> i (1- (length tag-names))))
  (hc (add (list-ref tag-names i)))
  (let ((key (list-ref tag-keys i)))
    ;; Mod-# goes to a tag
    (hc (keybind (string-append Mod "-" (number->string key))
                 (string-append "use_index " (number->string i))))
    ;; Mod-Shift-# moves a window to a tag
    (hc (keybind (string-append Mod "-Shift-" (number->string key))
                 (string-append "move_index " (number->string i))))))

;; cycle through tags
(hc (keybind (string-append Mod "-period") "use_index +1 --skip-visible"))
(hc (keybind (string-append Mod "-comma")  "use_index -1 --skip-visible"))

;; layouting
(hc (keybind (string-append Mod "-r") "remove"))
(hc (keybind (string-append Mod "-s") "floating toggle"))
(hc (keybind (string-append Mod "-f") "fullscreen toggle"))
(hc (keybind (string-append Mod "-Shift-f") (set-attr "clients.focus.floating"  "toggle")))
(hc (keybind (string-append Mod "-Shift-d") (set-attr "clients.focus.decorated" "toggle")))
(hc (keybind (string-append Mod "-Shift-m") (set-attr "clients.focus.minimized" "true")))
(hc (keybind (string-append Mod "-Control-m") "jumpto last-minimized"))
(hc (keybind (string-append Mod "-p") "pseudotile toggle"))

;; The following cycles through the available layouts within a frame, but skips
;; layouts, if the layout change wouldn't affect the actual window positions.
;; I.e. if there are two windows within a frame, the grid layout is skipped.
;; hc keybind $Mod-space                                                           \
;;             or , and . compare tags.focus.curframe_wcount = 2                   \
;;                      . cycle_layout +1 vertical horizontal max vertical grid    \
;;                , cycle_layout +1
(hc (keybind (string-append Mod "-space") "cycle_layout +1"))

;; mouse
(hc (command "mouseunbind" "--all"))
(hc (mousebind (string-append Mod "-Button1") "move"))
(hc (mousebind (string-append Mod "-Button2") "zoom"))
(hc (mousebind (string-append Mod "-Button3") "resize"))

;; focus
(hc (keybind (string-append Mod "-BackSpace") "cycle_monitor"))
(hc (keybind (string-append Mod "-Tab")       "cycle_all +1"))
(hc (keybind (string-append Mod "-Shift-Tab") "cycle_all -1"))
(hc (keybind (string-append Mod "-c")         "cycle"))
(hc (keybind (string-append Mod "-i")         "jumpto urgent"))

;; apps
(hc (keybind (string-append Mod "-d") "spawn $HOME/.config/herbstluftwm/dmenu_run_hlwm"))
(hc (keybind (string-append Mod "-Shift-d") "spawn $HOME/.config/herbstluftwm/rofi_run_hlwm"))
(hc (keybind (string-append Mod "-Shift-e") "spawn emacs --with-profile=mine"))

;; theme
(hc (attr "theme.tiling.reset" "1"))
(hc (attr "theme.floating.reset" "1"))
(hc (set "frame_border_active_color" "'#222222cc'"))
(hc (set "frame_border_normal_color" "'#101010cc'"))
(hc (set "frame_bg_normal_color" "'#565656aa'"))
(hc (set "frame_bg_active_color" "'#345F0Caa'"))
(hc (set "frame_border_width" "1"))
(hc (set "always_show_frame" "on"))
(hc (set "frame_bg_transparent" "on"))
(hc (set "frame_transparent_width" "5"))
(hc (set "frame_gap" "4"))

(hc (attr "theme.title_height" "15"))
(hc (attr "theme.title_when" "always"))
(hc (attr "theme.title_font" "'Dejavu Sans:pixelsize=12'"))  ; example using Xft
;; hc attr theme.title_font '-*-fixed-medium-r-*-*-13-*-*-*-*-*-*-*'
(hc (attr "theme.title_depth" "3"))  ; space below the title's baseline
(hc (attr "theme.active.color" "'#345F0Cef'"))
(hc (attr "theme.title_color" "'#ffffff'"))
(hc (attr "theme.normal.color" "'#323232dd'"))
(hc (attr "theme.urgent.color" "'#7811A1dd'"))
(hc (attr "theme.tab_color" "'#1F1F1Fdd'"))
(hc (attr "theme.active.tab_color" "'#2B4F0Add'"))
(hc (attr "theme.active.tab_outer_color" "'#6C8257dd'"))
(hc (attr "theme.active.tab_title_color" "'#ababab'"))
(hc (attr "theme.normal.title_color" "'#898989'"))
(hc (attr "theme.inner_width" "1"))
(hc (attr "theme.inner_color" "black"))
(hc (attr "theme.border_width" "3"))
(hc (attr "theme.floating.border_width" "4"))
(hc (attr "theme.floating.outer_width" "1"))
(hc (attr "theme.floating.outer_color" "black"))
(hc (attr "theme.active.inner_color" "'#789161'"))
(hc (attr "theme.urgent.inner_color" "'#9A65B0'"))
(hc (attr "theme.normal.inner_color" "'#606060'"))

;; copy inner color to outer_color
;; for state in active urgent normal ; do
;;     hc substitute C theme.${state}.inner_color \
;;         attr theme.${state}.outer_color C
;; done

(for-each
 (lambda (state)
   (hc (command "substitute" "C" (string-append "theme." state ".inner_color")
                (attr (string-append "theme." state ".outer_color") "C"))))
 '("active" "urgent" "normal"))

(hc (attr "theme.tiling.outer_width" "1"))
(hc (attr "theme.background_color" "'#141414'"))

(hc (set "window_gap" "0"))
(hc (set "frame_padding" "0"))
(hc (set "smart_window_surroundings" "off"))
(hc (set "smart_frame_surroundings" "on"))
(hc (set "mouse_recenter_gap" "0"))

;; rules
(hc (command "unrule" "-F"))
;; hc rule class=XTerm tag=3 # move all xterms to tag 3
(hc (rule "focus" "on"))                ; normally focus new clients
(hc (rule "floatplacement" "smart"))
;; hc rule focus=off # normally do not focus new clients
;; give focus to most common terminals
;; hc rule class~'(.*[Rr]xvt.*|.*[Tt]erm|Konsole)' focus=on
(hc (rule "windowtype~'_NET_WM_WINDOW_TYPE_(DIALOG|UTILITY|SPLASH)'" "floating" "on"))
(hc (rule "windowtype='_NET_WM_WINDOW_TYPE_DIALOG'" "focus" "on"))
(hc (rule "windowtype~'_NET_WM_WINDOW_TYPE_(NOTIFICATION|DOCK|DESKTOP)'" "manage" "off"))
(hc (rule "fixedsize" "floating" "on"))

(hc (set "tree_style" "'╾│ ├└╼─┐'"))

;; unlock, just to be sure
(hc (command "unlock"))

;; do multi monitor setup here, e.g.:
;; hc set_monitors 1280x1024+0+0 1280x1024+1280+0
;; or simply:
(hc (command "detect_monitors"))

;; find the panel
(define panel "~/.config/herbstluftwm/polybar-panel.sh")
;; [ -x "$panel" ] || panel=/etc/xdg/herbstluftwm/panel.sh

;; (let ((monitors (hc (command "list_monitors"))))
;;   (if (list? monitors)
;;       (let ((monitor-list (map (lambda (monitor) (car (string-split monitor #\:)))
;;                                monitors)))
;;         (for-each (lambda (monitor) (system (string-append panel " " monitor)))
;;             monitor-list))
;;       (system (string-append panel " " (car (string-split monitors #\:))))))

;; (if (list? (hc (command "list_monitors")))
;;     (display "monitors is a list")
;;     (display "monitors is not a list"))
;; (newline)

;; for monitor in $(hc list_monitors | cut -d: -f1) ; do
;;     # start it on each monitor
;;     "$panel" "$monitor" &
;; done
(system "~/.config/herbstluftwm/polybar-panel.sh")
