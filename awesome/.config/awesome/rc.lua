package.cpath = package.cpath .. ';/home/jeffbowman/dotfiles/awesome/.config/awesome/obvious/lib/unicode/native.so'
-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
local lain = require("lain")
-- local vicious = require("vicious.widgets.init")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup").widget

-- local checkout from github instead of using package from Arch
local obvious = require("obvious")
require("obvious.mem")

beautiful.wallpaper = "/home/jeffbowman/Pictures/rough_paper_v2_arch_svg_by_karl_schneider-das3gdp.png"

-- {{{ Debug package.cpath value
naughty.notify({ preset = naughty.config.presets.critical,
		 title = "package.cpath value",
		 text = package.cpath })

-- }}}

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
   naughty.notify({ preset = naughty.config.presets.critical,
                    title = "Oops, there were errors during startup!",
                    text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
   local in_error = false
   awesome.connect_signal("debug::error", function (err)
                             -- Make sure we don't go into an endless error loop    
                             if in_error then return end
                             in_error = true

                             naughty.notify({ preset = naughty.config.presets.critical,
                                              title = "Oops, an error happened!",
                                              text = tostring(err) })
                             in_error = false
   end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init(awful.util.getdir("config") .. "themes/bamboo/theme.lua")
-- beautiful.init("/usr/share/awesome/themes/sunjack/theme.lua")
-- beautiful.init(awful.util.getdir("config") .. "themes/sunjack/theme.lua")

-- This is used later as the default terminal and editor to run.
-- terminal = "terminator"
terminal = "kitty"
editor = os.getenv("EDITOR") or "emacsient -c"
-- editor_cmd = "xterm" .. " -e " .. "/usr/bin/vim"
editor_cmd = "/usr/bin/gvim"

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
   awful.layout.suit.tile,
   -- awful.layout.suit.tile.left,
   -- awful.layout.suit.tile.bottom,
   -- awful.layout.suit.tile.top,
   awful.layout.suit.floating,
   -- awful.layout.suit.fair,
   -- awful.layout.suit.fair.horizontal,
   -- awful.layout.suit.spiral,
   -- awful.layout.suit.spiral.dwindle,
   awful.layout.suit.max,
   -- awful.layout.suit.max.fullscreen,
   -- awful.layout.suit.magnifier,
   -- awful.layout.suit.corner.nw,
   -- awful.layout.suit.corner.ne,
   -- awful.layout.suit.corner.sw,
   -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Helper functions
local function client_menu_toggle_fn()
   local instance = nil

   return function ()
      if instance and instance.wibox.visible then
         instance:hide()
         instance = nil
      else
         instance = awful.menu.clients({ theme = { width = 250 } })
      end
   end
end
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() return false, hotkeys_popup.show_help end},
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end}
}

mycommsmenu = {
   { "BlueJeans", "/usr/bin/bluejeans" }, 
   { "Franz",     "/usr/bin/franz" }, 
   { "Skype",     "/usr/bin/skypeforlinux" },
   { "Slack",     "/usr/bin/slack" }
}

myutilsmenu = {
   { "Cisco AnyConnect", "/opt/cisco/anyconnect/bin/vpnui" },
   { "Keepass",          "/usr/bin/keepassxc" },
   { "KafkaTool2",       "/usr/bin/kafkatool2" },
   { "Files",            "/usr/bin/pcmanfm" },
   --   { "Nautilus",         "nautilus" },
   --   { "Thunar",           "thunar" }
}

mybrowsermenu = {
   { "Brave", "brave" },
   { "Chromium", "firejail chromium" },
   { "Firefox",  "firefox" },
   { "Google Chrome", "firejail google-chrome-stable" },
   { "Tor", "tor-browser" },
}

mydevtools = {
   { "Emacs", "emacsclient -c" },
   { "Idea", "/usr/bin/idea" },
}

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                             { "open terminal", terminal },
                             { "minecraft", "/usr/bin/multimc" },
                             { "cams", "/home/jeffbowman/bin/cams" },
                             { "comms", mycommsmenu },
                             { "dev tools", mydevtools },
                             { "browsers", mybrowsermenu },
                             { "utils", myutilsmenu },
}
                       })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a wibox for each screen and add it
local taglist_buttons = awful.util.table.join(
   awful.button({ }, 1, function(t) t:view_only() end),
   awful.button({ modkey }, 1, function(t)
         if client.focus then
            client.focus:move_to_tag(t)
         end
   end),
   awful.button({ }, 3, awful.tag.viewtoggle),
   awful.button({ modkey }, 3, function(t)
         if client.focus then
            client.focus:toggle_tag(t)
         end
   end),
   awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
   awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
)

local tasklist_buttons = awful.util.table.join(
   awful.button({ }, 1, function (c)
         if c == client.focus then
            c.minimized = true
         else
            -- Without this, the following
            -- :isvisible() makes no sense
            c.minimized = false
            if not c:isvisible() and c.first_tag then
               c.first_tag:view_only()
            end
            -- This will also un-minimize
            -- the client, if needed
            client.focus = c
            c:raise()
         end
   end),
   awful.button({ }, 3, client_menu_toggle_fn()),
   awful.button({ }, 4, function ()
         awful.client.focus.byidx(1)
   end),
   awful.button({ }, 5, function ()
         awful.client.focus.byidx(-1)
end))

local function set_wallpaper(s)
   -- Wallpaper
   if beautiful.wallpaper then
      local wallpaper = beautiful.wallpaper
      -- If wallpaper is a function, call it with the screen
      if type(wallpaper) == "function" then
         wallpaper = wallpaper(s)
      end
      gears.wallpaper.maximized(wallpaper, s, true)
   end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

-- Create a textclock widget
local mytextclock = wibox.widget.textclock()
local month_calendar = awful.widget.calendar_popup.month()
month_calendar:attach(mytextclock, 'tr')

-- ALSA volume control
--[[ 

Using 2 volume controls, one to display a slider and the other a
percentage. Left-click on either will mute, right-click on the
percentage to open alsa mixer. Scrolling on either will adjust the
volume. Volume keys on keyboard also work to control volume.

]]
local myvolume = obvious.volume_alsa() -- shows percentage
-- shows slider bar
local volumebar_widget = require("awesome-wm-widgets.volumebar-widget.volumebar") 

-- Battery widget .. Does not show up on the wibar for some reason
local mybattery = require("awesome-wm-widgets.battery-widget.battery")

-- CPU widget
local cpu_widget = require("awesome-wm-widgets.cpu-widget.cpu-widget")

local lain_mem = lain.widget.mem({
      settings = function ()
         local used = string.format("%2.2f", (mem_now.used / 1024))
         widget:set_text("Mem: " .. used .. "Gb | " .. mem_now.perc .. "%")
      end
})


awful.screen.connect_for_each_screen(function(s)
      -- Wallpaper
      set_wallpaper(s)

      -- Each screen has its own tag table.
      awful.tag({ "1-Main", "2-Chrome", "3-FF", "4-IDE", "5", "6", "7", "8", "9" }, s,
         {awful.layout.suit.floating,  -- 1-Main
          awful.layout.suit.tile,      -- 2-Chrome
          awful.layout.suit.tile,      -- 3-Firefox
          awful.layout.suit.tile,      -- 4-IDE
          awful.layout.suit.max,       -- 5
          awful.layout.suit.tile,      -- 6
          awful.layout.suit.tile,      -- 7
          awful.layout.suit.tile,      -- 8
          awful.layout.suit.floating}) -- 9

      -- Create a promptbox for each screen
      s.mypromptbox = awful.widget.prompt()
      -- Create an imagebox widget which will contains an icon indicating which layout we're using.
      -- We need one layoutbox per screen.
      s.mylayoutbox = awful.widget.layoutbox(s)
      s.mylayoutbox:buttons(awful.util.table.join(
                               awful.button({ }, 1, function () awful.layout.inc( 1) end),
                               awful.button({ }, 3, function () awful.layout.inc(-1) end),
                               awful.button({ }, 4, function () awful.layout.inc( 1) end),
                               awful.button({ }, 5, function () awful.layout.inc(-1) end)))
      -- Create a taglist widget
      s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)

      -- Create a tasklist widget
      s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons)

      -- Create the wibox
      s.mywibox = awful.wibar({ position = "top", screen = s })

      -- Add widgets to the wibox
      s.mywibox:setup {
         layout = wibox.layout.align.horizontal,
         { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mypromptbox,
         },
         s.mytasklist, -- Middle widget
         { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            --            mykeyboardlayout,
            wibox.widget.systray(),
            lain_mem.widget,
            cpu_widget(),
            mybattery(),
            volumebar_widget({
                  mute_color = "#900000",
                  shape = 'rounded_bar',
                  width = 80,
                  margins = 8
            }),
            myvolume,
            mytextclock,
            s.mylayoutbox,
         },
      }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
                awful.button({ }, 3, function () mymainmenu:toggle() end),
                awful.button({ }, 4, awful.tag.viewnext),
                awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
   awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
      {description="show help", group="awesome"}),
   awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
      {description = "view previous", group = "tag"}),
   awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
      {description = "view next", group = "tag"}),
   awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
      {description = "go back", group = "tag"}),

   awful.key({ modkey,           }, "j",
      function ()
         awful.client.focus.byidx( 1)
      end,
      {description = "focus next by index", group = "client"}
   ),
   awful.key({ modkey,           }, "k",
      function ()
         awful.client.focus.byidx(-1)
      end,
      {description = "focus previous by index", group = "client"}
   ),
   awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
      {description = "show main menu", group = "awesome"}),

   -- Layout manipulation
   awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
      {description = "swap with next client by index", group = "client"}),
   awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
      {description = "swap with previous client by index", group = "client"}),
   awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
      {description = "focus the next screen", group = "screen"}),
   awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
      {description = "focus the previous screen", group = "screen"}),
   awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
      {description = "jump to urgent client", group = "client"}),
   awful.key({ modkey,           }, "Tab",
      function ()
         awful.client.focus.history.previous()
         if client.focus then
            client.focus:raise()
         end
      end,
      {description = "go back", group = "client"}),

   -- Standard program
   awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
      {description = "open a terminal", group = "launcher"}),
   awful.key({ modkey, "Control" }, "r", awesome.restart,
      {description = "reload awesome", group = "awesome"}),
   awful.key({ modkey, "Shift"   }, "q", awesome.quit,
      {description = "quit awesome", group = "awesome"}),

   awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
      {description = "increase master width factor", group = "layout"}),
   awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
      {description = "decrease master width factor", group = "layout"}),
   awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
      {description = "increase the number of master clients", group = "layout"}),
   awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
      {description = "decrease the number of master clients", group = "layout"}),
   awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
      {description = "increase the number of columns", group = "layout"}),
   awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
      {description = "decrease the number of columns", group = "layout"}),
   awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
      {description = "select next", group = "layout"}),
   awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
      {description = "select previous", group = "layout"}),

   awful.key({ modkey, "Control" }, "n",
      function ()
         local c = awful.client.restore()
         -- Focus restored client
         if c then
            client.focus = c
            c:raise()
         end
      end,
      {description = "restore minimized", group = "client"}),

   -- Prompt
   awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
      {description = "run prompt", group = "launcher"}),

   awful.key({ modkey }, "x",
      function ()
         awful.prompt.run {
            prompt       = "Run Lua code: ",
            textbox      = awful.screen.focused().mypromptbox.widget,
            exe_callback = awful.util.eval,
            history_path = awful.util.get_cache_dir() .. "/history_eval"
         }
      end,
      {description = "lua execute prompt", group = "awesome"}),
   -- Menubar
   awful.key({ modkey }, "p", function() menubar.show() end,
      {description = "show the menubar", group = "launcher"}),
   -- Custom
   awful.key({ modkey, "Shift"   },          "a",      function () awful.spawn("/usr/bin/alacritty") end),
   awful.key({ modkey, "Shift"   },          "p",      function () awful.spawn("/usr/bin/xfce4-appfinder") end),
   awful.key({ modkey, "Shift"   },          "x",      function () awful.spawn("/usr/bin/i3lock -c 000011") end),
   awful.key({ modkey, "Shift"   },          "Return", function () awful.spawn("/usr/bin/termite") end),
   awful.key({ modkey, "Control", "Shift" }, "a",      function () awful.spawn("terminator -x sudo arch32") end),
   awful.key({ modkey, "Control", "Shift" }, "c",      function () awful.spawn("chromium") end),
   awful.key({ modkey, "Control", "Shift" }, "e",      function () awful.spawn("emacsclient -c") end),
   awful.key({ modkey, "Control", "Shift" }, "f",      function () awful.spawn("firefox") end),
   awful.key({ modkey, "Control", "Shift" }, "t",      function () awful.spawn("terminator") end),
   awful.key({         }, "XF86AudioLowerVolume",      function () awful.spawn("amixer -D pulse sset Master 5%-") end),
   awful.key({         }, "XF86AudioRaiseVolume",      function () awful.spawn("amixer -D pulse sset Master 5%+") end),
   awful.key({         }, "XF86AudioMute",             function () awful.spawn("amixer -D pulse sset Master toggle") end),
   awful.key({         }, "XF86Sleep",                 function () awful.spawn("systemctl suspend") end),
   awful.key({         }, "XF86Calculator",            function () awful.spawn("galculator") end),
   -- awful.key({ modkey  }, "`",                         function () awful.spawn("/usr/bin/tilix --quake") end),
   awful.key({ modkey  }, "=",                         function () awful.spawn("/usr/bin/emacsclient -n -e '(equake-invoke)'") end)
) 

clientkeys = awful.util.table.join(
   awful.key({ modkey,           }, "f",
      function (c)
         c.fullscreen = not c.fullscreen
         c:raise()
      end,
      {description = "toggle fullscreen", group = "client"}),
   awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end,
      {description = "close", group = "client"}),
   awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
      {description = "toggle floating", group = "client"}),
   awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
      {description = "move to master", group = "client"}),
   awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
      {description = "move to screen", group = "client"}),
   awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
      {description = "toggle keep on top", group = "client"}),
   awful.key({ modkey,           }, "n",
      function (c)
         -- The client currently has the input focus, so it cannot be
         -- minimized, since minimized clients can't have the focus.
         c.minimized = true
      end ,
      {description = "minimize", group = "client"}),
   awful.key({ modkey,           }, "m",
      function (c)
         c.maximized = not c.maximized
         c:raise()
      end ,
      {description = "maximize", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
   globalkeys = awful.util.table.join(globalkeys,
                                      -- View tag only.
                                      awful.key({ modkey }, "#" .. i + 9,
                                         function ()
                                            local screen = awful.screen.focused()
                                            local tag = screen.tags[i]
                                            if tag then
                                               tag:view_only()
                                            end
                                         end,
                                         {description = "view tag #"..i, group = "tag"}),
                                      -- Toggle tag display.
                                      awful.key({ modkey, "Control" }, "#" .. i + 9,
                                         function ()
                                            local screen = awful.screen.focused()
                                            local tag = screen.tags[i]
                                            if tag then
                                               awful.tag.viewtoggle(tag)
                                            end
                                         end,
                                         {description = "toggle tag #" .. i, group = "tag"}),
                                      -- Move client to tag.
                                      awful.key({ modkey, "Shift" }, "#" .. i + 9,
                                         function ()
                                            if client.focus then
                                               local tag = client.focus.screen.tags[i]
                                               if tag then
                                                  client.focus:move_to_tag(tag)
                                               end
                                            end
                                         end,
                                         {description = "move focused client to tag #"..i, group = "tag"}),
                                      -- Toggle tag on focused client.
                                      awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                                         function ()
                                            if client.focus then
                                               local tag = client.focus.screen.tags[i]
                                               if tag then
                                                  client.focus:toggle_tag(tag)
                                               end
                                            end
                                         end,
                                         {description = "toggle focused client on tag #" .. i, group = "tag"})
   )
end

clientbuttons = awful.util.table.join(
   awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
   awful.button({ modkey }, 1, awful.mouse.client.move),
   awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
   -- All clients will match this rule.
   { rule = { },
     properties = { border_width = beautiful.border_width,
                    border_color = beautiful.border_normal,
                    focus = awful.client.focus.filter,
                    raise = true,
                    keys = clientkeys,
                    buttons = clientbuttons,
                    screen = awful.screen.preferred,
                    placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
   },

   -- Floating clients.
   { rule_any = {
        instance = {
           "DTA",  -- Firefox addon DownThemAll.
           "copyq",  -- Includes session name in class.
        },
        class = {
           "Arandr",
           "Gpick",
           "Kruler",
           "MessageWin",  -- kalarm.
           "Sxiv",
           "Wpa_gui",
           "pinentry",
           "veromix",
           "xtightvncviewer"},

        name = {
           "Event Tester",  -- xev.
        },
        role = {
           "AlarmWindow",  -- Thunderbird's calendar.
           "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
   }, properties = { floating = true }},

   -- Add titlebars to normal clients and dialogs
   { rule_any = {type = { "normal", "dialog" }
                }, properties = { titlebars_enabled = true } },

   -- disable titlebars for equake
   { rule = { instance = "*EQUAKE*", class = "Emacs" },      
     properties = { titlebars_enabled = false } },

   -- Screen 1 rules
   -- skypeforlinux
   { rule = { class = "skypforlinux" },
     properties = { screen = 1, tag = "1-Main" } },
   -- Slack
   { rule = { class = "slack" },
     properties = { screen = 1, tag = "1-Main" } },
   -- KeePass2
   { rule = { class = "keepass2" },
     properties = { screen = 1, tag = "1-Main" } },
   -- BlueJeans
   { rule = { class = "Bluejeans" },
     properties = { screen = 1, tag = "9" } },
   -- Spotify
   { rule = { class = "Spotify" },
     properties = { screen = 1, tag = "9" } },
   
   -- Screen 2 rules
   -- Firefox
   { rule = { class = "Firefox" },
     properties = { tag = "3-FF" } },
   -- Chromium
   { rule = { class = "Chromium" },
     properties = { tag = "2-Chrome" } },
   { rule = { class = "Google-chrome" },
     properties = { tag = "2-Chrome" } },
   -- IntelliJ IDEA
   { rule = { class = "jetbrains-idea-ce" },
     properties = { tag = "4-IDE" } }
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
                         -- Set the windows at the slave,
                         -- i.e. put it at the end of others instead of setting it master.
                         -- if not awesome.startup then awful.client.setslave(c) end

                         if awesome.startup and
                            not c.size_hints.user_position
                         and not c.size_hints.program_position then
                            -- Prevent clients from being unreachable after screen count changes.
                            awful.placement.no_offscreen(c)
                         end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
                         -- buttons for the titlebar
                         local mybuttons = awful.util.table.join(
                            awful.button({ }, 1, function()
                                  client.focus = c
                                  c:raise()
                                  awful.mouse.client.move(c)
                            end),
                            awful.button({ }, 3, function()
                                  client.focus = c
                                  c:raise()
                                  awful.mouse.client.resize(c)
                            end)
                         )

                         local mytitlebar = awful.titlebar(c)

                         -- awful.titlebar(c) : setup {
                         mytitlebar : setup {
                            { -- Left
                               awful.titlebar.widget.iconwidget(c),
                               buttons = mybuttons,
                               layout  = wibox.layout.fixed.horizontal
                            },
                            { -- Middle
                               { -- Title
                                  align  = "center",
                                  font   = "Courier New 14",
                                  widget = awful.titlebar.widget.titlewidget(c)
                               },
                               buttons = buttons,
                               layout  = wibox.layout.flex.horizontal
                            },
                            { -- Right
                               awful.titlebar.widget.floatingbutton (c),
                               awful.titlebar.widget.maximizedbutton(c),
                               awful.titlebar.widget.stickybutton   (c),
                               awful.titlebar.widget.ontopbutton    (c),
                               awful.titlebar.widget.closebutton    (c),
                               layout = wibox.layout.fixed.horizontal()
                            },
                            layout = wibox.layout.align.horizontal
                                            }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
                         if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
                         and awful.client.focus.filter(c) then
                            client.focus = c
                         end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- {{{ Autostart
--
-- runonce should only run one time
--
awful.spawn.once("/home/jeffbowman/bin/awesome_startup.sh")

--
-- miscellaneous start options (possible candidates to move to either 
-- startup.sh or runonce
--
awful.spawn.with_shell("/home/jeffbowman/bin/fix_vid")

-- these 2 are handled in runonce
-- awful.spawn.with_shell("/usr/bin/xmodmap /home/jeffbowman/.xmodmap.swapcaps")
-- awful.spawn.single_instance("/usr/bin/volumeicon")
--
-- updated bin/startup.sh to include starting gnome-keyring-daemon, which should replace this
-- 
-- awful.spawn.with_shell("eval $(/usr/bin/ssh-agent -s > /home/jeffbowman/bin/ssh-agent.vars)")
-- }}}
