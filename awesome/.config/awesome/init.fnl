;;;; -*- mode: fennel -*-
;;
;; Outline minor mode is useful for navigating between sections.
;;

;;; Libraries
;; Standard awesome library
(local gears (require :gears))
(local awful (require :awful))
(require :awful.autofocus)
;; Widget and layout library
(local wibox (require :wibox))
(local lain (require :lain))
;; Theme handline library
(local beautiful (require :beautiful))
;; Notification library
(local naughty (require :naughty))
(local menubar (require :menubar))
(local hotkeys-popup (. (require :awful.hotkeys_popup) :widget))

;; local checkout from github instead of package from Arch
(local obvious (require :obvious))
(require :obvious.mem)


(set beautiful.wallpaper
     :/home/jeffbowman/Pictures/rough_paper_v2_arch_svg_by_karl_schneider-das3gdp.png)

;;; Debug package.cpath value
(naughty.notify {:preset naughty.config.presets.low
                 :title "package.cpath value"
                 :text package.cpath})

;;; Error handling
;; Check if awesome encountered an error during startup and fall back
;; to another config (This code will only ever execute for the
;; fallback config)
(when awesome.startup_errors
  (naughty.notify {:preset naughty.config.presets.critical
                   :title "Oops, there were errors during startup!"
                   :text awesome.startup_errors}))

;;; Handle runtime errors after startup
(do
  (var in-error false)
  (awesome.connect_signal "debug::error"
                          (fn [err]
                            (when in-error
                              (lua "return "))
                            (set in-error true)
                            (naughty.notify {:preset naughty.config.presets.critical
                                             :title "Oops, an error happened!"
                                             :text (tostring err)})
                            (set in-error false))))

;;; Variable definitions
;; Themes define colors, icons, font and wallpapers
(beautiful.init (.. (awful.util.getdir :config) :themes/bamboo/theme.lua))

;;; This is used later as the default terminal and editor to run
(global terminal :kitty)
(global editor (or (os.getenv :EDITOR) "emacsient -c"))
(global editor-cmd :/usr/bin/gvim)

;;; Default modkey
;; Usually, Mod4 is the key with the logo between Control and Alt. If
;; you do not like this or do not have such a key, I suggest you to
;; remap Mod4 to another key using xmodmap or other tools. However,
;; you can use another modifier like Mod1, but it may interact with
;; others.
(global modkey :Mod4)

;;; Table of layouts to cover with awful.layout.inc, order matters
;; Options not currently used:
;; -- awful.layout.suit.tile.left,
;; -- awful.layout.suit.tile.bottom,
;; -- awful.layout.suit.tile.top,
;; -- awful.layout.suit.fair.horizontal,
;; -- awful.layout.suit.spiral,
;; -- awful.layout.suit.spiral.dwindle,
;; -- awful.layout.suit.max.fullscreen,
;; -- awful.layout.suit.magnifier,
;; -- awful.layout.suit.corner.nw,
;; -- awful.layout.suit.corner.ne,
;; -- awful.layout.suit.corner.sw,
;; -- awful.layout.suit.corner.se,
(set awful.layout.layouts {1 awful.layout.suit.tile
                           2 awful.layout.suit.floating
                           3 awful.layout.suit.fair
                           4 awful.layout.suit.max})


;;; Helper functions
(fn client-menu-toggle-fn []
  (var instance nil)
  (fn []
    (if (and instance instance.wibox.visible)
        (do
          (instance:hide)
          (set instance nil))
        (set instance (awful.menu.clients {:theme {:width 250}})))))

;;; Menu
;; Create a launcher widget and a main menu
(global myawesomemenu {1 {1 :hotkeys
                          2 (fn []
                              (values false hotkeys-popup.show_help))}
                       2 {1 :manual 2 (.. terminal " -e man awesome")}
                       3 {1 "edit config"
                          2 (.. editor-cmd " " awesome.conffile)}
                       4 {1 :restart 2 awesome.restart}
                       5 {1 :quit
                          2 (fn []
                              (awesome.quit))}})
(global mycommsmenu {1 {1 :Franz 2 :/usr/bin/franz}
                     2 {1 :Skype 2 :/usr/bin/skypeforlinux}
                     3 {1 :Slack 2 :/usr/bin/slack}})
(global myutilsmenu {1 {1 "Cisco AnyConnect"
                        2 :/opt/cisco/anyconnect/bin/vpnui}
                     2 {1 :Keepass 2 :/usr/bin/keepassxc}
                     3 {1 :KafkaTool2 2 :/usr/bin/kafkatool2}
                     4 {1 :Files 2 :/usr/bin/pcmanfm}})
(global mybrowsermenu {1 {1 :Brave 2 :brave}
                       2 {1 :Chromium 2 "firejail chromium"}
                       3 {1 :Firefox 2 :firefox}
                       4 {1 "Google Chrome" 2 "firejail google-chrome-stable"}
                       5 {1 :Tor 2 :tor-browser}})
(global mydevtools {1 {1 :Emacs 2 "emacsclient -c"}
                    2 {1 :Idea 2 :/usr/bin/idea}})
(global mymainmenu
        (awful.menu {:items {1 {1 :awesome
                                2 myawesomemenu
                                3 beautiful.awesome_icon}
                             2 {1 "open terminal" 2 terminal}
                             3 {1 :minecraft 2 :/usr/bin/multimc}
                             4 {1 :cams 2 :/home/jeffbowman/bin/cams}
                             5 {1 :comms 2 mycommsmenu}
                             6 {1 "dev tools" 2 mydevtools}
                             7 {1 :browsers 2 mybrowsermenu}
                             8 {1 :utils 2 myutilsmenu}}}))
(global mylauncher
        (awful.widget.launcher {:image beautiful.awesome_icon :menu mymainmenu}))

;;; Menubar configuration
(set menubar.utils.terminal terminal)   ; Set the terminal for
                                        ; applications that require
                                        ; it.

;;; Keyboard map indicator and switcher
(global mykeyboardlayout (awful.widget.keyboardlayout))

;;; Wibar
;; Create a wibox for each screen and add it
(local taglist-buttons
       (awful.util.table.join (awful.button {} 1
                                            (fn [t]
                                              (t:view_only)))
                              (awful.button {1 modkey} 1
                                            (fn [t]
                                              (when client.focus
                                                (client.focus:move_to_tag t))))
                              (awful.button {} 3 awful.tag.viewtoggle)
                              (awful.button {1 modkey} 3
                                            (fn [t]
                                              (when client.focus
                                                (client.focus:toggle_tag t))))
                              (awful.button {} 4
                                            (fn [t]
                                              (awful.tag.viewnext t.screen)))
                              (awful.button {} 5
                                            (fn [t]
                                              (awful.tag.viewprev t.screen)))))
(local tasklist-buttons
       (awful.util.table.join (awful.button {} 1
                                            (fn [c]
                                              (if (= c client.focus)
                                                  (set c.minimized true)
                                                  (do
                                                    ;; Without this,
                                                    ;; the following
                                                    ;; :isvisible
                                                    ;; makes no sense
                                                    (set c.minimized false)
                                                    (when (and (not (c:isvisible))
                                                               c.first_tag)
                                                      (c.first_tag:view_only))
                                                    ;; This will also
                                                    ;; un-minimize the
                                                    ;; client, if
                                                    ;; needed
                                                    (set client.focus c)
                                                    (c:raise)))))
                              (awful.button {} 3 (client-menu-toggle-fn))
                              (awful.button {} 4
                                            (fn []
                                              (awful.client.focus.byidx 1)))
                              (awful.button {} 5
                                            (fn []
                                              (awful.client.focus.byidx (- 1))))))

(fn set-wallpaper [s]
  ;; Wallpaper
  (when beautiful.wallpaper
    (var wallpaper beautiful.wallpaper)
    ;; If wallpaper is a function, call it with the screen
    (when (= (type wallpaper) :function)
      (set wallpaper (wallpaper s)))
    (gears.wallpaper.maximized wallpaper s true)))

;; Re-set wallpaper when a screen's geometry changes (e.g. different
;; resolution)
(screen.connect_signal "property::geometry" set-wallpaper)

;;; Create a textclock widget
(local mytextclock (wibox.widget.textclock))
(local month-calendar (awful.widget.calendar_popup.month))
(month-calendar:attach mytextclock :tr)


(local volumebar-widget (require :awesome-wm-widgets.volume-widget.volume))

;;; Battery widget .. Does not show up on the wibar for some reason
(local mybattery (require :awesome-wm-widgets.battery-widget.battery))

;;; CPU widget
(local cpu-widget (require :awesome-wm-widgets.cpu-widget.cpu-widget))

(local lain-mem
       (lain.widget.mem {:settings
                         (fn []
                           (local used
                                  (string.format "%2.2f" (/ mem-now.used 1024)))
                           (widget:set_text (.. "Mem: " used "Gb | "
                                                mem-now.perc "%")))}))

(awful.screen.connect_for_each_screen
 (fn [s]
   ;; Wallpaper
   (set-wallpaper s)

   ;; Each screen has its own tag table
   (awful.tag {1 :1-Main
               2 :2-Web
               3 :3-Emacs
               4 :4-Term
               5 :5
               6 :6
               7 :7
               8 :8
               9 :9}
              s
              {1 awful.layout.suit.floating
               2 awful.layout.suit.tile
               3 awful.layout.suit.fair
               4 awful.layout.suit.fair
               5 awful.layout.suit.max
               6 awful.layout.suit.tile
               7 awful.layout.suit.tile
               8 awful.layout.suit.tile
               9 awful.layout.suit.floating})

   ;; Create a promptbox for each screen
   (set s.mypromptbox (awful.widget.prompt))

   ;; Create an imagebox widget which will contain an icon indicating
   ;; which layout we're using. Need one layoutbox per screen
   (set s.mylayoutbox (awful.widget.layoutbox s))
   (s.mylayoutbox:buttons
    (awful.util.table.join
     (awful.button {} 1 (fn [] (awful.layout.inc 1)))
     (awful.button {} 3 (fn [] (awful.layout.inc (- 1))))
     (awful.button {} 4 (fn [] (awful.layout.inc 1)))
     (awful.button {} 5 (fn [] (awful.layout.inc (- 1))))))

   ;; Create a taglist widget
   (set s.mytaglist
        (awful.widget.taglist s
                              awful.widget.taglist.filter.all
                              taglist-buttons))

   ;; Create a tasklist widget
   (set s.mytasklist
        (awful.widget.tasklist s
                               awful.widget.tasklist.filter.currenttags
                               tasklist-buttons))

   ;; Create the wibox
   (set s.mywibox
        (awful.wibar {:position :top
                      :screen s}))

   ;; Add widgets to the wibox
   (s.mywibox:setup {:layout wibox.layout.align.horizontal
                     1 {:layout wibox.layout.fixed.horizontal
                        ;; Left widgets
                        1 mylauncher
                        2 s.mytaglist
                        3 s.mypromptbox}
                     2 s.mytasklist     ; Middle widget
                     3 {:layout wibox.layout.fixed.horizontal
                        ;; Right widgets
                        1 (wibox.widget.systray)
                        2 lain-mem.widget
                        3 (cpu-widget)
                        4 (mybattery)
                        5 (volumebar-widget {:widget_type :horizontal_bar
                                             :shape :rounded_bar
                                             :width 80
                                             :margins 8})
                        6 mytextclock
                        7 s.mylayoutbox}})))

;;; Mouse bindings
(root.buttons (awful.util.table.join
               (awful.button {} 3 (fn [] (mymainmenu:toggle)))
               (awful.button {} 4 awful.tag.viewnext)
               (awful.button {} 5 awful.tag.viewprev)))

;;; Key bindings
(global globalkeys
        (awful.util.table.join
         (awful.key {1 modkey} :s hotkeys-popup.show_help
                    {:description "show help" :group :awesome})
         (awful.key {1 modkey} :Left awful.tag.viewprev
                    {:description "view previous" :group :tag})
         (awful.key {1 modkey} :Right awful.tag.viewnext
                    {:description "view next" :group :tag})
         (awful.key {1 modkey} :Escape
                    awful.tag.history.restore
                    {:description "go back" :group :tag})
         (awful.key {1 modkey} :j
                    (fn [] (awful.client.focus.byidx 1))
                    {:description "focus next by index" :group :client})
         (awful.key {1 modkey} :k
                    (fn [] (awful.client.focus.byidx (- 1)))
                    {:description "focus previous by index" :group :client})
         (awful.key {1 modkey} :w
                    (fn [] (mymainmenu:show))
                    {:description "show main menu" :group :awesome})
         (awful.key {1 modkey 2 :Shift} :j
                    (fn [] (awful.client.swap.byidx 1))
                    {:description "swap with next client by index" :group :client})
         (awful.key {1 modkey 2 :Shift} :k
                    (fn [] (awful.client.swap.byidx (- 1)))
                    {:description "swap with previous client by index" :group :client})
         (awful.key {1 modkey 2 :Control} :j
                    (fn [] (awful.screen.focus_relative 1))
                    {:description "focus the next screen" :group :screen})
         (awful.key {1 modkey 2 :Control} :k
                    (fn [] (awful.screen.focus_relative (- 1)))
                    {:description "focus the previous screen" :group :screen})
         (awful.key {1 modkey} :u
                    awful.client.urgent.jumpto
                    {:description "jump to urgent client" :group :client})
         (awful.key {1 modkey} :Tab
                    (fn []
                      (awful.client.focus.history.previous)
                      (when client.focus
                        (client.focus:raise)))
                    {:description "go back" :group :client})

         ;; Standard program
         (awful.key {1 modkey} :Return
                    (fn [] (awful.spawn terminal))
                    {:description "open a terminal" :group :launcher})
         (awful.key {1 modkey 2 :Control} :r
                    awesome.restart
                    {:description "reload awesome" :group :awesome})
         (awful.key {1 modkey 2 :Shift} :q awesome.quit
                    {:description "quit awesome" :group :awesome})
         (awful.key {1 modkey} :l
                    (fn [] (awful.tag.incmwfact 0.05))
                    {:description "increase master width factor" :group :layout})
         (awful.key {1 modkey} :h
                    (fn [] (awful.tag.incmwfact (- 0.05)))
                    {:description "decrease master width factor" :group :layout})
         (awful.key {1 modkey 2 :Shift} :h
                    (fn []
                      (awful.tag.incnmaster 1 nil true))
                    {:description "increase the number of master clients" :group :layout})
         (awful.key {1 modkey 2 :Shift} :l
                    (fn [] (awful.tag.incnmaster (- 1) nil true))
                    {:description "decrease the number of master clients" :group :layout})
         (awful.key {1 modkey 2 :Control} :h
                    (fn [] (awful.tag.incncol 1 nil true))
                    {:description "increase the number of columns" :group :layout})
         (awful.key {1 modkey 2 :Control} :l
                    (fn [] (awful.tag.incncol (- 1) nil true))
                    {:description "decrease the number of columns" :group :layout})
         (awful.key {1 modkey} :space
                    (fn [] (awful.layout.inc 1))
                    {:description "select next" :group :layout})
         (awful.key {1 modkey 2 :Shift} :space
                    (fn [] (awful.layout.inc (- 1)))
                    {:description "select previous" :group :layout})
         (awful.key {1 modkey 2 :Control} :n
                    (fn []
                      (let [c (awful.client.restore)]
                        ;; Focus restored client
                        (when c
                          (set client.focus c)
                          (c:raise))))
                    {:description "restore minimized" :group :client})

         ;; Prompt
         (awful.key {1 modkey} :r
                    (fn [] (: (. (awful.screen.focused) :mypromptbox) :run))
                    {:description "run prompt" :group :launcher})
         (awful.key {1 modkey} :x
                    (fn []
                      (awful.prompt.run {:prompt "Run Lua code: "
                                         :textbox (. (. (awful.screen.focused)
                                                        :mypromptbox)
                                                     :widget)
                                         :exe_callback awful.util.eval
                                         :history_path (.. (awful.util.get_cache_dir)
                                                           :/history_eval)}))
                    {:description "lua execute prompt"
                     :group :awesome})

         ;; Menubar
         (awful.key {1 modkey} :p
                    (fn [] (menubar.show))
                    {:description "show the menubar" :group :launcher})

         ;; Custom
         (awful.key {1 modkey 2 :Shift} :a
                    (fn [] (awful.spawn :/usr/bin/alacritty)))
         (awful.key {1 modkey 2 :Shift} :p
                    (fn [] (awful.spawn :/usr/bin/xfce4-appfinder)))
         (awful.key {1 modkey 2 :Shift} :x
                    (fn [] (awful.spawn "/usr/bin/i3lock -c 000011")))
         (awful.key {1 modkey 2 :Shift} :Return
                    (fn [] (awful.spawn :/usr/bin/termite)))
         (awful.key {1 modkey 2 :Control 3 :Shift} :a
                    (fn [] (awful.spawn "terminator -x sudo arch32")))
         (awful.key {1 modkey 2 :Control 3 :Shift} :c
                    (fn [] (awful.spawn :chromium)))
         (awful.key {1 modkey 2 :Control 3 :Shift} :e
                    (fn [] (awful.spawn "emacsclient -c")))
         (awful.key {1 modkey 2 :Control 3 :Shift} :f
                    (fn [] (awful.spawn :firefox)))
         (awful.key {1 modkey 2 :Control 3 :Shift} :t
                    (fn [] (awful.spawn :terminator)))
         (awful.key {} :XF86AudioLowerVolume
                    (fn [] (awful.spawn "amixer -D pulse sset Master 5%-")))
         (awful.key {} :XF86AudioRaiseVolume
                    (fn [] (awful.spawn "amixer -D pulse sset Master 5%+")))
         (awful.key {} :XF86AudioMute
                    (fn [] (awful.spawn "amixer -D pulse sset Master toggle")))
         (awful.key {} :XF86Sleep
                    (fn [] (awful.spawn "systemctl suspend")))
         (awful.key {} :XF86Calculator
                    (fn [] (awful.spawn :galculator)))
         (awful.key {1 modkey} "="
                    (fn []
                      (awful.spawn "/usr/bin/emacsclient -n -e '(equake-invoke)'")))))

(global clientkeys
        (awful.util.table.join
         (awful.key {1 modkey} :f
                    (fn [c]
                      (set c.fullscreen
                           (not c.fullscreen))
                      (c:raise))
                    {:description "toggle fullscreen" :group :client})
         (awful.key {1 modkey 2 :Shift} :c
                    (fn [c] (c:kill))
                    {:description :close :group :client})
         (awful.key {1 modkey 2 :Control} :space
                    awful.client.floating.toggle
                    {:description "toggle floating" :group :client})
         (awful.key {1 modkey 2 :Control} :Return
                    (fn [c] (c:swap (awful.client.getmaster)))
                    {:description "move to master" :group :client})
         (awful.key {1 modkey} :o
                    (fn [c] (c:move_to_screen))
                    {:description "move to screen" :group :client})
         (awful.key {1 modkey} :t
                    (fn [c] (set c.ontop (not c.ontop)))
                    {:description "toggle keep on top" :group :client})
         (awful.key {1 modkey} :n
                    ;; The client currently has the input focus, so it
                    ;; cannot be minimized, since minimized clients
                    ;; can't have the focus.
                    (fn [c] (set c.minimized true))
                    {:description :minimize :group :client})
         (awful.key {1 modkey} :m
                    (fn [c]
                      (set c.maximized (not c.maximized))
                      (c:raise))
                    {:description :maximize :group :client})))

;;; Bind all key numbers to tags.
;; Be careful: we use keycodes to make it work on any keyboard
;; layout. This should map on the top row of your keyboard, usually 1
;; to 9
(for [i 1 9 1]
  (global globalkeys
          (awful.util.table.join
           globalkeys
           (awful.key {1 modkey} (.. "#" (+ i 9))
                      (fn []
                        (let [screen (awful.screen.focused)
                              tag (. screen.tags i)]
                          (when tag
                            (tag:view_only))))
                      {:description (.. "view tag #" i) :group :tag})
           (awful.key {1 modkey 2 :Control}
                      (.. "#" (+ i 9))
                      (fn []
                        (let [screen (awful.screen.focused)
                              tag (. screen.tags i)]
                          (when tag
                            (awful.tag.viewtoggle tag))))
                      {:description (.. "toggle tag #" i) :group :tag})
           (awful.key {1 modkey 2 :Shift}
                      (.. "#" (+ i 9))
                      (fn []
                        (when client.focus
                          (local tag
                                 (. client.focus.screen.tags i))
                          (when tag
                            (client.focus:move_to_tag tag))))
                      {:description (.. "move focused client to tag #" i) :group :tag})
           (awful.key {1 modkey 2 :Control 3 :Shift}
                      (.. "#" (+ i 9))
                      (fn []
                        (when client.focus
                          (local tag
                                 (. client.focus.screen.tags i))
                          (when tag
                            (client.focus:toggle_tag tag))))
                      {:description (.. "toggle focused client on tag #" i) :group :tag}))))

(global clientbuttons
        (awful.util.table.join
         (awful.button {} 1
                       (fn [c]
                         (set client.focus c)
                         (c:raise)))
         (awful.button {1 modkey} 1 awful.mouse.client.move)
         (awful.button {1 modkey} 3 awful.mouse.client.resize)))

;;; Set keys
(root.keys globalkeys)

;;; Rules
;; Rules to apply to new clients (through the "manage" signal)
(set awful.rules.rules
     ;; All clients match this rule
     {1 {:rule {}
         :properties {:border_width beautiful.border_width
                      :border_color beautiful.border_normal
                      :focus awful.client.focus.filter
                      :raise true
                      :keys clientkeys
                      :buttons clientbuttons
                      :screen awful.screen.preferred
                      :placement (+ awful.placement.no_overlap
                                    awful.placement.no_offscreen)}}

      ;; Floating clients
      2 {:rule_any {:instance {1 :DTA 2 :copyq}
                    :class {1 :Arandr
                            2 :Gpick
                            3 :Kruler
                            4 :MessageWin ; kalarm
                            5 :Sxiv
                            6 :Wpa_gui
                            7 :pinentry
                            8 :veromix
                            9 :xtightvncviewer}
                    :name {1 "Event Tester"} ; xev
                    :role {1 :AlarmWindow    ; Thunderbirds calendar
                           2 :pop-up}}       ; Google Chromes (detached) Developer tools
         :properties {:floating true}}
      ;; Add titlebars to normal clients and dialogs
      3 {:rule_any {:type {1 :normal 2 :dialog}}
         :properties {:titlebars_enabled true}}
      ;; disable titlebars for equake
      4 {:rule {:instance :*EQUAKE* :class :Emacs}
         :properties {:titlebars_enabled false}}

      ;; Screen 1 rules
      5 {:rule {:class :skypforlinux}
         :properties {:screen 1 :tag :1-Main}}
      6 {:rule {:class :slack}
         :properties {:screen 1 :tag :1-Main}}
      7 {:rule {:class :keepass2}
         :properties {:screen 1 :tag :1-Main}}
      8 {:rule {:class :Bluejeans}
         :properties {:screen 1 :tag :9}}
      9 {:rule {:class :Spotify}
         :properties {:screen 1 :tag :9}}

      ;; Screen 2 rules
      10 {:rule {:class :Firefox} :properties {:tag :2-Web}}
      11 {:rule {:class :Chromium}
          :properties {:tag :2-Web}}
      12 {:rule {:class :Google-chrome}
          :properties {:tag :2-Web}}
      13 {:rule {:class :jetbrains-idea-ce}
          :properties {:tag :5}}})

;;; Signals
;; Signal function to execute when a new client appears
(client.connect_signal :manage
                       (fn [c]
                         ;; Set the windows a the slave, i.e., put it
                         ;; a the end of the others instead of setting
                         ;; it master.
                         (when (and (and awesome.startup
                                         (not c.size_hints.user_position))
                                    (not c.size_hints.program_position))
                           ;; Prevent clients from being unreachable
                           ;; after screen count changes
                           (awful.placement.no_offscreen c))))

;; add titlebar if titlebars_enabled is set to true in the rules
(client.connect_signal
 "request::titlebars"
 (fn [c]
   (let [mybuttons (awful.util.table.join
                    (awful.button {} 1
                                  (fn []
                                    (set client.focus c)
                                    (c:raise)
                                    (awful.mouse.client.move c)))
                    (awful.button {} 3
                                  (fnlfmt []
                                          (set client.focus c)
                                          (c:raise)
                                          (awful.mouse.client.resize c))))
         mytitlebar (awful.titlebar c)]
     (mytitlebar:setup
      ;; Left
      {1 {1 (awful.titlebar.widget.iconwidget c)
          :buttons mybuttons
          :layout wibox.layout.fixed.horizontal}
       ;; Middle
       2 {1 {:align :center
             :font "Courier New 14"
             :widget (awful.titlebar.widget.titlewidget c)}
          : buttons
          :layout wibox.layout.flex.horizontal}
       ;; Right
       3 {1 (awful.titlebar.widget.floatingbutton c)
          2 (awful.titlebar.widget.maximizedbutton c)
          3 (awful.titlebar.widget.stickybutton c)
          4 (awful.titlebar.widget.ontopbutton c)
          5 (awful.titlebar.widget.closebutton c)
          :layout (wibox.layout.fixed.horizontal)}
       :layout wibox.layout.align.horizontal}))))

;; Enable sloppy focus, so that focus follows mouse
(client.connect_signal "mouse::enter"
                       (fn [c]
                         (when (and (not= (awful.layout.get c.screen)
                                          awful.layout.suit.magnifier)
                                    (awful.client.focus.filter c))
                           (set client.focus c))))
(client.connect_signal :focus
                       (fn [c]
                         (set c.border_color beautiful.border_focus)))
(client.connect_signal :unfocus
                       (fn [c]
                         (set c.border_color beautiful.border_normal)))

;;; Autostart
(awful.spawn.once :/home/jeffbowman/bin/awesome_startup.sh)
(awful.spawn.with_shell :/home/jeffbowman/bin/fix_vid)
