;;; my-ide-packages.el --- General IDE packages -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Jeff Bowman

;; Author: Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: lisp

;;; Commentary:
;;    Generally useful IDE related packages

;;; Code:

(require 'crafted-ide-packages)

(add-to-list 'package-selected-packages 'diff-hl)
(add-to-list 'package-selected-packages 'dape)
(add-to-list 'package-selected-packages 'eldoc)
(add-to-list 'package-selected-packages 'indent-tools)
(add-to-list 'package-selected-packages 'magit)
(add-to-list 'package-selected-packages 'yaml-mode)

(provide 'my-ide-packages)
;;; my-ide-packages.el ends here
