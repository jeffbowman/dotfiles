;;; my-genai-config.el --- Generative AI configuration  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Jeff Bowman

;; Author: Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Generative AI configuration.  Uses `ellama' as the main package,
;; which has it's own dependencies externally.  Need to install
;; `ollama' on the system, pull a model, and run the ollama service.
;;
;; On Arch Linux this looks like:
;;
;;   sudo pacman -S ollama
;;   sudo systemctl enable ollama.service
;;   sudo systemctl start ollama.service
;;   ollama pull zephyr # N.B. this could be a different model
;;
;; On MacOS this looks like:
;;
;;   brew install ollama
;;   brew services start ollama
;;   ollama pull zephyr # N.B. this could be a different model
;;
;; `ellama' works on a "request" model, meaning it doesn't
;; autocomplete in the same way as `codeium'.  By default, `ellama'
;; binds things to the `C-c e' prefix, with sub-groups for code
;; completion, chat, etc.  These do not seem to be autoloaded
;; functions, so need to require the library to get them enabled.
;;
;;; Code:

;; if possible, enable ellama
(require 'ellama nil :noerror)

(with-eval-after-load "ellama"
  (customize-set-variable 'ellama-keymap-prefix "C-c e")
  (ellama-setup-keymap))


(provide 'my-genai-config)
;;; my-genai-config.el ends here
