;;; my-lisp-config.el --- My enhancements to crafted-lisp-config  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Jeff Bowman

;; Author: Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'adjust-parens)

(with-eval-after-load 'crafted-lisp-config
  (add-hook 'clojure-mode-hook #'adjust-parens-mode)
  (add-hook 'emacs-lisp-mode-hook #'adjust-parens-mode)
  (add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode) ; not already added by crafted-lisp-config
  (add-hook 'lisp-mode-hook #'adjust-parens-mode)
  (add-hook 'scheme-mode-hook #'adjust-parens-mode))

(customize-set-variable 'geiser-chez-binary "chez")

(with-eval-after-load 'geiser-impl
  (add-to-list 'geiser-active-implementations 'gauche)
  (add-to-list 'geiser-active-implementations 'chez)
  (add-to-list 'geiser-implementations-alist '((regexp "\\.gsh\\'") gauche))
  (add-to-list 'geiser-implementations-alist '((dir "/home/jeffbowman/Projects/Scheme/Guile") guile))
  (add-to-list 'geiser-implementations-alist '((dir "/home/jeffbowman/Projects/Scheme/Chez") chez)))

(add-to-list 'auto-mode-alist '("\\.gsh\\'" . scheme-mode))
(add-to-list 'auto-mode-alist '("\\.fnl\\'" . fennel-mode))

(provide 'my-lisp-config)
;;; my-lisp-config.el ends here
