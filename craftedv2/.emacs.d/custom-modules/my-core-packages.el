;;; my-core-packages.el --- Core packages for my configuration  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Jeff Bowman

;; Author: Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Packages I use daily in Emacs

;;; Code:

(add-to-list 'package-selected-packages 'avy)
(add-to-list 'package-selected-packages 'bufferlo)
(add-to-list 'package-selected-packages 'crux)
(add-to-list 'package-selected-packages 'diminish)
(add-to-list 'package-selected-packages 'key-chord)
(add-to-list 'package-selected-packages 'multiple-cursors)
(add-to-list 'package-selected-packages 'noccur)
(add-to-list 'package-selected-packages 'pinentry)
(add-to-list 'package-selected-packages 'which-key)


(provide 'my-core-packages)
;;; my-core-packages.el ends here
