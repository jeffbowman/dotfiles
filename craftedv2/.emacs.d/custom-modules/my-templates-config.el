;;; my-templates-config.el --- Custom Templates     -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Jeff Bowman

;; Author: Jeff Bowman <jeffbowman@archlinux>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Custom templates using `tempo' syntax

;;; Code:

;; use tempo mode to define snippets
(require 'tempo)

(customize-set-variable 'tempo-interactive t) ; allows for prompting
                                              ; when inserting
                                              ; snippets
;; define org header for new org files
(tempo-define-template
 "org-header"
 '("#+STARTUP: lognotereschedule logdrawer overview" n
   "#+SEQ_TODO: TODO(t/!) IN_PROGRESS(i/!) HOLD(h@/!) READY_FOR_QA(r@/!) | DONE(d) CANCELED(c@)" n
   "#+COLUMNS: %50ITEM %CLOCKSUM %CLOCKSUM_T" n)
 "orgheader"
 "Insert standard Org header"
 nil)

(defun my/org-time-stamp ()
  "Call `org-schedule' but swallow the return value."
  (interactive)
  (let ((_ignore (org-time-stamp nil)))
    nil))

;; define a new blog entry (also found in my custom yasnippets)
(tempo-define-template
 "newblog"
 '("#+TITLE: " (p "Title: " title) n
   "#+OPTIONS: toc:nil num:nil todo:nil pri:nil tags:nil ^:nil" n
   "#+DESCRIPTION: " (p "Description: " description) n
   "#+DATE: " (my/org-time-stamp) n n
   p
   n "Tags: " n
   (progn
     (tempo-forward-mark)
     (when (package-installed-p 'writefreely)
       (load-write-freely)
       (write-freely-mode))))
 "newblog"
 "Org configuration for a new blog entry"
 nil)

(require 'calendar)                     ; needed to avoid warnings
                                        ; using calendar functions
(defun my-calendar-week-number ()
  "Return the current week of the year number as a string."
  (number-to-string
   (car (calendar-iso-from-absolute
         (string-to-number (cadr (split-string
                                  (calendar-day-of-year-string (calendar-current-date)))))))))

(defun my-calendar-current-year ()
  "Return the current year as a string."
  (number-to-string (caddr (calendar-current-date))))

;; define weektime for tracking time in projects
(tempo-define-template
 "weektime"
 '("#+BEGIN: clocktable :scope file :block " (my-calendar-current-year) "-w" (my-calendar-week-number)
   " :step day :maxlevel 6 :compact t :narrow 80! :emphasize t :stepskip0 t" n
   "#+END:")
 "weektime"
 "Insert standard week timeclock summary"
 nil)

(defun my/week-time ()
  "Insert dates and run template `weektime'."
  (interactive)
  (let* ((now (calendar-current-date))
         (month (number-to-string (car now)))
         (mm (car now))
         (dd (cadr now))
         (yy (caddr now))

         ;; first Monday before today or today if it is Monday
         (start-day
          (number-to-string
           (cadr (calendar-nth-named-day -1 1 mm yy dd))))

         ;; first Friday after today or today if it is Friday
         (end-day
          (number-to-string
           (cadr
            (calendar-nth-named-day 1 5 mm yy dd)))))

    (insert (format "%s/%s - %s/%s\n" month start-day month end-day))
    (tempo-template-weektime)))

;;; meeting templates,

;; my/org-schedule provided to swallow return value
;; since it is output already and would otherwise output it twice.
(defun my/org-schedule ()
  "Call `org-schedule' but swallow the return value."
  (let ((_ignore (org-schedule nil)))
    nil))

;; Pfizer GTx sprint demo prep
(tempo-define-template
 "sprintdemoprep"
 '("TODO Sprint " (p "Sprint number: " sprint-number) " System Demo Preparation Check-in"
   (my/org-schedule))
 "sprintdemoprep"
 "Insert a Sprint Demo Checkin header"
 nil)

;; generic new meeting template
(tempo-define-template
 "newmeeting"
 '("TODO " (p "Meeting name: " meeting-name) n>
   (my/org-time-stamp))
 "newmeeting"
 "Org new meeting entry"
 nil)


;; define bills table for personal bill tracking
(tempo-define-template
 "billtable"
 '("| Account             | Due Date | Date Paid | Amount Due | Amount Paid | Total |" n>
   "|---------------------+----------+-----------+------------+-------------+-------|" n>
   "| OG&E                |          |           |            |             |       |" n>
   "| Capitol One         |          |           |            |             |       |" n>
   "| ONG                 |          |           |            |             |       |" n>
   "| Amex Savings        |          |           |            |             |       |" n>
   "| USAA Visa           |          |           |            |             |       |" n>
   "| American Express    |          |           |            |             |       |" n>
   "|                     |          |           |            |             |       |" n>
   "| USAA Visa           |          |           |            |             |       |" n>
   "| American Express    |          |           |            |             |       |" n>
   "| OKC Utilities       |          |           |            |             |       |" n>
   "| Amex Loan           |          |           |            |             |       |" n>
   "| Amex Savings        |          |           |            |             |       |" n>
   "|                     |          |           |            |             |       |" n>
   "#+TBLFM: @8$6=vsum(@2$5..@8$5)::@>$6=vsum(@9$5..@>>$5)" n
   )
 "billtable"
 "Org mode table for monthly bills"
 nil)

(define-key global-map (kbd "C-c t c") #'tempo-complete-tag)

;; Set an abbrev to expand to the tempo snippet above, but have to
;; wrap these to avoid errors where the abbrev table is not created
;; when Emacs starts.
(with-eval-after-load "org"
  (define-abbrev org-mode-abbrev-table "billtable" "" 'tempo-template-billtable)
  (define-abbrev org-mode-abbrev-table "newblog" "" 'tempo-template-newblog)
  (define-abbrev org-mode-abbrev-table "orgheader" "" 'tempo-template-org-header)
  (define-abbrev org-mode-abbrev-table "weektime" "" 'tempo-template-weektime))

(provide 'my-templates-config)
;;; my-templates-config.el ends here
