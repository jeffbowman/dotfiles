;;; init.el --- Emacs initializaiont -*- mode: emacs-lisp; lexical-binding: t; -*-
;;; Commentary:
;;; Code:
;;;_* Initial phase.

;;;_ * Custom file

;; Load the custom file if it exists.  Among other settings, this will
;; have the list `package-selected-packages', so we need to load that
;; before adding more packages.  The value of the `custom-file'
;; variable must be set appropriately, by default the value is nil.
;; This can be done here, or in the early-init.el file.
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (and custom-file
           (file-exists-p custom-file))
  (load custom-file nil 'nomessage))

;;;_ * Update `load-path'
;; Adds crafted-emacs modules to the `load-path', sets up a module
;; writing template, sets the `crafted-emacs-home' variable.
(load "~/crafted-emacs/modules/crafted-init-config")

;;;_* Packages phase

;;;_ * Crafted Package Bundles

;;;_ * Local Package Bundles
(require 'my-core-packages)
(require 'my-blog-packages)
(require 'my-completion-packages)
(require 'my-genai-packages)
(require 'my-ide-packages)
(require 'my-ide-python-packages)
(require 'my-lisp-packages)

;;;_ * Local Packages
(add-to-list 'package-selected-packages 'bbdb)              ;gnus
(add-to-list 'package-selected-packages 'ef-themes)         ;themes
(add-to-list 'package-selected-packages 'modus-themes)      ;themes
(add-to-list 'package-selected-packages 'sxhkdrc-mode)      ;config
(add-to-list 'package-selected-packages 'vagrant)           ;remote
(add-to-list 'package-selected-packages 'vagrant-tramp)     ;remote

;;;_ * Install the packages listed in the `package-selected-packages' list.
(package-install-selected-packages :noconfirm)

;;;_* Configuration phase
;;;_

;;;_ * Constants
;; Check the system used
(defconst ON-WINDOWS (memq system-type '(cygwin windows-nt ms-dos)))

(when (and (memq system-type '(darwin))
           (executable-find "gls"))
  (setq insert-directory-program "gls")
  (customize-set-variable 'dired-listing-switches "-alF --group-directories-first"))


;; (when (package-installed-p 'modus-themes)
;;   (load-theme 'modus-vivendi-tinted t)
;;   (customize-set-variable 'modus-themes-completions
;;                           '((matches background intense)
;;                             (selection accented intense)
;;                             (popup accented intense))))

;;;_ * Crafted Emacs Modules
(require 'crafted-defaults-config)
(require 'crafted-lisp-config)
(require 'crafted-speedbar-config)
(require 'crafted-startup-config)
(require 'crafted-writing-config)

;; needs to be after `crafted-startup' or the variable
;; `crafted-startup-inhibit-splash' isn't defined.
(unless crafted-startup-inhibit-splash
  (setq initial-buffer-choice #'crafted-startup-screen))

;;;_ * Local Custom Modules
(require 'my-completion-config)
(require 'my-custom-config)
(require 'my-genai-config)
(require 'my-ide)
(require 'my-ide-python)
(require 'my-lisp-config)
(require 'my-org-config)
(require 'my-templates-config)

;;;_ * Theme configuration

(cond
 ((package-installed-p 'ef-themes)
  (reload-ef-bio))
 ((package-installed-p 'modus-themes)
  (customize-set-variable 'modus-themes-completions
                          '((matches background intense)
                            (selection accented intense)
                            (popup accented intense)))
  (reload-modus-vivendi)))              ; from my-custom-config

;;;_* Personal configuration
;;;

;;;_ * whitespace
(crafted-writing-configure-whitespace nil nil '(prog-mode text-mode))


;;;_ * dired
(customize-set-variable 'dired-dwim-target t)

;;;_* General customization
(column-number-mode t)
(delete-selection-mode t)
(electric-pair-mode t)
(show-paren-mode t)
(global-hl-line-mode t)

(put 'narrow-to-region 'disabled nil)
(put 'upcase-region    'disabled nil)
(put 'downcase-region  'disabled nil)
(put 'scroll-left      'disabled nil)

(unless (version< emacs-version "28")
  (repeat-mode 1))

(add-hook 'prog-mode-hook #'which-function-mode)

(customize-set-variable 'display-time-24hr-format t)
(display-time-mode t)

;;;_ * Tempo snippets, autoinsert and skeletons
(auto-insert-mode)

(add-hook 'find-file-hook #'auto-insert)

;; Don't want to be prompted before insertion:
(customize-set-variable 'auto-insert-query nil)
(customize-set-variable 'auto-insert-directory
                        (expand-file-name "templates" user-emacs-directory))
;; Historical configuration from when I used the venerable `yasnippet'
;; to handle auto insert/file templates
;;
;; (define-auto-insert "\\.el?$" ["default-elisp.el" mysnippet-autoinsert-yas-expand])
;; (define-auto-insert "pom.xml" ["default-pom.xml" mysnippet-autoinsert-yas-expand])

;; uses builtin auto-insert mechanism, not yasnippets as above
(with-eval-after-load "autoinsert"
  ;; if it is an interview, load a blank interview file.
  (define-auto-insert
    ".*/interviews/[a-z_-]+[0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}.org"
    (expand-file-name "templates/blank-interview.org" user-emacs-directory) nil)
  (define-auto-insert
    ".*/work/appnovation/\\(projects\\|mulesoft\\)/.*/.*\\.org"
    (expand-file-name "templates/newproject.org" user-emacs-directory)))

(setq-default abbrev-mode t)

;;;_* General keybindings

;; zap-up-to-char is more useful
(define-key global-map (kbd "M-z")           #'zap-up-to-char)
(define-key global-map (kbd "C-+")           #'text-scale-increase)
(define-key global-map (kbd "C--")           #'text-scale-decrease)
(define-key global-map (kbd "<f5>")          #'execute-extended-command)
(define-key global-map (kbd "<f6>")          #'isearch-forward)
(define-key global-map [remap zap-to-char]   #'zap-up-to-char)
(define-key global-map [remap upcase-word]   #'upcase-dwim)
(define-key global-map [remap downcase-word] #'downcase-dwim)
(define-key global-map (kbd "C-h K")         #'describe-keymap)

;;;_ * Window navigation
(defvar my-windows-prefix-key "C-c w"
  "Configure the prefix key for `my-windows' bindings.")

(define-prefix-command 'my-windows-key-map)

(define-key 'my-windows-key-map (kbd "n") 'windmove-down)
(define-key 'my-windows-key-map (kbd "p") 'windmove-up)
(define-key 'my-windows-key-map (kbd "b") 'windmove-left)
(define-key 'my-windows-key-map (kbd "f") 'windmove-right)

(global-set-key (kbd my-windows-prefix-key) 'my-windows-key-map)


;;;_* Installed package configuration
(add-hook 'prog-mode
          (lambda ()
            (hs-minor-mode)
            (flymake-mode)
            (editorconfig-mode)))

(with-eval-after-load "editorconfig"
  (diminish 'editorconfig-mode "EC"))

;;;_ * bbdb - big brother insidious database contacts rolodex
(when (require 'bbdb nil :noerror)
  (bbdb-initialize 'gnus 'message)
  (bbdb-mua-auto-update-init 'gnus 'message)
  (setq bbdb-mua-action 'query)
  (add-hook 'gnus-startup-hook #'bbdb-insinuate-gnus)

  ;; set the bbdb window size, since bbdb does not use the
  ;; `display-buffer-alist' configuration.
  (customize-set-variable 'bbdb-pop-up-window-size 5))

;;;_  ^ eudc - ldap like lookup for contacts etc
;; only load when loading gnus for email etc.
(with-eval-after-load 'gnus
  (when (require 'message nil :noerror)
    (keymap-set message-mode-map "C-c <tab>" #'eudc-expand-inline))
  (with-eval-after-load 'sendmail
    (keymap-set message-mode-map "C-c <tab>" #'eudc-expand-inline)))

(with-eval-after-load 'eudc
  (require 'eudcb-bbdb nil :noerror))

;; add a hook for dired to be able to attach files from there to an
;; email. See info node (gnus)"Other modes" [[info:gnus#Other modes][gnus#Other modes]]
(add-hook 'dired-mode-hook #'turn-on-gnus-dired-mode)

(customize-set-variable 'eudc-server-hotlist
                        '(("" . bbdb)))


;;;_ * Buffer handling configuration
(require 'ibuffer)
(bufferlo-mode)                         ; list buffers local to tabs
(with-eval-after-load 'bufferlo
  (define-key global-map (kbd "C-x C-b") #'bufferlo-ibuffer))
(customize-set-variable 'ibuffer-movement-cycle nil)
(customize-set-variable 'ibuffer-old-time 24)
(add-hook 'ibuffer-hook
          (lambda ()
            (setq ibuffer-filter-groups (ibuffer-project-generate-filter-groups))
            (unless (eq ibuffer-sorting-mode 'project-file-relative)
              (ibuffer-do-sort-by-project-file-relative))))

;;;_ * Navigation

;; crux is for some nice utilities
(require 'crux)
(with-eval-after-load "avy"
  (avy-setup-default))
(with-eval-after-load "crux"
  (key-chord-define-global "ji" #'avy-goto-char-in-line)
  (key-chord-define-global "jj" #'avy-goto-word-1)
  (key-chord-define-global "jl" #'avy-goto-line)
  (key-chord-define-global "jk" #'avy-goto-char-timer)
  (key-chord-define-global "JJ" #'switch-to-prev-buffer)
  (key-chord-define-global "JK" #'switch-to-next-buffer)
  (key-chord-define-global "xx" #'execute-extended-command)
  (key-chord-mode +1)

  (define-key global-map [remap move-beginning-of-line] #'crux-move-beginning-of-line)

  ;; Don't need crux to provide this with Emacs29 which provides
  ;; `duplicate-line' and `duplicate-dwim' functions
  (if (version< emacs-version "29")
      (define-key global-map (kbd "C-c d") #'crux-duplicate-current-line-or-region)
    (keymap-set global-map "C-c d" #'duplicate-line))

  ;; Emacs 29 now provides this functionality
  (if (version< emacs-version "29")
      (define-key global-map (kbd "C-c f") #'crux-recentf-find-file)
    (keymap-set global-map "C-c f" #'recentf-open))
  (define-key global-map (kbd "C-c k") #'crux-kill-other-buffers)
  (define-key global-map (kbd "C-c n") #'crux-cleanup-buffer-or-region))

;;;_ * which-key
(customize-set-variable 'which-key-show-early-on-C-h t)
(customize-set-variable 'which-key-idle-delay 1.0)
(customize-set-variable 'which-key-idle-secondary-delay 0.05)
(which-key-mode)
(with-eval-after-load "which-key"
  (which-key-setup-side-window-right-bottom))

;;;_ * writefreely

;; This is called in the yasnippet snippet to create a new blog entry.
(defun load-write-freely ()
  "Loads writefreely.  Use similar to a hook function"
  (interactive)
  (unless (featurep 'writefreely)
    (package-activate 'writefreely))
  (writefreely-mode))

;; Once writefreely is loaded, get the auth-token so blogs published
;; will show up correctly.
(with-eval-after-load "writefreely"
  (require 'auth-source)
  (customize-set-variable 'writefreely-auth-token
                          (auth-source-pick-first-password :host "write.as")))


;;;_ * Pin Entry for use with GnuPG et.al.
;; pinentry -- used by vc-mode & magit
;; gpg config to use pinentry from the minibuffer
(customize-set-variable 'epg-pinentry-mode 'loopback)

;; MS Windows does not support local sockets, so this won't work
;; there.
(unless ON-WINDOWS (pinentry-start))

;;;_ * magit
(customize-set-variable 'magit-diff-refine-hunk 'all)
(define-key global-map (kbd "C-x g") #'magit-status)

;;;_ ^ diff-hl
;; Add diff-hl to vc-mode command prefix
(customize-set-variable 'diff-hl-command-prefix (kbd "C-x v"))
(global-diff-hl-mode)
(add-hook 'magit-process-find-password-functions
          #'magit-process-password-auth-source)
(add-hook 'magit-post-refresh-hook #'diff-hl-magit-post-refresh)

;;;_ * YAML
;; hydra to help navigate YAML files
(add-hook 'yaml-mode-hook
          (lambda () (define-key yaml-mode-map (kbd "C-c >") #'indent-tools-hydra/body)))

;;;_ * multiple-cursors
(defvar my-multiple-cursors-prefix-key "C-c m"
  "Configure the prefix key for `my-multiple-cursors' bindings.")

(define-prefix-command 'my-multiple-cursors-key-map)
(define-key 'my-multiple-cursors-key-map (kbd "m") #'mc/mark-all-like-this-dwim)
(define-key 'my-multiple-cursors-key-map (kbd "r") #'mc/mark-all-in-region)
(define-key 'my-multiple-cursors-key-map (kbd "R") #'mc/mark-all-in-region-regexp)

(global-set-key (kbd my-multiple-cursors-prefix-key) 'my-multiple-cursors-key-map)

(define-key global-map (kbd "C-c >") #'mc/mark-next-like-this)
(define-key global-map (kbd "C-c <") #'mc/mark-previous-like-this)
(define-key global-map (kbd "C-c s") #'mc/mark-next-symbol-like-this)
(define-key global-map (kbd "C-c S") #'mc/mark-previous-symbol-like-this)
(define-key global-map (kbd "C-c n") #'mc/skip-to-next-like-this)
(define-key global-map (kbd "C-c p") #'mc/skip-to-previous-like-this)
(define-key global-map (kbd "C-c #") #'mc/insert-numbers)
(define-key global-map (kbd "C-c l") #'mc/insert-letters)


;;;_ * php-mode
(add-hook 'php-mode-hook #'eglot-ensure)

;;;_ * java-mode
(add-hook 'java-mode-hook #'eglot-java-mode)
(add-hook 'eglot-java-mode-hook
          (lambda ()
            (define-key eglot-java-mode-map (kbd "C-c j n") #'eglot-java-file-new)
            (define-key eglot-java-mode-map (kbd "C-c j r") #'eglot-java-run-main)
            (define-key eglot-java-mode-map (kbd "C-c j t") #'eglot-java-run-test)
            (define-key eglot-java-mode-map (kbd "C-c j b") #'eglot-java-project-build-task)
            (define-key eglot-java-mode-map (kbd "C-c j B") #'eglot-java-project-build-refresh)))

(with-eval-after-load 'eglot
  (define-key eglot-mode-map (kbd "C-c e a") #'eglot-code-actions))

;;;_ * UI tweaks
(defun pulse-line (&rest _)
  "Pulse the current line."
  (pulse-momentary-highlight-one-line (point)))

(dolist (command '(scroll-up-command scroll-down-command
                                     recenter-top-bottom other-window))
  (advice-add command :after #'pulse-line))

;;;_ * Tab bar
(customize-set-variable 'tab-bar-new-tab-choice "*scratch*")
(customize-set-variable 'tab-bar-new-tab-to 'rightmost)
(customize-set-variable 'tab-bar-select-tab-modifiers '(control meta))
(customize-set-variable 'tab-bar-tab-hints t)

;;;_ * Info
(let ((my-info-dir (expand-file-name "info/dir" user-emacs-directory)))
  (when (file-exists-p my-info-dir)
    (require 'info)
    (info-initialize)
    (push (file-name-directory my-info-dir) Info-directory-list)))

;;;_* Profile emacs startup

;; profiler support, not needed normally
;; start emacs with `-l profiler --eval "(profiler-start 'cpu+mem)"'
(require 'profiler)
(add-hook 'after-init-hook #'profiler-stop)

;; mention how long it took for Emacs to start
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Crafted Emacs loaded in %s."
                     (emacs-init-time))))

;; Set default coding system (especially for Windows)
(set-default-coding-systems 'utf-8)

(provide 'init)
;;; init.el ends here
