#!/usr/bin/env sh

killall -q polybar

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

polybar panel-primary &

HAVE_EXTERNAL_MONITOR=$(xrandr --query | grep 'HDMI1')
if [[ $HAVE_EXTERNAL_MONITOR = *connected* ]]; then
    polybar panel-external &
fi
