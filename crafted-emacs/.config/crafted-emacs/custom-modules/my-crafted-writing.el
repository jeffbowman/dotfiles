;;; my-crafted-writing.el --- My Crafted Writing Module  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jeff Bowman

;; Author: Jeff Bowman <jeffbowman@archlinux>
;; Keywords: lisp, lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Writing module for prose

;;; Code:

(require 'crafted-latex)
(crafted-package-install-package 'langtool)

;; Neat idea, doesn't quite work, not sure why.  Without the
;; `langtool-bin' variable set, it errors with the following:
;;
;; LanguageTool exited abnormally with code 1 (Error: Unable to
;; initialize main class org.languagetool.commandline.Main)
;;
;; Not sure how to make it start a server. There is no documentation
;; other than the commentary at the top of `langtool.el', so will have
;; to do more research / debugging.

(customize-set-variable 'langtool-java-classpath
                        "/usr/share/java/languagetool/*")

(setq langtool-language-tool-jar
      "/usr/share/java/languagetool/languagetool-commandline.jar")

(setq langtool-language-tool-server-jar
      "/usr/share/java/languagetool/languagetool-server.jar"
      langtool-server-user-arguments '("-p" "8082"))

(customize-set-variable 'langtool-bin "/usr/bin/languagetool")
(require 'langtool)

(provide 'my-crafted-writing)
;;; my-crafted-writing.el ends here
