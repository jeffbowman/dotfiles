;;; my-crafted-completion.el -*- mode: emacs-lisp; mode: outline-minor; lexical-binding: t; -*-

;; Copyright (C) 2022

;; Author:  Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary

;; My preferences for completion, prefers completion from
;; `crafted-mastering-emacs' (thus fido-vertical-mode)

;;; Code:
(require 'crafted-mastering-emacs)

(crafted-package-install-package 'cape)
(crafted-package-install-package 'consult) ; for consult-imenu, consult-line, consult-org-heading
(crafted-package-install-package 'corfu)
(crafted-package-install-package 'embark)
(crafted-package-install-package 'embark-consult)
(crafted-package-install-package 'orderless)
(crafted-package-install-package 'prescient)


;;; Cape
(require 'cape)
(define-key global-map (kbd "C-c c p") #'completion-at-point)
(define-key global-map (kbd "C-c c l") #'cape-line)


;;; Consult
;; (global-set-key (kbd "C-s") #'consult-line)
(define-key minibuffer-local-map (kbd "C-r") #'consult-history)
(with-eval-after-load 'org
  (define-key org-mode-map (kbd "C-c C-j") #'consult-org-heading))

(setq completion-in-region-function #'consult-completion-in-region)

(add-hook 'prog-mode-hook
            (lambda ()
              ;; TODO: Emacs 29 provides a global binding `M-g i'
              ;; bound to imenu.  This may still be a better choice,
              ;; will evaluate after Emacs 29 is released.
              (when (version< emacs-version "29")
                  (define-key prog-mode-map (kbd "C-c '") #'consult-imenu))))

;;; Completion and Corfu
(customize-set-variable 'completion-cycle-threshold 3)
(customize-set-variable 'tab-always-indent 'complete)
;; disables auto completion, maybe I don't want to do that?
;; (customize-set-variable 'corfu-auto nil)

(define-key global-map [remap dabbrev-expand] #'hippie-expand)

(require 'corfu)
;; Setup corfu for popup like completion
(customize-set-variable 'corfu-cycle t)                 ; Allows cycling through candidates
(customize-set-variable 'corfu-auto t)                  ; Enable auto completion
(customize-set-variable 'corfu-auto-prefix 2)           ; Complete with less prefix keys
(customize-set-variable 'corfu-auto-delay 0.0)          ; No delay for completion
(customize-set-variable 'corfu-echo-documentation 0.25) ; Echo docs for current completion option

(global-corfu-mode 1)
(corfu-popupinfo-mode 1)
(eldoc-add-command #'corfu-insert)
(define-key corfu-map (kbd "M-p") #'corfu-popupinfo-scroll-down)
(define-key corfu-map (kbd "M-n") #'corfu-popupinfo-scroll-up)
(define-key corfu-map (kbd "M-d") #'corfu-popupinfo-toggle)

;; turn off Corfu in eshell, pcomplete is broken in Emacs28 but seems
;; to be fixed in Emacs29.  The issue is when entering a double-quote
;; character, a TAB character is automatically entered as well.  This
;; workaround solves the problem.
;; see https://github.com/minad/corfu/issues/204
(add-hook 'eshell-mode-hook
          (lambda ()
            (setq-local corfu-auto nil)
            (corfu-mode)))

;;; Embark
;; additional keybinding for terminal based Emacs
(global-set-key (kbd "C-c .") 'embark-act)


;;; Orderless

;; Set up Orderless for better fuzzy matching
(require 'orderless)
(customize-set-variable 'completion-styles '(prescient orderless basic))
(customize-set-variable 'completion-category-overrides '((file (styles . (partial-completion)))))


;;; Prescient
(require 'prescient)
(customize-set-variable 'prescient-save-file
                        (expand-file-name "prescient-save.el" crafted-config-var-directory))
(prescient-persist-mode +1)


(provide 'my-crafted-completion)
;;; crafted-completion.el ends here
