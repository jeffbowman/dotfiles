;;; my-crafted-ide.el --- My IDE Configuration       -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jeff Bowman

;; Author: Jeff Bowman <jeffbowman@archlinux>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Enhance /Crafted-Emacs/ IDE configuration by using `tree-sitter'
;; and related projects.  Requires the `tree-sitter' external programs
;; to be installed.

;;; Code:

(crafted-package-install-package 'tree-sitter)
(crafted-package-install-package 'tree-sitter-indent)
(crafted-package-install-package 'tree-sitter-ispell)
;; tree-sitter-langs is broken for version 0.12.10, the build process
;; did not put a release file for the grammars to download, see this
;; issue:
;; https://github.com/emacs-tree-sitter/tree-sitter-langs/issues/154
;;
;; [2023-01-01] installed 0.12.8 version (latest version with
;; downloadable grammars), pinned it to this version in
;; `package-load-list' and can revert that whenever the build gets
;; fixed.
;; steps:
;; (quelpa '(tree-sitter-langs :fetcher github :repo emacs-tree-sitter/tree-sitter-langs :commit "bf12547"))
;; then rename elpa/tree-sitter-langs-<date.xxxx> elpa/tree-sitter-langs-0.12.8
;; edit elpa/tree-sitter-langs-0.12.8/tree-sitter-langs-pkg.el change the version to 0.12.8
;; restart emacs
(crafted-package-install-package 'tree-sitter-langs)

(require 'tree-sitter-indent)

;; enhance python indentation with tree-sitter-indentation
(tree-sitter-require 'python)

;; enhance all programming modes by turning on tree-sitter
(add-hook 'prog-mode #'tree-sitter-mode)


(provide 'my-crafted-ide)
;;; my-crafted-ide.el ends here
