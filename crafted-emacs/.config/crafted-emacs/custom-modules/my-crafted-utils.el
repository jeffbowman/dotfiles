;;; my-crafted-utils.el --- My Crafted Utilities

;;; Commentary:
;; Some utility fucntions.  Bits of this work is from the book:
;; "Hacking your way around Emacs" by Marcin Borkowski

;;; Code:

;; move-text replacement of moving a line (or lines with a prefix
;; argument) down or up. Code from "Hacking your way around Emacs"
(defun move-line-down (count)
  "Move a line of text down.

With a prefix argument, move the current line down that many
lines."
  (interactive "*p")
  (unless (zerop count)
    (save-excursion
      (push (point) buffer-undo-list)
      (forward-line (if (> count 0) 1 0))
      (let ((region-to-move-up
             (delete-and-extract-region
              (point)
              (save-excursion
                (forward-line count)
                (point)))))
        (forward-line (if (> count 0) -1 1))
        (insert-before-markers region-to-move-up)
        (indent-according-to-mode)))))

;; From the book "Hacking your way around Emacs"
(defun move-line-up (count)
  "Move a line of text up.

With a prefix argument, move the current line up that many
lines."
  (interactive "*p")
  (move-line-down (- count)))

(provide 'my-crafted-utils)

;;; my-crafted-utils.el ends here
