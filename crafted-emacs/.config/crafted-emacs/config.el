;;;; config.el --- crafted emacs custom config      -*- mode: emacs-lisp; mode: outline-minor; lexical-binding: t; -*-

;; Copyright (C) 2022

;; Author:  Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WAANTY; without even the implied warranty of
;; MECHANTABILITY or FITNESS FO A PATICULA PUPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Personal configuration. Use C-x [ to page forward or C-x ] to page
;; backward through the sections. Outline minor mode is also active,
;; C-c @ is the key prefix.

;;; Code:

;; more code can go here for setting up treesit, combobulate and
;; programming modes using tree-sitter
(when (>= (string-to-number emacs-version) 29)
  (require 'treesit)
  (unless (package-installed-p 'combobulate)
    (package-vc-install "https://github.com/mickeynp/combobulate" :last-release 'Git))
  (require 'combobulate)

  ;; for example, python mode
  (add-to-list 'major-mode-remap-alist '(python-mode . python-ts-mode))
  (add-hook 'python-ts-mode 'combobulate-mode))

;;; Crafted Emacs Modules
(require 'crafted-compile)
(require 'crafted-defaults)
(require 'crafted-editing)
(require 'crafted-ide)
(require 'crafted-latex)
(require 'crafted-lisp)
(require 'crafted-pdf-reader)
(require 'crafted-project)
(require 'crafted-python)

;; Custom Modules

;; my-crafted-completion loads crafted-mastering-emacs which
;; configures ispell, configuring ispell here before it is loaded in
;; crafted-mastering-emacs.
;;
;; not needed - this variable does this work already
;; (customize-set-variable 'ispell-program-name "/usr/bin/aspell")

;; causes Emacs to hammer the CPU for some reason when starting.
;; (customize-set-variable
;;  'ispell-personal-dictionary (expand-file-name ".aspell-personal"
;;                                                crafted-config-var-directory))
(require 'my-crafted-completion)
(require 'my-crafted-ide)
(require 'my-crafted-org)
(require 'my-crafted-ui)
(require 'my-crafted-templates)


;;; module configuration

(with-eval-after-load "crafted-defaults"
  (if (boundp 'use-short-answers)
      (setq use-short-answers nil)
    (advice-remove 'yes-or-no-p #'y-or-n-p))

  (with-eval-after-load 'eudc
    (require 'eudcb-bbdb)               ; bbdb support
    (crafted-defaults--sensible-path crafted-config-etc-directory
                                     'eudc-options-file "eudc-options"))

  (when (package-installed-p 'bbdb)
    (with-eval-after-load "bbdb"
      (crafted-defaults--sensible-path crafted-config-etc-directory
                                       'bbdb-file "bbdb" )))


  (with-eval-after-load "calendar"
    (crafted-defaults--sensible-path crafted-config-etc-directory
                                     'diary-file "diary")))

(with-eval-after-load "crafted-editing"
  (add-to-list 'crafted-editing-whitespace-cleanup-enabled-modes 'prog-mode)
  (add-to-list 'crafted-editing-whitespace-cleanup-enabled-modes 'text-mode))

(with-eval-after-load "crafted-lisp"
  (crafted-package-install-package 'geiser-gauche)
  (crafted-package-install-package 'geiser-chez)
  (crafted-package-install-package 'fennel-mode)

  (customize-set-variable 'geiser-chez-binary "chez")

  (with-eval-after-load 'geiser-impl
    (add-to-list 'geiser-active-implementations 'gauche)
    (add-to-list 'geiser-active-implementations 'chez)
    (add-to-list 'geiser-implementations-alist '((regexp "\\.gsh\\'") gauche))
    (add-to-list 'geiser-implementations-alist '((dir "/home/jeffbowman/Projects/Scheme/Guile") guile))
    (add-to-list 'geiser-implementations-alist '((dir "/home/jeffbowman/Projects/Scheme/Chez") chez)))

  (add-to-list 'auto-mode-alist '("\\.gsh\\'" . scheme-mode))
  (add-to-list 'auto-mode-alist '("\\.fnl\\'" . fennel-mode)))

(with-eval-after-load "my-crafted-ui"
  (customize-set-variable 'crafted-ui-line-numbers-disabled-modes nil)
  (customize-set-variable 'crafted-ui-line-numbers-enabled-modes
                          (add-to-list 'crafted-ui-line-numbers-enabled-modes 'org-mode))
  (customize-set-variable 'crafted-ui-display-line-numbers t))

(with-eval-after-load "eww"
  (customize-set-variable 'eww-bookmarks-directory crafted-config-var-directory))


;;; general customization
(customize-set-variable 'dired-dwim-target t)

(column-number-mode t)
(delete-selection-mode t)
(electric-pair-mode t)
(show-paren-mode t)
(global-hl-line-mode t)

(put 'narrow-to-region 'disabled nil)
(put 'upcase-region    'disabled nil)
(put 'downcase-region  'disabled nil)
(put 'scroll-left      'disabled nil)

(unless (version< emacs-version "28")
  (repeat-mode 1))

(add-hook 'prog-mode-hook #'which-function-mode)

(customize-set-variable 'display-time-24hr-format t)
(display-time-mode t)


;;; tempo snippets, autoinsert and skeletons
(auto-insert-mode)

(add-hook 'find-file-hook #'auto-insert)

;; Don't want to be prompted before insertion:
(customize-set-variable 'auto-insert-query nil)
(customize-set-variable 'auto-insert-directory
                        (expand-file-name "templates" crafted-config-etc-directory))
;; (define-auto-insert "\\.el?$" ["default-elisp.el" mysnippet-autoinsert-yas-expand])
;; (define-auto-insert "pom.xml" ["default-pom.xml" mysnippet-autoinsert-yas-expand])

;; uses builtin auto-insert mechanism, not yasnippets as above
(with-eval-after-load "autoinsert"
  ;; if it is an interview, load a blank interview file.
  (define-auto-insert
    ".*/interviews/[a-z_-]+[0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}.org"
    (expand-file-name "templates/blank-interview.org" crafted-config-etc-directory) nil)
  (define-auto-insert
    ".*/work/appnovation/\\(projects\\|mulesoft\\)/.*/.*\\.org"
    (expand-file-name "templates/newproject.org" crafted-config-etc-directory)))

;;; snippets
;; use built-in tempo mode to create snippets, a little more verbose
;; than yasnippet, but only barely, and used with abbrev-mode
;; turn on abbrev-mode everywhere
(setq-default abbrev-mode t)
(customize-set-variable 'abbrev-file-name
                        (expand-file-name "abbrev_defs" crafted-config-var-directory))


;;; general keybindings

;; zap-up-to-char is more useful
(define-key global-map (kbd "M-z")           #'zap-up-to-char)
(define-key global-map (kbd "C-+")           #'text-scale-increase)
(define-key global-map (kbd "C--")           #'text-scale-decrease)
(define-key global-map (kbd "<f5>")          #'execute-extended-command)
(define-key global-map (kbd "<f6>")          #'isearch-forward)
(define-key global-map [remap zap-to-char]   #'zap-up-to-char)
(define-key global-map [remap upcase-word]   #'upcase-dwim)
(define-key global-map [remap downcase-word] #'downcase-dwim)

(defcustom my-windows-prefix-key "C-c w"
  "Configure the prefix key for `my-windows' bindings."
  :type '(string)
  :group 'crafted-ui)

(define-prefix-command 'my-windows-key-map)

(define-key 'my-windows-key-map (kbd "n") 'windmove-down)
(define-key 'my-windows-key-map (kbd "p") 'windmove-up)
(define-key 'my-windows-key-map (kbd "b") 'windmove-left)
(define-key 'my-windows-key-map (kbd "f") 'windmove-right)

(global-set-key (kbd my-windows-prefix-key) 'my-windows-key-map)


;;; initial package installation
(crafted-package-install-package 'avy)
(crafted-package-install-package 'crux)
(crafted-package-install-package 'diff-hl)
(crafted-package-install-package 'diminish)
(crafted-package-install-package 'editorconfig)
(crafted-package-install-package 'eldoc)
(crafted-package-install-package 'eglot-java)
(crafted-package-install-package 'flymake-proselint)
(crafted-package-install-package 'ibuffer-project)
(crafted-package-install-package 'indent-tools)
(crafted-package-install-package 'key-chord)
(crafted-package-install-package 'lua-mode)
(crafted-package-install-package 'magit)
(crafted-package-install-package 'multiple-cursors)
(crafted-package-install-package 'noccur)
(crafted-package-install-package 'php-mode)
(crafted-package-install-package 'pinentry)
(crafted-package-install-package 'sxhkdrc-mode)
(crafted-package-install-package 'verb)
(crafted-package-install-package 'web-mode) ; needed for php blade templates
(crafted-package-install-package 'which-key)
(crafted-package-install-package 'writefreely)
(crafted-package-install-package 'yaml-mode)


;;; installed package configuration
(add-hook 'prog-mode
          (lambda ()
            (hs-minor-mode)
            (flymake-mode)
            (editorconfig-mode)))

(with-eval-after-load "editorconfig"
  (diminish 'editorconfig-mode "EC"))

;;; bbdb - big brother insidious database contacts rolodex
(when (featurep 'bbdb)
  (bbdb-initialize 'gnus 'message)
  (bbdb-mua-auto-update-init 'gnus 'message)
  (setq bbdb-mua-action 'query)
  (add-hook 'gnus-startup-hook #'bbdb-insinuate-gnus))

;;; eudc - ldap like lookup for contacts etc
;; only load when loading gnus for email etc.
(with-eval-after-load 'gnus
  (with-eval-after-load 'message
    (define-key message-mode-map [(control ?c) (tab)] #'eudc-expand-inline))
  (with-eval-after-load 'sendmail
    (define-key message-mode-map [(control ?c) (tab)] #'eudc-expand-inline)))

;; add a hook for dired to be able to attach files from there to an
;; email. See info node (gnus)"Other modes" [[info:gnus#Other modes][gnus#Other modes]]
(add-hook 'dired-mode-hook #'turn-on-gnus-dired-mode)

;;; flymake-proselint
(add-hook 'text-mode-hook (lambda ()
                            (flymake-mode)
                            (flymake-proselint-setup)))

;;; ibuffer for list-buffers
(require 'ibuffer)
(customize-set-variable 'ibuffer-movement-cycle nil)
(customize-set-variable 'ibuffer-old-time 24)
(add-hook 'ibuffer-hook
          (lambda ()
            (setq ibuffer-filter-groups (ibuffer-project-generate-filter-groups))
            (unless (eq ibuffer-sorting-mode 'project-file-relative)
              (ibuffer-do-sort-by-project-file-relative))))
(define-key global-map (kbd "C-x C-b") #'ibuffer)

;; navigation / buffer management
;;
;; crux is for some nice utilities
(require 'crux)
(with-eval-after-load "avy"
  (avy-setup-default))
(with-eval-after-load "crux"
  (key-chord-define-global "ji" #'avy-goto-char-in-line)
  (key-chord-define-global "jj" #'avy-goto-word-1)
  (key-chord-define-global "jl" #'avy-goto-line)
  (key-chord-define-global "jk" #'avy-goto-char)
  (key-chord-define-global "JJ" #'crux-switch-to-previous-buffer)
  (key-chord-define-global "xx" #'execute-extended-command)
  (key-chord-mode +1)

  (define-key global-map [remap move-beginning-of-line] #'crux-move-beginning-of-line)

  ;; Don't need crux to provide this with Emacs29 which provides
  ;; `duplicate-line' and `duplicate-dwim' functions
  (if (version< emacs-version "29")
      (define-key global-map (kbd "C-c d") #'crux-duplicate-current-line-or-region)
    (keymap-set global-map "C-c d" #'duplicate-line))

  ;; Emacs 29 now provides this functionality
  (if (version< emacs-version "29")
      (define-key global-map (kbd "C-c f") #'crux-recentf-find-file)
    (keymap-set global-map "C-c f" #'recentf-open))
  (define-key global-map (kbd "C-c k") #'crux-kill-other-buffers)
  (define-key global-map (kbd "C-c n") #'crux-cleanup-buffer-or-region))

;;; which-key
(customize-set-variable 'which-key-show-early-on-C-h t)
(customize-set-variable 'which-key-idle-delay 1.0)
(customize-set-variable 'which-key-idle-secondary-delay 0.05)
(which-key-mode)
(with-eval-after-load "which-key"
  (which-key-setup-side-window-right-bottom))

;;; writefreely

;; This is called in the yasnippet snippet to create a new blog entry.
(defun load-write-freely ()
  "Loads writefreely.  Use similar to a hook function"
  (interactive)
  (unless (featurep 'writefreely)
    (package-activate 'writefreely))
  (writefreely-mode))

;; Once writefreely is loaded, get the auth-token so blogs published
;; will show up correctly.
(with-eval-after-load "writefreely"
  (require 'auth-source)
  (customize-set-variable 'writefreely-auth-token
                          (auth-source-pick-first-password :host "write.as")))


;;; pinentry -- used by vc-mode & magit
;; gpg config to use pinentry from the minibuffer
(customize-set-variable 'epg-pinentry-mode 'loopback)

;; MS Windows does not support local sockets, so this won't work
;; there.
(unless ON-WINDOWS (pinentry-start))

;;; magit & diff-hl
(customize-set-variable 'magit-diff-refine-hunk 'all)
(define-key global-map (kbd "C-x g") #'magit-status)

;; Add diff-hl to vc-mode command prefix
(customize-set-variable 'diff-hl-command-prefix (kbd "C-x v"))
(global-diff-hl-mode)
(add-hook 'magit-process-find-password-functions
          #'magit-process-password-auth-source)
(add-hook 'magit-post-refresh-hook #'diff-hl-magit-post-refresh)


;;; Python configuration
;; put the anaconda python files in the crafted-config-var-directory
(customize-set-variable 'anaconda-mode-installation-directory
                        (expand-file-name "anaconda-mode" crafted-config-var-directory))
;; turn on flymake
(add-hook 'python-mode-hook #'flymake-mode)

;; Suggested additional keybindings for python mode
(with-eval-after-load "python"
  (define-key python-mode-map (kbd "C-c C-n") #'numpydoc-generate)
  (define-key python-mode-map (kbd "C-c e n") #'flymake-goto-next-error)
  (define-key python-mode-map (kbd "C-c e p") #'flymake-goto-prev-error))

;; Suggested keybindings for pyvenv mode
(with-eval-after-load "pyvenv"
  (define-key pyvenv-mode-map (kbd "C-c p a") #'pyvenv-activate)
  (define-key pyvenv-mode-map (kbd "C-c p d") #'pyvenv-deactivate)
  (define-key pyvenv-mode-map (kbd "C-c p w") #'pyvenv-workon))

;; hydra to help navigate YAML files
(add-hook 'yaml-mode-hook
          (lambda () (define-key yaml-mode-map (kbd "C-c >") #'indent-tools-hydra/body)))

;; profiler support, not needed normally
;; start emacs with `-l profiler --eval "(profiler-start 'cpu+mem)"'
(require 'profiler)
(add-hook 'after-init-hook #'profiler-stop)

;;; multiple-cursors
(defvar my-multiple-cursors-prefix-key "C-c m"
  "Configure the prefix key for `my-multiple-cursors' bindings.")

(define-prefix-command 'my-multiple-cursors-key-map)
(define-key 'my-multiple-cursors-key-map (kbd "m") #'mc/mark-all-like-this-dwim)
(define-key 'my-multiple-cursors-key-map (kbd "r") #'mc/mark-all-in-region)
(define-key 'my-multiple-cursors-key-map (kbd "R") #'mc/mark-all-in-region-regexp)

(global-set-key (kbd my-multiple-cursors-prefix-key) 'my-multiple-cursors-key-map)

(define-key global-map (kbd "C-c >") #'mc/mark-next-like-this)
(define-key global-map (kbd "C-c <") #'mc/mark-previous-like-this)
(define-key global-map (kbd "C-c s") #'mc/mark-next-symbol-like-this)
(define-key global-map (kbd "C-c S") #'mc/mark-previous-symbol-like-this)
(define-key global-map (kbd "C-c n") #'mc/skip-to-next-like-this)
(define-key global-map (kbd "C-c p") #'mc/skip-to-previous-like-this)
(define-key global-map (kbd "C-c #") #'mc/insert-numbers)
(define-key global-map (kbd "C-c l") #'mc/insert-letters)


;;; php-mode
(add-hook 'php-mode-hook #'eglot-ensure)

;;; java-mode
(add-hook 'java-mode-hook #'eglot-java-mode)
(add-hook 'eglot-java-mode-hook
          (lambda ()
            (define-key eglot-java-mode-map (kbd "C-c j n") #'eglot-java-file-new)
            (define-key eglot-java-mode-map (kbd "C-c j r") #'eglot-java-run-main)
            (define-key eglot-java-mode-map (kbd "C-c j t") #'eglot-java-run-test)
            (define-key eglot-java-mode-map (kbd "C-c j b") #'eglot-java-project-build-task)
            (define-key eglot-java-mode-map (kbd "C-c j B") #'eglot-java-project-build-refresh)))

(with-eval-after-load 'eglot
  (define-key eglot-mode-map (kbd "C-c e a") #'eglot-code-actions))


;;; Info
(let ((my-info-dir (expand-file-name "info/dir" crafted-config-path)))
  (when (file-exists-p my-info-dir)
    (require 'info)
    (info-initialize)
    (push (file-name-directory my-info-dir) Info-directory-list)))


;;; presentations with epresent
(when (featurep 'epresent)
  ;; turn on/off line numbers when in presentations
  (add-hook 'epresent-start-presentation-hook
            (lambda ()
              (display-line-numbers-mode -1)))

  (add-hook 'epresent-stop-presentation-hook
            (lambda ()
              (display-line-numbers-mode))))

(provide 'config)
;;; config.el ends here
