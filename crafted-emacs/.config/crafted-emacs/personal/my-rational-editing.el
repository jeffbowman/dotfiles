;;;; my-rational-editing.el --- My rational editing custom config  -*- lexical-binding: t; -*-

;; Copyright (C) 2022
;; SPDX-License-Identifier: MIT

;; Author: System Crafters Community

;;; Commentary:

;; Overrides and additions for rational-editing specific to my
;; configuration.

;;; Code:


(electric-pair-mode 1) ; auto-insert matching bracket
(show-paren-mode 1)    ; turn on paren match highlighting
(column-number-mode t) ; column numbers in the modeline

(provide 'my-rational-editing)
;;; my-rational-editing.el ends here
