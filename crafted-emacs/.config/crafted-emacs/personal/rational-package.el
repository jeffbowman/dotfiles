;;; rational-package.el --- pacakge frameworks  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  
;; SPDX-License-Identifier: MIT

;; Author:  Jeff Bowman

;;; Commentary:

;; Prefer package.el for installing packages. This provides some
;; helper funtions to make that more efficient.

;;; Code:

(require 'cl-extra)                     ; for #'cl-every

(defun check-all-installed (packages)
  "Check to make sure all PACKAGES are installed. 

PACKAGES is a list of package symols"
  (cl-every #'package-installed-p packages))

(defun rational-install-packages (packages)
  "Install the list of PACKAGES"
  ;; (package-refresh-contents)
  (mapc #'rational-package-install-package packages))

(provide 'rational-package)
;;; my-rational-completion.el ends here
