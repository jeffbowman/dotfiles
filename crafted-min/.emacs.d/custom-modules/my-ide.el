;;; my-ide.el --- IDE configuration                  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Jeff Bowman

;; Author: Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'crafted-ide-config)

(with-eval-after-load 'crafted-ide-config
  (message "configuring treesit-auto")
  ;; skip python, it was giving too many parse errors.
  (crafted-ide-configure-tree-sitter '(go java javascript typescript)))

(with-eval-after-load "prog-mode"
  (keymap-set prog-mode-map "C-c e n" #'flymake-goto-next-error)
  (keymap-set prog-mode-map "C-c e p" #'flymake-goto-prev-error)
  (keymap-set prog-mode-map "C-c e b" #'flymake-show-buffer-diagnostics))

;; eglot commands
(with-eval-after-load "eglot"
  (keymap-set eglot-mode-map "C-c e a" #'eglot-code-actions)
  (keymap-set eglot-mode-map "C-c e e" #'eglot-code-action-extract)
  (keymap-set eglot-mode-map "C-c e i" #'eglot-code-action-inline)
  (keymap-set eglot-mode-map "C-c e R" #'eglot-rename)
  (keymap-set eglot-mode-map "C-c e r" #'eglot-code-action-rewrite)
  (keymap-set eglot-mode-map "C-c e q" #'eglot-code-action-quickfix))

(provide 'my-ide)
;;; my-ide.el ends here
