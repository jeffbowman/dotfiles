;;; my-ide-python-packages.el --- Packages for Python development  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Jeff Bowman

;; Author: Jeff Bowman <jeff@appnovation.com@REM-MBA-9465>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Packages for Python development

;;; Code:

(add-to-list 'package-selected-packages 'blacken)
(add-to-list 'package-selected-packages 'eglot)
(add-to-list 'package-selected-packages 'numpydoc)
(add-to-list 'package-selected-packages 'pyvenv)

(provide 'my-ide-python-packages)
;;; my-ide-python-packages.el ends here
