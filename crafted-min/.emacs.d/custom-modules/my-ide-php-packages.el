;;; my-ide-php-packages.el --- PHP Packages          -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Jeff Bowman

;; Author: Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; PHP related packages.  Admittedly, Emacs doesn't seem to have
;; strong support for PHP programming, which seems odd.  It is at
;; least hard to find examples of people using Emacs for PHP
;; development

;;; Code:

(add-to-list 'package-selected-packages 'php-mode)          ;ide-php
(add-to-list 'package-selected-packages 'web-mode)          ;ide-php - needed for php blade templates


(provide 'my-ide-php-packages)
;;; my-ide-php-packages.el ends here
