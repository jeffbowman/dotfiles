;;; my-ide-go.el --- Go Lang configuration           -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Jeff Bowman

;; Author: Jeff Bowman <jeff@appnovation.com@REM-MBA-9465>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Configuration for programming using the Go language.  Some elements
;; directly lifted from:

;; https://github.com/golang/tools/master/gopls/doc/emacs.md

;;; Code:

(require 'crafted-ide-config nil :noerror)

;;; setup project.el
(require 'project)

(defun project-find-go-module (dir)
  (when-let ((root (locate-dominating-file dir "go.mod")))
    (cons 'go-module root)))

(cl-defmethod project-root ((project (head go-module)))
  (cdr project))

(add-hook 'project-find-functions #'project-find-go-module)

;;; setup hooks

(require 'go-mode)
(require 'eglot)
(add-hook 'go-mode-hook 'flymake-mode 8)
(add-hook 'go-mode-hook 'flymake-show-buffer-diagnostics 9)
(add-hook 'go-mode-hook 'eglot-ensure 10)
(add-hook 'go-ts-mode-hook 'flymake-mode 8)
(add-hook 'go-ts-mode-hook 'flymake-show-buffer-diagnostics 9)
(add-hook 'go-ts-mode-hook 'eglot-ensure 10)

;; Optional: install eglot-format-buffer as a save hook.
;; The depth of -10 places this before eglot's willSave notification,
;; so that that notification reports the actual contents that will be saved.
(defun eglot-format-buffer-on-save ()
  (add-hook 'before-save-hook #'eglot-format-buffer -10 t))
(add-hook 'go-mode-hook #'eglot-format-buffer-on-save)
(add-hook 'go-ts-mode-hook #'eglot-format-buffer-on-save)

;;; setup gopls

;; option 1
;; this is the global setting, but may prefer the next configuration
;; instead.

(setq-default eglot-workspace-configuration
              '((:gopls .
                        ((staticcheck . t)
                         (matcher . "CaseSensitive")))))


;; option 2
;; add the following to a .dir-locals.el file in the project root

;; ((nil (eglot-workspace-configuration . ((gopls . ((staticcheck . t)
;;                                                   (matcher . "CaseSensitive")))))))

;;; setup flymake
(add-to-list 'display-buffer-alist
             '("^\\*Flymake diagnostics"
               (display-buffer-reuse-window display-buffer-pop-up-window)
               (window-height . 10)))

(provide 'my-ide-go)
;;; my-ide-go.el ends here
