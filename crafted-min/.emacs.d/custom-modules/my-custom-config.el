;;; my-custom-config.el --- Personal Utilities and Customizations  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Jeff Bowman

;; Author: Jeff Bowman <jeffbowman@archlinux>
;; Keywords: lisp, lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Custom code written to provide various utilities.

;;; Code:

;; not sure if I still want this code or not
;; (when (version<= "29" emacs-version)
;;   (package-install 'standard-themes)
;;   (standard-themes-load-dark))

;; 2023-10-05: Doesn't quite work.  Tried setting up pantalaimon,
;; managed to download the keys and get them installed once, was able
;; to connect and see encrypted messages, but couldn't send messages.
;; After disconnecting, could no longer connect, keep getting this
;; error: (ement-api-error "7: Failed to connect to host.")
;; panctl list-servers does not list any servers. 
(defun my-ement-connect ()
  (interactive)
  (let ((password (auth-source-pick-first-password :host "element.io" :user "@wolfjb:matrix.org")))
    ;; attempt pantalaimon connection to handle encrypted rooms
    ;; (ement-connect :uri-prefix "https://localhost:8009" :user-id "@wolfjb:matrix.org" :password password)
    (ement-connect :user-id "@wolfjb:matrix.org" :password password)))

(setq mastodon-instance-url "https://emacs.ch"
      mastodon-active-user "wolfjb")
;; mastodon authorization code
;; "QQMrWDws8y-HxneT6bmBjAYvWfRO4KlidS1gWczB-Ns"

;; twitch.tv irc oauth token
;; oauth:dvw7yye3qk7mleima7tlh272fgie1c
;;
;; need to send the following once connected:
;; /quote CAP REQ :twitch.tv/membership
;; /join #systemcrafters
;;
;; Haven't quite figured out how to find the buffer to switch to which
;; hosts the `#systemcrafters' IRC channel. 
(defun my-erc-join-twitch-systemcrafters ()
  "Connect to twitch.tv IRC channel for SystemCrafters stream."
  (interactive)
  (let ((credential (auth-source-pick-first-password :host "irc.twitch.tv")))
    (erc-tls :server "irc.twitch.tv"
             :port 6697
             :password credential
             :nick "jefftbowman"
             :id "twitch.tv/systemcrafters")

    (sleep-for 3)   ; wait a bit before sending the following commands
    (erc-cmd-QUOTE "CAP REQ :twitch.tv/membership")
    (erc-cmd-JOIN "#systemcrafters")))

(defun my-erc-join-libre-chat ()
  "Connect to Libera Chat IRC server."
  (let ((credential (auth-source-pick-first-password :host "irc.libre.chat")))
    (erc :server "irc.libera.chat"
         :port 6667
         :password credential
         :nick "wolfjb"
         :id "Libera.Chat")))

(defun my-erc-join-libre-systemcrafters ()
  "Connect to libera chat IRC channel for SystemCrafters."
  (interactive)
  (my-erc-join-libre-chat)
  (sleep-for 3)   ; wait a bit before sending the following commands
  (erc-cmd-JOIN "#systemcrafters")
  ;; wait until the systemcrafters channel buffer has been
  ;; created.
  (while (= 0 (length (match-buffers "^#")))
    (sleep-for 1))
  (switch-to-buffer-other-window "#systemcrafters"))

(defun my-erc-join-libre-systemcrafters-live ()
  "Connect to libera chat IRC channel for SystemCrafters-Live."
  (interactive)
  (my-erc-join-libre-chat)
  (sleep-for 3)   ; wait a bit before sending the following commands
  (erc-cmd-JOIN "#systemcrafters-live")
  ;; wait until the systemcrafters-live channel buffer has been
  ;; created.
  (while (= 0 (length (match-buffers "^#systemcrafters-live")))
    (sleep-for 1))
  (switch-to-buffer-other-window "#systemcrafters-live"))

;; ement development version, change `commit' as needed
;; (quelpa '(ement :fetcher github :repo "alphapapa/ement.el"
;;                 :commit "cc815b8"))

;;; Theme related functions
(defun reload-modus-vivendi ()
  "Load Modus Vivendi theme and set tab bar tabs."
  (interactive)
  (reload-theme 'modus-vivendi)
  (set-face-foreground 'tab-bar-tab "black")
  (set-face-background 'tab-bar-tab "SkyBlue"))

(defun reload-ef-bio ()
  "Load EF Bio theme and set tab bar tabs."
  (interactive)
  (reload-theme 'ef-bio)
  (set-face-foreground 'tab-bar-tab "#d0ffe0")
  (set-face-background 'tab-bar-tab "#00552f"))

(provide 'my-custom-config)
;;; my-custom-config.el ends here
