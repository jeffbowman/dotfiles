;;; my-codeium-config.el --- Configuration for Codeium GenAI  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Jeff Bowman

;; Author: Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Add codeium gen-ai for ai generated completion suggestions

;;; Code:

(unless (package-installed-p 'codeium)
  (package-vc-install "https://github.com/Exafunction/codeium.el" :last-release 'Git))

(require 'codeium)

(add-to-list 'completion-at-point-functions
             #'codeium-completion-at-point)

(add-hook 'after-init-hook
          (lambda ()
            (require 'codeium)
            (unless codeium/metadata/api_key
              (codeium-install))))

(provide 'my-codeium-config)
;;; my-codeium-config.el ends here
