;;; my-markdown-config.el --- Markdown configuration  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Jeff Bowman

;; Author: Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Configure markdown mode(s)

;;; Code:

(when (and (package-installed-p 'markdown-mode)
           (package-installed-p 'grip-mode))
  (add-hook 'markdown-mode-hook #'grip-mode))

(provide 'my-markdown-config)
;;; my-markdown-config.el ends here
