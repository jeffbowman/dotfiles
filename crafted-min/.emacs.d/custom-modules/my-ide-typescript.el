;;; my-ide-typescript.el --- typescript configuration  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Jeff Bowman

;; Author: Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; typescript/tsx configuration.  Added 

;;; Code:

;; The unfortunate thing here is the hard coded version number. May
;; need to look into other ways to get that. N.B. the `treesit-auto'
;; package uses the `master' branch as the version.
(dolist (grammar '((tsx . ("https://github.com/tree-sitter/tree-sitter-typescript" "v0.23.2" "tsx/src"))
                   (typescript . ("https://github.com/tree-sitter/tree-sitter-typescript" "v0.23.2" "typescript/src"))))
  (unless (alist-get (car grammar) treesit-language-source-alist)
    (add-to-list 'treesit-language-source-alist grammar))
  (unless (treesit-language-available-p (car grammar))
    (treesit-install-language-grammar (car grammar)))
  (dolist (mapping '((typescript-mode . typescript-ts-base-mode)
                     (js-mode . typescript-ts-base-mode)
                     (js2-mode . typescript-ts-base-mode)))
    (unless (alist-get (car mapping) major-mode-remap-alist)
      (add-to-list 'major-mode-remap-alist mapping))))

(add-to-list 'auto-mode-alist (cons "\\.tsx\\'"  #'tsx-ts-mode))

(provide 'my-ide-typescript)
;;; my-ide-typescript.el ends here
