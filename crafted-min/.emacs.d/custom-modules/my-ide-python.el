;;; my-ide-python.el --- Python IDE Configuration    -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Jeff Bowman

;; Author: Jeff Bowman <jeff@appnovation.com@REM-MBA-9465>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Python configuration

;;; Code:
(require 'python)

;;; Hooks
(add-hook 'python-base-mode-hook #'blacken-mode)
(add-hook 'python-base-mode-hook #'eldoc-mode)
(add-hook 'python-base-mode-hook #'pyvenv-mode)
(add-hook 'python-base-mode-hook #'pyvenv-tracking-mode)

;; flymake hooks, handle specially since the ordering matters
(add-hook 'python-mode-hook #'eglot-ensure 10)

;;; pyvenv
;;;; restart python when the virtual environment changes
(add-hook 'pyvenv-post-activate-hooks #'pyvenv-restart-python)
;;;; Suggested keybindings for pyvenv mode
(require 'pyvenv)
(define-key pyvenv-mode-map (kbd "C-c p a") #'pyvenv-activate)
(define-key pyvenv-mode-map (kbd "C-c p d") #'pyvenv-deactivate)
(define-key pyvenv-mode-map (kbd "C-c p w") #'pyvenv-workon)

;;;; default to the commonly used "venv" folder for the virtual
;; environment
(customize-set-variable 'pyvenv-default-virtual-env-name "venv")

;;; python mode
(customize-set-variable 'python-indent-guess-indent-offset-verbose nil)
;;;; Suggested additional keybindings for python mode

(define-key python-base-mode-map (kbd "C-c C-n") #'numpydoc-generate)

;;; numpydoc
(customize-set-variable 'numpydoc-insert-examples-block nil)
(customize-set-variable 'numpydoc-template-long nil)

;;; debugging via dape - assumes dape is installed
(require 'dape)
;; Dape configs
(add-to-list 'dape-configs
	     `(debugpy-flask
	       modes (python-mode jinja2-mode)
	       command "python"
	       command-args ["-m" "debugpy.adapter" "--host" "0.0.0.0" "--port" :autoport]
	       port :autoport
	       :type "python"
	       :request "launch"
	       :module "flask"
	       :args ["--app" "app" "run" "--no-debugger" "--no-reload"]
	       :console "integratedTerminal"
	       :showReturnValue t
	       :justMyCode nil
	       :jinja t
	       :cwd dape-cwd-fn))

(provide 'my-ide-python)
;;; my-ide-python.el ends here
