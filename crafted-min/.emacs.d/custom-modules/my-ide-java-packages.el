;;; my-ide-java-pacakges.el --- Java IDE packages             -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Jeff Bowman

;; Author: Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: languages,

;;; Commentary:
;;      Packages useful for Java development

;;; Code:

(add-to-list 'package-selected-packages 'eglot-java)        ;ide-java

(provide 'my-ide-java-packages)
;;; my-ide-java.el ends here
