;;; my-completion-packages.el --- packages used in completion configuration  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Jeff Bowman

;; Author: Jeff Bowman <jeff@appnovation.com@Jeffs-MacBook-Air.local>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Packages for completion

;;; Code:

(add-to-list 'package-selected-packages 'cape)
;; no longer using consult
;; (add-to-list 'package-selected-packages 'consult) ; for consult-imenu, consult-line, consult-org-heading
(add-to-list 'package-selected-packages 'corfu)
;; not using embark
;; (add-to-list 'package-selected-packages 'embark)
;; (add-to-list 'package-selected-packages 'embark-consult)
(add-to-list 'package-selected-packages 'orderless)
(add-to-list 'package-selected-packages 'prescient)

(provide 'my-completion-packages)
;;; my-completion-packages.el ends here
