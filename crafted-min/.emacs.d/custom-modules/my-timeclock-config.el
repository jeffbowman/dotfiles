;;; my-timeclock-config.el --- Timeclock configuration  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Jeff Bowman

;; Author: Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Some contracts require more granular time tracking.  This
;; configuration allows for tracking when I start a "project" (aka a
;; task) and when I stop (via timeclock-in and timeclock-out
;; functions).  Unfortunately, the "project" name is only a single
;; word, so have to use underscore characters as space values.

;;; Code:

(global-set-key (kbd "C-c t c") #'timeclock-change)
(global-set-key (kbd "C-c t i") #'timeclock-in)
(global-set-key (kbd "C-c t o") #'timeclock-out)
(global-set-key (kbd "C-c t r") #'timeclock-generate-report)
(global-set-key (kbd "C-c t s") #'timeclock-status-string)
(global-set-key (kbd "C-c t v") #'timeclock-visit-timelog)

(provide 'my-timeclock-config)
;;; my-timeclock-config.el ends here
