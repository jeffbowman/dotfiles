;;; my-gcloud-tramp.el --- Tramp config to ssh to glcoud instances  -*- lexical-binding: t; -*-

;; Copyright (C) 2025  Jeff Bowman

;; Author: Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Add Google cloud ssh access to tramp, requires google-cloud-cli /
;; google-cloud-sdk command line tools installed.
;; from this gist, copied verbatim:
;; https://gist.github.com/jackrusher/36c80a2fd6a8fe8ddf46bc7e408ae1f9

;;; Code:

;; make sure you've set your default project with:
;; gcloud config set project <project-name>

(require 'tramp)
(add-to-list 'tramp-methods
             '("gcssh"
               (tramp-login-program        "/opt/google-cloud-cli/bin/gcloud compute ssh")
               (tramp-login-args           (("%h")))
               (tramp-async-args           (("-q")))
               (tramp-remote-shell         "/bin/sh")
               (tramp-remote-shell-args    ("-c"))
               (tramp-gw-args              (("-o" "GlobalKnownHostsFile=/dev/null")
                                            ("-o" "UserKnownHostsFile=/dev/null")
                                            ("-o" "StrictHostKeyChecking=no")))
               (tramp-default-port         22)))

;; ... after which it's as easy as:
;;
;; C-x C-f /gcssh:compute-instance:/path/to/filename.clj

(provide 'my-gcloud-tramp)
;;; my-gcloud-tramp.el ends here
