;;; early-init.el --- Emacs early initialization for Crafted Emacs (optional) -*- lexical-binding: t; -*-
;;; Commentary:
;;
;;; Code:

;; Configures `package.el'
(load "~/crafted-emacs/modules/crafted-early-init-config")

;; turn off the toolbar
(push '(tool-bar-lines .0) default-frame-alist)

;; Turn off minor modes in the mode line
(dolist (elt mode-line-modes)
  (when (eq (type-of elt) 'cons)
    (when (listp (cadr elt))
      (when (eq 'minor-mode-alist (cadr (cadr elt)))
        (setf (cadr (cadr elt)) nil)))))

(cond
 ((string-equal (symbol-name system-type) "windows-nt")
  ;; Windows does not have Anonymous Pro, use Courier New instead
  (custom-set-faces `(default ((t (:family "Courier New" :height 140))))
                    `(fixed-pitch ((t (:inherit (default)))))
                    `(fixed-pitch-serif ((t (:inherit (default)))))))
 ((string-equal (symbol-name system-type) "darwin")
  ;; MacOS does not have Anonymous Pro, use Monaco instead
  (custom-set-faces `(default ((t (:family "Monaco" :height 180))))
                    `(fixed-pitch ((t (:inherit (default)))))
                    `(fixed-pitch-serif ((t (:inherit (default)))))))
 (t
  ;; Linux systems, allow using Emacs preference (probably DejaVu Sans
  ;; Mono) but set the size to something readable
  (custom-set-faces `(default ((t (:height 180))))
                    `(fixed-pitch ((t (:inherit (default)))))
                    `(fixed-pitch-serif ((t (:inherit (default)))))
                    '(tab-bar-tab ((t (:box (:line-width (2 . -2) :color "white")
                                            ;; foreground and
                                            ;; background have to be
                                            ;; set here, reloading the
                                            ;; theme and setting them
                                            ;; doesn't seem to work
                                            ;; during initalization
                                            :foreground "#d0ffe0"
                                            :background "#00552f"
                                            :inherit mode-line-active)))))))

(defun reload-theme (theme)
  "Reloads THEME."
  (mapc #'disable-theme custom-enabled-themes)
  (when theme
    (load-theme theme :no-confirm)))

;; (if (version<= "28.0" emacs-version)
;;     (progn
;;       ;; attempt to avoid light to dark flash when Emacs is starting
;;       (load-theme 'deeper-blue)

;;       ;; variables have to be set before loading the theme or they
;;       ;; don't take affect
;;       (customize-set-variable 'modus-themes-completions '((matches   . (background intense))
;;                                                           (selection . (accented intense))
;;                                                           (popup     . (accented intense))))
;;       (customize-set-variable 'modus-themes-hl-line '(accented intense))
;;       (customize-set-variable 'modus-themes-intense-hl-line t)
;;       (customize-set-variable 'modus-themes-links '(faint background))
;;       (customize-set-variable 'modus-themes-syntax '(faint yellow-comments green-strings))
;;       ;; load theme after Emacs finishes loading. Not doing so seems
;;       ;; to load the wrong version of modus-themes since the package
;;       ;; system isn't fully initialized yet causing an error on
;;       ;; startup.  This is due to upgrading modus-themes to a version
;;       ;; not included in Emacs, which also has breaking API changes
;;       (add-hook 'after-init-hook (lambda () (reload-theme 'modus-vivendi))))
;;   (load-theme 'deeper-blue))

(setq frame-inhibit-implied-resize t)

;; packages
(add-to-list 'package-pinned-packages (cons 'embark-consult "stable"))
(add-to-list 'package-pinned-packages (cons 'pdf-tools "melpa"))

(customize-set-variable 'package-load-list
                        '((doom-modeline nil)
                          ;; (tree-sitter-langs "0.12.8")
                          all))

(provide 'early-init)

;;; early-init.el ends here
