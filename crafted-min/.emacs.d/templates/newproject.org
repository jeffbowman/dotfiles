#+STARTUP: lognotereschedule logdrawer
#+SEQ_TODO: TODO(t/!) IN_PROGRESS(i/!) HOLD(h@/!) READY_FOR_QA(r@/!) | DONE(d@) CANCELED(c@)
#+COLUMNS: %TODO %50ITEM %CLOCKSUM_T(today) %CLOCKSUM(total)

* TODO Initial Project Sync 
** People
   - Client POCs and roles
** Questions
   - Base level questions to get answered. Others added at this level.
*** Problem to Solve
    - What does this client need?
*** Infrastructure
    - Version control - assuming git, but hosted where?
    - Continuous build (Jenkins, Bamboo, Travis, CircleCI, etc)?
* Notes
  - Space for additional information before the contract starts.
* Access
  - VPN
  - Version control
  - Ticketing (user stories or whatever)
  - Documentation
* Links
  This section holds useful links for the project.  Examples might be:
  - Ticketing system
  - Documentation system
  - Deployment environments
    - Dev
    - QA
    - Prod
* Work
** Journal
*** 2025
**** January
***** [2025-01-01 Wed] Description of work
** Time
*** 2025
**** January
***** 1/1 - 1/3
   =my/week-time= template expanded here
#+BEGIN: clocktable :scope file :block 2025-w1 :step day :maxlevel 6 :compact t :narrow 80! :emphasize t :stepskip0 t
#+END:
** Meetings
*** Recurring
**** Status Updates 10:00-10:15
     N.B. change the ~[...]~ to ~<...>~ to make this an active
     schedule, also update the time on the title
     [%%(memq (calendar-day-of-week date) '(1 2 3 4 5))]
*** One-Time
** Deliverables
   Items not necessarily tracked in a ticketing system.
*** Documenation
