;;; custom.el --- EasyCustomization options -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(Man-notify-method 'aggressive)
 '(after-save-hook '(executable-make-buffer-file-executable-if-script-p))
 '(anaconda-mode-installation-directory "/home/jeffbowman/.emacs.d/anaconda-mode" t)
 '(auto-insert-directory (expand-file-name "templates" user-emacs-directory))
 '(auto-insert-mode t)
 '(auto-insert-query nil)
 '(bbdb-mua-action 'query)
 '(bbdb-pop-up-window-size 5)
 '(bidi-paragraph-direction 'left-to-right)
 '(blacken-skip-string-normalization t)
 '(bookmark-save-flag 1)
 '(column-number-mode t)
 '(completion-auto-select 'second-tab)
 '(completion-category-overrides '((file (styles partial-completion))))
 '(completion-cycle-threshold 3)
 '(completion-styles '(basic initials partial-completion emacs22))
 '(completions-detailed t)
 '(completions-format 'vertical)
 '(completions-group t)
 '(completions-group-sort 'alphabetical)
 '(completions-max-height 10)
 '(custom-enabled-themes '(ef-bio))
 '(custom-safe-themes
   '("ae20535e46a88faea5d65775ca5510c7385cbf334dfa7dde93c0cd22ed663ba0"
     "e85a354f77ae6c2e47667370a8beddf02e8772a02e1f7edb7089e793f4762a45"
     default))
 '(delete-selection-mode t)
 '(diary-list-entries-hook '(diary-include-other-diary-files))
 '(diary-mark-entries-hook '(diary-mark-included-diary-files))
 '(dired-auto-revert-buffer t)
 '(dired-dwim-target t)
 '(dired-listing-switches "-alF --group-directories-first")
 '(display-time-24hr-format t)
 '(display-time-mode t)
 '(dynamic-completion-mode t)
 '(ediff-window-setup-function 'ediff-setup-windows-plain)
 '(eglot-autoshutdown t t)
 '(electric-pair-mode t)
 '(epg-pinentry-mode 'loopback)
 '(eudc-server-hotlist '(("" . bbdb)))
 '(fancy-splash-image "/home/jeffbowman/crafted-emacs/system-crafters-logo.png")
 '(fast-but-imprecise-scrolling t)
 '(find-file-hook
   '(auto-insert auto-revert--global-adopt-current-buffer
                 completion-find-file-hook
                 auto-revert-find-file-function
                 epa-file-find-file-hook vc-refresh-state))
 '(font-use-system-font t)
 '(geiser-chez-binary "chez" t)
 '(global-auto-revert-mode t)
 '(global-auto-revert-non-file-buffers t)
 '(global-diff-hl-mode t)
 '(global-display-line-numbers-mode t)
 '(global-hl-line-mode t)
 '(global-so-long-mode t)
 '(ibuffer-movement-cycle nil)
 '(ibuffer-old-time 24)
 '(indent-tabs-mode nil)
 '(initial-scratch-message
   ";; This buffer is for text that is not saved, and for Lisp evaluation.\12;; To create a file, visit it with \\[find-file] and enter text in its buffer.\12;;\12;; Using the `crafted-min' configuration\12")
 '(isearch-allow-motion t)
 '(isearch-lazy-count t)
 '(isearch-yank-on-move 'shift)
 '(kill-do-not-save-duplicates t)
 '(kubed-menu-bar-mode t)
 '(kubed-yaml-setup-hook '(yaml-mode view-mode))
 '(load-prefer-newer t t)
 '(magit-diff-refine-hunk 'all)
 '(magit-post-refresh-hook '(diff-hl-magit-post-refresh))
 '(magit-process-find-password-functions '(magit-process-password-auth-source))
 '(numpydoc-insert-examples-block nil t)
 '(numpydoc-template-long nil t)
 '(org-adapt-indentation t)
 '(org-agenda-files
   '("~/org/work/appnovation/projects/uki/uki.org"
     "/home/jeffbowman/org/work/appnovation/Appnovation.org"))
 '(org-agenda-include-diary t t)
 '(org-capture-templates
   '(("n" "Note" entry (file+headline "~/.notes" "Notes") "** %U %?\12%i")
     ("t" "Todo" entry (file+headline "~/.notes" "Tasks")
      "** TODO %?\12  %i\12  %a")
     ("i" "Issue" entry (file+headline "~/.notes" "Issues")
      "* IN_PROGRESS %?\12:PROPERTIES:\12:ONSET:\12:DESCRIPTION:\12:ACTION:\12:COMPLETED:\12:END:"
      :empty-lines 0)) t)
 '(org-directory "~/org")
 '(org-duration-format '(("h") (special . h:mm)) t)
 '(org-export-backends '(ascii html icalendar latex md odt))
 '(org-outline-path-complete-in-steps nil)
 '(org-refile-allow-creating-parent-nodes 'confirm)
 '(org-refile-targets '((nil :maxlevel . 9) (org-agenda-files :maxlevel . 9)))
 '(org-refile-use-outline-path 'file)
 '(org-todo-keyword-faces
   '(("HOLD" :foreground "cyan" :weight "bold")
     ("READY_FOR_QA" :foreground "dark green" :underline t)
     ("IN_PROGRESS" :foreground "DarkOliveGreen1" :weight "bold")))
 '(package-archive-priorities
   '(("gnu" . 99) ("nongnu" . 80) ("stable" . 70) ("melpa" . 0)))
 '(package-load-list '((doom-modeline nil) all))
 '(package-selected-packages
   '(adjust-parens aggressive-indent ansible avy bbdb blacken bufferlo
                   clj-refactor dape diff-hl diminish ef-themes eldoc
                   flycheck-clojure geiser-chez geiser-gauche
                   geiser-guile geiser-racket gnuplot gnuplot-mode
                   grip-mode impostman k8s-mode key-chord kubed
                   logview magit markdown-mode modus-themes numpydoc
                   ob-restclient package-lint-flymake pinentry
                   plantuml-mode pyvenv rego-mode sly-asdf
                   sly-quicklisp sly-repl-ansi-color sxhkdrc-mode
                   vagrant vagrant-tramp verb writefreely))
 '(prog-mode-hook
   '(flyspell-prog-mode abbrev-mode flymake-mode
                        display-line-numbers-mode which-function-mode))
 '(python-indent-guess-indent-offset-verbose nil)
 '(python-mode-hook
   '(pyvenv-tracking-mode pyvenv-mode eldoc-mode blacken-mode jedi:setup
                          flymake-mode eglot-ensure) t)
 '(pyvenv-default-virtual-env-name "venv" t)
 '(recentf-mode t)
 '(repeat-mode t)
 '(safe-local-variable-values
   '((eval progn (require 'dape)
           (add-to-list 'dape-configs
                        `(debugpy-flask modes
                                        (python-mode jinja2-mode)
                                        command "python" command-args
                                        ["-m" "debugpy" "--connect"
                                         "0.0.0.0:5678"]
                                        port 5678 :type "python"
                                        :request "attach" :module
                                        "flask" :args
                                        ["--app" "app" "run"
                                         "--no-debugger" "--no-reload"]
                                        :console "integratedTerminal"
                                        :showReturnValue t :justMyCode
                                        nil :jinja t :cwd dape-cwd-fn)))
     (eval progn (require 'dape)
           (add-to-list 'dape-configs
                        `(debugpy-flask modes
                                        (python-mode jinja2-mode)
                                        command "python" command-args
                                        ["-m" "debugpy.adapter"
                                         "--host" "0.0.0.0" "--port"
                                         :autoport]
                                        port :autoport :type "python"
                                        :request "launch" :module
                                        "flask" :args
                                        ["--app" "app" "run"
                                         "--no-debugger" "--no-reload"]
                                        :console "integratedTerminal"
                                        :showReturnValue t :justMyCode
                                        nil :jinja t :cwd dape-cwd-fn)))))
 '(savehist-mode t)
 '(scheme-program-name "guile" t)
 '(scroll-conservatively 101)
 '(show-paren-mode t)
 '(speedbar-update-flag t)
 '(speedbar-use-images nil)
 '(switch-to-buffer-in-dedicated-window 'pop)
 '(switch-to-buffer-obey-display-actions t)
 '(tab-always-indent 'complete)
 '(tab-bar-mode t)
 '(tab-bar-new-tab-choice "*scratch*")
 '(tab-bar-new-tab-to 'rightmost)
 '(tab-bar-select-tab-modifiers '(control meta))
 '(tab-bar-tab-hints t)
 '(tempo-interactive t)
 '(text-mode-hook
   '(turn-on-flyspell turn-on-auto-fill text-mode-hook-identify))
 '(tool-bar-mode nil)
 '(treesit-auto-langs '(go java javascript typescript))
 '(user-mail-address "jeff.t.bowman@gmail.com")
 '(which-key-idle-delay 1.0)
 '(which-key-idle-secondary-delay 0.05)
 '(which-key-mode t)
 '(which-key-show-early-on-C-h t)
 '(whitespace-action '(cleanup auto-cleanup) t)
 '(whitespace-style '(face empty trailing tab-mark indentation::space) t)
 '(winner-mode t)
 '(xref-show-definitions-function 'xref-show-definitions-completing-read))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:height 180))))
 '(fixed-pitch ((t (:inherit (default)))))
 '(fixed-pitch-serif ((t (:inherit (default)))))
 '(tab-bar-tab ((t (:box (:line-width (2 . -2) :color "white") :foreground "#d0ffe0" :background "#00552f" :inherit mode-line-active))))
 '(tab-bar-tab-inactive ((t (:foreground "SteelBlue" :background "grey75" :inherit tab-bar-tab)))))


(provide 'custom)

;; Local Variables:
;; eval: (eldoc-mode)
;; eval: (outline-minor-mode 1)
;; End:

;;; init.el ends here
