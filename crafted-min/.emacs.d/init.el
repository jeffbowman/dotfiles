;;; init.el --- Emacs initialization                 -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Jeff Bowman

;; Author: Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; A Crafted Emacs configuration, but with a focus on minimalism, in
;; this case reducing the needed number of extra packages to have a
;; well configured Emacs.

;;; Code:

;; Load the custom file if it exists.  Among other settings, this will
;; have the list `package-selected-packages', so we need to load that
;; before adding more packages.  The value of the `custom-file'
;; variable must be set appropriately, by default the value is nil.
;; This can be done here, or in the early-init.el file.
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (and custom-file
           (file-exists-p custom-file))
  (load custom-file nil 'nomessage))

;; Adds crafted-emacs modules to the `load-path', sets up a module
;; writing template, sets the `crafted-emacs-home' variable.
(load "~/crafted-emacs/modules/crafted-init-config")

;;;; Packages


;; Crafted Emacs Packages
(require 'crafted-lisp-packages)

;; Packages are listed in `custom.el' as `package-selected-packages',
;; the packages listed here are installed conditionally.

(when (version< emacs-version "30")
  (add-to-list 'package-selected-packages 'which-key))

;; Not sure if I still need/want this, it is for navigation by
;; indentation, which may be useful with python.  From MELBA
;; (add-to-list 'package-selected-packages 'indent-tools)

(when (eq system-type 'gnu/linux)
  ;; Only needed for configuring window manager
  (add-to-list 'package-selected-packages 'sxhkdrc-mode))

(when (executable-find "vagrant")
  ;; only needed when vagrant is installed
  (add-to-list 'package-selected-packages 'vagrant)
  (add-to-list 'package-selected-packages 'vagrant-tramp))

;;;;; DevOps
(add-to-list 'package-selected-packages 'ansible)

;;;;; Emacs
(add-to-list 'package-selected-packages 'bufferlo)
(add-to-list 'package-selected-packages 'compat)
(add-to-list 'package-selected-packages 'diminish)

;;;;; Editing
(add-to-list 'package-selected-packages 'grip-mode)
(add-to-list 'package-selected-packages 'markdown-mode)
(add-to-list 'package-selected-packages 'yaml-mode)

;;;;; Mail/Email
(add-to-list 'package-selected-packages 'bbdb)

;;;;; Misc
(add-to-list 'package-selected-packages 'avy)
(add-to-list 'package-selected-packages 'gnuplot)
(add-to-list 'package-selected-packages 'gnuplot-mode)
(add-to-list 'package-selected-packages 'pinentry)

;;;;; Org
(add-to-list 'package-selected-packages 'verb)
(add-to-list 'package-selected-packages 'ob-restclient)

;;;;; Programming
(add-to-list 'package-selected-packages 'adjust-parens)
(add-to-list 'package-selected-packages 'dape)
(add-to-list 'package-selected-packages 'eglot)
(add-to-list 'package-selected-packages 'eldoc)
(add-to-list 'package-selected-packages 'multiple-cursors)
(add-to-list 'package-selected-packages 'pyvenv)
(add-to-list 'package-selected-packages 'restclient)

;;;;; Themes
(add-to-list 'package-selected-packages 'ef-themes)

;;;;; Web
(add-to-list 'package-selected-packages 'impostman)

;;;;; Install Packages
(package-install-selected-packages :noconfirm)

;;;; Customization


;;;;; Crafted Emacs Modules
(when (locate-library "aggressive-indent")
  (require 'aggressive-indent nil :noerror)) ; needed before crafted-lisp for some reason
(require 'crafted-lisp-config)
(require 'crafted-startup-config)
(require 'crafted-writing-config)

(unless crafted-startup-inhibit-splash
  (setq initial-buffer-choice #'crafted-startup-screen))

;;;;; Local Custom Modules
(require 'my-custom-config)
(require 'my-gcloud-tramp)              ; tramp ssh to google cloud vms
(require 'my-ide)
(require 'my-ide-python)
(require 'my-lisp-config)
(require 'my-org-config)
(require 'my-templates-config)
(require 'my-timeclock-config)
(require 'my-ide-typescript)

;;;;; General
;; When on a MacOS system, use `gls' instead of `ls' for `dired'
(when (and (eq system-type 'darwin)
           (executable-find "gls"))
  (setq insert-directory-program "gls"))

;; More advanced customizations.
(put 'narrow-to-region 'disabled nil)
(put 'upcase-region    'disabled nil)
(put 'downcase-region  'disabled nil)
(put 'scroll-left      'disabled nil)

;; autoinsert, for templates and similar
(with-eval-after-load "autoinsert"
  ;; if it is an interview, load a blank interview file.
  (define-auto-insert
    ".*/interviews/[a-z_-]+[0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}.org"
    (expand-file-name "templates/blank-interview.org" user-emacs-directory) nil)
  (define-auto-insert
    ".*/work/appnovation/\\(projects\\|mulesoft\\)/.*/.*\\.org"
    (expand-file-name "templates/newproject.org" user-emacs-directory)))

;;;; key bindings
(global-set-key [remap list-buffers]   #'ibuffer-list-buffers)
(global-set-key [remap zap-to-char]    #'zap-up-to-char)
(global-set-key [remap upcase-word]    #'upcase-dwim)
(global-set-key [remap downcase-word]  #'downcase-dwim)
(global-set-key [remap dabbrev-expand] #'hippie-expand)

(global-set-key (kbd "C-+")   #'text-scale-increase)
(global-set-key (kbd "C--")   #'text-scale-decrease)
(global-set-key (kbd "M-#")   #'dictionary-lookup-definition)
(global-set-key (kbd "C-c d") #'duplicate-dwim)
(global-set-key (kbd "C-h K") #'describe-keymap)
(global-set-key (kbd "C-c f") #'recentf-open)
(global-set-key (kbd "C-x g") #'magit-status)

(when (package-installed-p 'kubed)
  (global-set-key (kbd "C-c k") #'kubed-prefix-map))

;;;;; eglot
(with-eval-after-load 'eglot
  (define-key eglot-mode-map (kbd "C-c e a") #'eglot-code-actions))

;;;;; multiple cursors
(defvar my-multiple-cursors-prefix-key "C-c m"
  "Configure the prefix key for `my-multiple-cursors' bindings.")

(define-prefix-command 'my-multiple-cursors-key-map)
(define-key 'my-multiple-cursors-key-map (kbd "m") #'mc/mark-all-like-this-dwim)
(define-key 'my-multiple-cursors-key-map (kbd "r") #'mc/mark-all-in-region)
(define-key 'my-multiple-cursors-key-map (kbd "R") #'mc/mark-all-in-region-regexp)

(global-set-key (kbd my-multiple-cursors-prefix-key) 'my-multiple-cursors-key-map)

(define-key global-map (kbd "C-c >") #'mc/mark-next-like-this)
(define-key global-map (kbd "C-c <") #'mc/mark-previous-like-this)
(define-key global-map (kbd "C-c s") #'mc/mark-next-symbol-like-this)
(define-key global-map (kbd "C-c S") #'mc/mark-previous-symbol-like-this)
(define-key global-map (kbd "C-c n") #'mc/skip-to-next-like-this)
(define-key global-map (kbd "C-c p") #'mc/skip-to-previous-like-this)
(define-key global-map (kbd "C-c #") #'mc/insert-numbers)
(define-key global-map (kbd "C-c l") #'mc/insert-letters)

;;;;; window navigation and control
(define-prefix-command 'minimal-windows-key-map)

(keymap-set 'minimal-windows-key-map "u" 'winner-undo)
(keymap-set 'minimal-windows-key-map "r" 'winner-redo)
(keymap-set 'minimal-windows-key-map "n" 'windmove-down)
(keymap-set 'minimal-windows-key-map "p" 'windmove-up)
(keymap-set 'minimal-windows-key-map "b" 'windmove-left)
(keymap-set 'minimal-windows-key-map "f" 'windmove-right)

(keymap-global-set "C-c w" 'minimal-windows-key-map)

;;;;; avy and key chords
(require 'avy nil :noerror)
(with-eval-after-load "avy"
  (avy-setup-default)
  (key-chord-define-global "ji" #'avy-goto-char-timer)
  (key-chord-define-global "jl" #'avy-goto-line)
  (key-chord-define-global "xx" #'execute-extended-command)
  (key-chord-mode +1))

;;;; lists

(add-to-list 'display-buffer-alist
             '("^\\*Dictionary\\*"
               (display-buffer-in-side-window)
               (side . left)
               (window-width . 70)))
(add-to-list 'display-buffer-alist
             '("\\*Help\\*"
               (display-buffer-reuse-window display-buffer-pop-up-window)))

;;;; Hooks

(add-hook 'ibuffer-hook
          (lambda ()
            (setq ibuffer-filter-groups (ibuffer-project-generate-filter-groups))
            (unless (eq ibuffer-sorting-mode 'project-file-relative)
              (ibuffer-do-sort-by-project-file-relative))))

(with-eval-after-load "yaml-mode"
  (add-hook 'yaml-mode-hook
            (lambda ()
              (define-key yaml-mode-map (kbd "C-c >") #'indent-tools-hydra/body))))

;; mention how long it took for Emacs to start
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Crafted Emacs loaded in %s."
                     (emacs-init-time))))

;;;; Personal


;; whitespace
(crafted-writing-configure-whitespace nil nil '(prog-mode text-mode))

;; diminish editorconfig lighter
(with-eval-after-load "editorconfig"
  (diminish 'editorconfig-mode "EC"))

;; bbdb
(when (require 'bbdb nil :noerror)
  (bbdb-initialize 'gnus 'message)
  (bbdb-mua-auto-update-init 'gnus 'message)
  ;; (setq bbdb-mua-action 'query)
  (add-hook 'gnus-startup-hook #'bbdb-insinuate-gnus))

;; gnus
(with-eval-after-load 'gnus
  (when (require 'message nil :noerror)
    (keymap-set message-mode-map "C-c <tab>" #'eudc-expand-inline))
  (with-eval-after-load 'sendmail
    (keymap-set message-mode-map "C-c <tab>" #'eudc-expand-inline))
  (add-hook 'dired-mode-hook #'turn-on-gnus-dired-mode))

;; eudc
(with-eval-after-load 'eudc
  (require 'eudcb-bbdb nil :noerror))

;; buffer handling
(require 'ibuffer)
(bufferlo-mode)                         ; list buffers local to tabs
(with-eval-after-load 'bufferlo
  (global-set-key (kbd "C-x C-b") #'bufferlo-ibuffer))

;; which-key
(with-eval-after-load "which-key"
  (which-key-setup-side-window-right-bottom))

;; writefreely

;; This is called in the yasnippet snippet to create a new blog entry.
(defun load-write-freely ()
  "Loads writefreely.  Use similar to a hook function"
  (interactive)
  (unless (featurep 'writefreely)
    (package-activate 'writefreely))
  (writefreely-mode))

;; Once writefreely is loaded, get the auth-token so blogs published
;; will show up correctly.
(with-eval-after-load "writefreely"
  (require 'auth-source)
  (customize-set-variable 'writefreely-auth-token
                          (auth-source-pick-first-password :host "write.as")))

(unless (memq system-type '(cygwin ms-dos windows-nt))
  (pinentry-start))

;; personal info files - sicp
(let ((my-info-dir (expand-file-name "info/dir" user-emacs-directory)))
  (when (file-exists-p my-info-dir)
    (require 'info)
    (info-initialize)
    (push (file-name-directory my-info-dir) Info-directory-list)))

(provide 'init)

;; Local Variables:
;; eval: (eldoc-mode)
;; eval: (outline-minor-mode 1)
;; End:

;;; init.el ends here
