(defun %browse-url (url)
  (run-shell-command (format nil "firefox ~a" url)))

(defun menu-get-item (menu item)
  (if (eql (car menu) :title)
      (menu-get-item (cddr menu) item)
      (cdr (assoc item menu :test #'string=))))
;; i.e. (show-menu (menu-get-item *mymenu* "Web") :title "Web")

(defun show-menu (menu &key (title "") parent-id)
  (let ((my-id (gensym)))
    (labels ((run ()
               (if (listp menu)
                   (case (car menu)
                     ((:eval) (eval (cadr menu)))
                     ((:value) (cadr menu))
                     ((:shell) (destructuring-bind (command &optional collect-output-p)
                                   (cdr menu)
                                 (run-shell-command command collect-output-p)))
                     ((:cmd) (run-commands (cadr menu)))
                     ((:url) (%browse-url (cadr menu)))
                     ((:title) (show-menu (cddr menu)
                                          :title (cadr menu)
                                          :parent-id parent-id))
                     (otherwise (let ((ret (select-from-menu (current-screen)
                                                             (if parent-id
                                                                 (append menu
                                                                         (list (list "Back" :value parent-id)))
                                                                 menu)
                                                             title)))
                                  (if (consp ret)
                                      (show-menu (cdr ret)
                                                 :title (car ret)
                                                 :parent-id my-id)
                                      ret))))
                   menu)))
      (loop
         for ret = (run)
         while (eq ret my-id)
         finally (return ret)))))
	 
(defparameter *mymenu*
  '(:title "MAIN MENU"
    ("Web" 
     ("cnn" :url "https://www.cnn.com/")
     ("gmail" :url "https://mail.google.com/mail/u/0/#inbox")
     ("facebook" :url "https://facebook.com")
     ("Videos"
      ("youtube" :url "https://www.youtube.com/")
      ("Amazon" :url "https://www.amazon.com/gp/video/library/movie/ref=atv_yvl_mv?ie=UTF8&sort=DATE_ADDED_DESC")
      ("Netflix" :url "https://netflix.com")
      ("Hulu" :url "https://www.hulu.com/")))
    ("Apps" :title "Apps"               ; just to remember the syntax
     ("Graphics"
      ("Houdini" :shell "houdini") 
      ("blender" :shell "blender")
      ("gimp" :shell "gimp")) 
     ("Firefox" :shell "firefox")
     ("Skype" :shell "skype")
     ("Foxit Reader" :shell "FoxitReader")
     ("Open Office" :shell "openoffice")
     ("wireshark" :shell "gksudo wireshark")
     #++("test" "asd" ("ffff" :eval 7)))
    ("Settings"
     ("Volume" :shell "pavucontrol"))))

