(in-package :stumpwm)

;;;;;;;;;;;;;;;
;; functions ;;
;;;;;;;;;;;;;;;

(defun starts-with? (start str &optional ignore-case)
  (let* ((start-pos 0)
	 (end-pos (length start))
	 (max-len (length str)))
    (if (< max-len end-pos)
	nil
	(let ((sub-str (subseq str start-pos end-pos)))
	  (if ignore-case
	      (string-equal start sub-str)
	      (string= start sub-str))))))



;;;;;;;;;;;;;;;;;;;;;;
;; initial settings ;;
;;;;;;;;;;;;;;;;;;;;;;
;; paths
(set-module-dir "/home/jeffbowman/tools/stumpwm-contrib/")

;; server to use for connecting with Emacs
(push #p"/home/jeffbowman/.config/emacs/elpa/sly-1.0.43/slynk/" asdf:*central-registry*)
(asdf:load-system :slynk)
(slynk:create-server :port 4008 :dont-close t)

;; modules
(load-module "amixer")
(load-module "battery-portable")
(load-module "net")
(load-module "mem")
;; only works on startup, and then confuses stumpwm to believe there
;; is only one monitor. turning off for the moment
;; (load-module "stumptray")
(load-module "wifi")
(load-module "kbd-layouts")

;; clx-truetype is a local-project in quicklisp, needs to be loaded
;; here to use the module
(ql:quickload :clx-truetype)
(load-module "ttf-fonts")

;; fonts
(set-font (make-instance 'xft:font :family "Courier New" :subfamily "Regular" :size 14))

;; mouse policy
(setf *mouse-focus-policy*    :sloppy
      *float-window-modifier* :meta)

;; must come after loading the battery module not currently used, but
;; throws an error if moved up in the file
(defun jb/battery-status-modeline ()
  (let ((battery-modeline "")
	(battery-status ""))
    (when (probe-file "/sys/class/power_supply/BAT0")
      (setf battery-modeline
	    (let ((battery-status
		   (string-trim
		    '(#\Space #\Tab #\Newline)
		    (run-shell-command
		     "acpi -b | awk -F '[ ,]' /'Battery 1'/'{print $3$5}'" t))))

	      (if (starts-with? "Discharging" battery-status)
		  (concatenate 'string "-" (subseq battery-status
						   (length "Discharging")))
		  "%B")))

      (setf battery-portable:*refresh-time* 30))
    battery-modeline))

;; modeline
(setf *screen-mode-line-format*
      '("^2*[%n]"
	" ^7*%W"
	" ^6*%l"
	" ^2*%M"
        " ^3%B"
;;        " ^7*" (:eval (jb/battery-status-modeline))
	"^>^2*" (:eval (time-format "%Y-%m-%d %H:%M"))))

;; turn on the mode-line
(when *initializing*
  (mode-line))

;; toggle the mode-line on the current screen
;; (toggle-mode-line (current-screen) (current-head))

;; swap caps lock and control keys
(kbd-layouts:keyboard-layout-list "us")
(setf kbd-layouts:*caps-lock-behavior* :swapped)
(kbd-layouts:switch-keyboard-layout)

;; start system tray -- this after toggling the mode-line
;; blocks usage of multiple monitors
;; (run-commands "stumptray")

;; parameters, here so only have to change in one place
(defparameter WEB-BROWSER "exec firefox"
  "set default web browser")
(defparameter X-TERM "exec kitty"
  "Use kitty instead of xterm")

;;;;;;;;;;;;;;;;;;;;;;
;; startup programs ;;
;;;;;;;;;;;;;;;;;;;;;;

;; autorandr to detect monitors automatically
(run-shell-command "autorandr -c")

;; start background services
(run-shell-command "systemctl --user start pulseaudio.service")
(run-shell-command "systemctl --user start dbus.service")
(run-shell-command "systemctl --user start ssh-agent.service")
(run-shell-command "systemctl --user start gpg-agent.service")

(run-shell-command "xrdb -load ~/.Xresources -quiet")
(run-shell-command "/usr/bin/megasync")
(run-shell-command "/usr/bin/nm-applet &> /dev/null &")
;; (run-shell-command "/usr/bin/xfce4-panel &> /dev/null &")
;; (run-shell-command "/usr/bin/tint2 &> /dev/null &")
;; (run-shell-command "/usr/bin/stalonetray &> /dev/null &")
;; (run-shell-command "/usr/bin/xfce4-volumed &> /dev/null &")
(run-shell-command "/usr/bin/xfce4-power-manager &> /dev/null &")

;; mount google drive so KeePassXC can find the database 
(run-shell-command "dex /home/jeffbowman/.config/autostart/rclone.desktop")

;; set the background
(run-shell-command "~/.fehbg")

;;;;;;;;;;;;;;
;; commands ;;
;;;;;;;;;;;;;;

(defcommand vol-up () ()
  "[DEPRECATED by module amixer] Increase the volume from the shell"
  (run-shell-command "~/bin/volume_up.sh &> /dev/null"))

(defcommand vol-down () ()
  "[DEPRECATED by module amixer] Decrease the volume from the shell"
  (run-shell-command "pactl set-sink-volume 0 -5% &> /dev/null"))

(defcommand vol-mute () ()
  "[DEPRECATED by module amixer] Mute the volume from the shell"
  (run-shell-command "~/bin/toggle_mute.sh"))

;;;;;;;;;;;;;;;;;;
;; key mappings ;;
;;;;;;;;;;;;;;;;;;
(set-prefix-key (kbd "C-z"))

(define-key *top-map* (kbd "XF86AudioLowerVolume") "amixer-Master-1-")
(define-key *top-map* (kbd "XF86AudioRaiseVolume") "amixer-Master-1+")
(define-key *top-map* (kbd "XF86AudioMute") "amixer-Master-toggle")

(define-key *root-map* (kbd "c") X-TERM)
(define-key *root-map* (kbd "C-c") X-TERM)

(define-key *root-map* (kbd "C-f") WEB-BROWSER)
(define-key *root-map* (kbd "*") "exec slock")
(define-key *root-map* (kbd "d") "exec /usr/bin/dmenu_run")

