# This has moved to GitLab and will not be updated here, please see https://gitlab.com/jeffbowman/dotfiles #

# Dot Files README #

Uses [GNU Stow](https://www.gnu.org/software/stow/) to manage where
things go in the home folder.

## Packages ##

- [Awesome Window Manager](https://awesomewm.org)

	Install with `stow awesome`, then in the `~/.config/awesome`
    folder, install the widgets by executing the shell script there: 

	`sh ./install-widgets`
	
	which will checkout a widget folder from github.

- [GNU Emacs](https://www.gnu.org/software/emacs)

	Install with `stow emacs` from the `dotfiles` folder. This will
	create the symlinks in the `$HOME/.config/emacs` folder, which may
	need to have a symlink made to `$HOME/.emacs.d` before Emacs 28
	comes out, which is a manual step in anticipation of not needing it
	in the future.
  
	See [my Emacs README](emacs/.config/emacs/README.md) for any specific
	notes on using my configuration.


