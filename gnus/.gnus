;; (setq gnus-select-method
;;       '(nnimap "imap.gmail.com"
;;                (nnimap-server-port "imaps")
;;                (nnimap-stream ssl)
;;                (nnimap-authinfo-file (expand-file-name ".authinfo" (getenv "HOME")))
;;                (nnimap-authenticator 'login)
;;                (nnimap-use-namespaces t)))

;; (setq gnus-select-method
;;       '(nnimap "imap.gmail.com"
;;                (nnimap-server-port 993)))

(setq gnus-select-method '(nnnil nil))
(setq netrc-file (expand-file-name ".authinfo.gpg" (getenv "HOME")))

(setq gnus-secondary-select-methods
      '((nnimap "bowmansarrow"
                (nnimap-address "imap.gmail.com")
                (nnimap-server-port "imaps")
                (nnimap-stream ssl)
                (nnir-search-engine imap)
                (nnmail-expiry-target "nnimap+bowmansarrow:[Gmail]/Trash")
                (nnmail-expiry-wait 'immediate)
                (nnimap-split-methods default)
                (nnmail-split-methods
                 '(("nnimap+bowmansarrow:PurelyFunctional" "^From:.*eric@purelyfunctional\\.tv.*"))))
        (nnimap "mygoogle"
                (nnimap-address "imap.gmail.com")
                (nnimap-server-port "imaps")
                (nnimap-stream ssl)
                (nnir-search-engine imap)
                (nnmail-expiry-target "nnimap+mygoogle:[Gmail]/Trash")
                (nnmail-expiry-wait 'immediate)
                (nnmail-resplit-incoming t)
                (nnimap-split-methods default)
                (nnmail-split-methods
                 '(("nnimap+mygoogle:SystemCrafters" "^Subject:.*System Crafters Newsletter.*")
                   ("nnimap+mygoogle:SystemCrafters" "^From:.*@lists.systemcrafters\\.net.*")
                   ("nnimap+mygoogle:emacs" "^From:.*david@systemcrafters\\.net.*") 
                   ("nnimap+mygoogle:emacs" "^From:.*david@daviwil\\.com.*")
                   ("nnimap+mygoogle:family" "^From:.*jeff@bowmansarrow\\.us.*")
                   ("nnimap+mygoogle:INBOX" ""))))))



(setq gnus-posting-styles
      '(("bowmansarrow"
         (address "Jeff Bowman <jeff@bowmansarrow.us>")
         ("X-Message-SMTP-Method" "smtp smtp.gmail.com 587 jeff@bowmansarrow.us"))
        ("mygoogle"
         (address "Jeff Bowman <jeff.t.bowman@gmail.com>")
         (signature-file "~/.mygmail-sig")
         ("X-Message-SMTP-Method" "smtp smtp.gmail.com 587 jeff.t.bowman@gmail.com"))))

;; "%U%R%z%I%(%[%4L: %-23,23f%]%) %s\n"
;; U - Unread
;; R - Secondary mark: [AF*S.+-%#] (Answered, Forwarded, Cached, Saved, Seen, Offline[Downloaded], Offline[Not Downloaded], Explicitly Downloaded, Process Mark)
;; z - Zcore
;; I - Indentation based on thread level
;; 4L - Number of lines in article (4 digits)
;; -23,23f - Name, To header or Newsgroups header
;; s - Subject

(defun gnus-user-format-function-o (date-str)
  "%~(form (format-time-string \"%Y-%m-%d %H:%M:%S\" date-str))o")

(setq gnus-summary-line-format "%U%R%z%I%(%[%~(max-right 4)o-%~(cut 4)~(max-right 2)o-%~(cut 6)~(max-right 2)o %~(cut 9)~(max-right 2)o:%~(cut 11)~(max-right 2)o:%~(cut 13)~(max-right 2)o: %-23,23f%]%) %s\n")

(setq gnus-article-sort-functions '((not gnus-article-sort-by-number)))

;; not sure what this does, referenced from:
;; https://github.com/redguardtoo/mastering-emacs-in-one-year-guide/blob/master/gnus-guide-en.org#my-gnusel
;; (setq gnus-topic-topology '(("Gnus" visible)
;;                             ("bowmansarrow" visible nil nil)
;;                             ("mygoogle" visibible nil nil)
;;                             ("misc" visible)))
