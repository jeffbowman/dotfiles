;;; CHNUM --- json schema string->number macro
;;
;; Author: Jeff Bowman
;; Created: 14 September 2018
;;
;;; Commentary:
;;
;; Changes an existing JSON Schema property definition from string to
;; number type
;;
;;; Code:

(fset 'chnum
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([19 115 116 114 105 110 103 return C-backspace 110 117 109 98 101 114 14 14 5 2 134217730 4 5 2 2 67108896 14 5 4 16 16 16 5] 0 "%d")) arg)))


(provide 'chnum)
;;; chnum.el ends here
