;;; CHBOOL --- json schema string->boolean macro
;;
;; Author: Jeff Bowman 
;; Created: 14 September 2018
;;
;;; Commentary:
;;
;; Change an existing JSON Schema property definition from string to
;; boolean type
;;
;;; Code:

(fset 'chbool
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([19 115 116 114 105 110 103 13 C-backspace 98 111 111 108 101 97 110 14 14 5 2 134217730 4 5 2 2 67108896 14 5 4 16 16 16 5] 0 "%d")) arg)))


(provide 'chbool)
;;; chbool.el ends here
