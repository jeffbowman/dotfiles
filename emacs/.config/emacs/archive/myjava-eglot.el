;;; MYJAVA-EGLOT --- Use Eglot for Java development
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 25 September 2020
;;
;;; Commentary:
;;
;; While struggling to get LSP mode to work, was inspired by Mike
;; Zamanskys blog on using Eglot for Java development. This is the
;; configuration from his blog.
;;
;; https://cestlaz.github.io/post/using-emacs-74-eglot/
;;
;;; Code:

(require 'eglot)

;; "/Users/bowman.jeff/.config/emacs/.cache/lsp/eclipse.jdt.ls/plugins/org.eclipse.equinox.launcher_1.5.800.v20200727-1323.jar"

;; FIXME: This seems fragile, an update to this jar file will break things.
(defconst my-eclipse-jdt-home
  (expand-file-name ".cache/lsp/eclipse.jdt.ls/plugins/org.eclipse.equinox.launcher_1.5.800.v20200727-1323.jar" user-emacs-directory))

(defun my-eglot-jdt-server-contact (interactive)
  (let ((cp (getenv "CLASSPATH")))
    (setenv "CLASSPATH" (concat cp ":" my-eclipse-jdt-home))
    (unwind-protect (eglot--eclipse-jdt-contact nil)
      (setenv "CLASSPATH" cp))))
(setcdr (assq 'java-mode eglot-server-programs) #'my-eglot-jdt-server-contact)
(add-hook 'java-mode-hook 'eglot-ensure)


(provide 'myjava-eglot)
;;; myjava-eglot.el ends here
