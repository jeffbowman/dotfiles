;;; MYVTERM --- vterm for better terminal integration
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 23 April 2020
;;
;;; Commentary:
;;    Not available on all systems, notably MacOS and NixOS. Probably
;;    need libvterm to be available to be able to install this. 
;; 
;;
;;; Code:

;; A better term using libvterm within Emacs.
(use-package vterm
  :ensure t
  :demand t)


(provide 'myvterm)
;;; myvterm.el ends here
