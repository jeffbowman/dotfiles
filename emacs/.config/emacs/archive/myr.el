;;; Summary --- R Lang Configuration
;;; Commentary:
;;;    Emacs Speaks Statistics (ESS) configuration for R computing
;;; Code:

(require 'myprogmode)
(require 'eldoc)

(use-package ess)
(use-package ess-R-data-view)
(use-package ess-smart-equals)
(use-package ess-smart-underscore)
(use-package ess-view)

(provide 'myr)
;;; myr.el ends here
