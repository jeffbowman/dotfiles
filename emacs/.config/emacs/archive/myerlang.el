;;; Summary --- Erlang configuration
;;; Commentary:
;;; Code:

(require 'myprogmode)
(require 'eldoc)

(use-package erlang
  :config (progn
	    (sp-use-paredit-bindings)
	    (smartparens-mode)
	    (yas-minor-mode t)
	    (flycheck-mode t)))

;; company backend seems to conflict with haskell mode
;; (use-package company-erlang
;;   :config (add-to-list 'company-backends 'company-erlang))

(use-package ivy-erlang-complete)

(use-package elixir-mode
  :config (yas-minor-mode t))
(use-package elixir-yasnippets
  :after yasnippets)

(provide 'myerlang)
;;; myerlang.el ends here
