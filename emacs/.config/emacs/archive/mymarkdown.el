;;; Summary --- Markdown configuration
;;; Commentary:
;;; Code:

(use-package markdown-preview-mode)
(use-package gh-md)

(provide 'mymarkdown)
;;; mymarkdown.el ends here
