;;; Summary --- org keybindings for prelude
;;;
;;; Commentary:
;;; due to some conflicts with org mode, have to update the keybindings here
;;;
;;; Code:

(define-key prelude-mode-map (kbd "C-c -") nil)
(define-key prelude-mode-map (kbd "C-c +") nil)
(define-key prelude-mode-map [(shift return)] nil)
(define-key prelude-mode-map [(control shift return)] nil)

;;; prelude-org-keys-remap.el ends here
