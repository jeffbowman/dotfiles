;; slack
(use-package slack
  :ensure t
  :disabled t
  :commands (slack-start)
  :init
  (setq slack-buffer-emojify t)
  (setq slack-prefer-current-team t)
  :config
  (slack-register-team
   :name "techlahoma"
   :default t
   :client-id "3558251564.359825199299"
   :client-secret "c1d05c94da78c7ab3eba8b722c872591"
   :token "xoxs-3558251564-25328370054-359523443268-82f5df2dcd"
   :subscribed-channels '(emacs integration mule okc-fp scala)
   :full-and-display-names t)
  (slack-register-team
   :name "geeklunchteam"
   :default t
   :client-id "3558251564.359825199299"
   :client-secret "c1d05c94da78c7ab3eba8b722c872591"
   :token "xoxp-106641995506-106728607269-360458507089-cc8d3329adc12b06bfec571beb63798f"
   :subscribed-channels '(general)
   :full-and-display-names t))

(use-package alert
  :ensure t
  :defer t
  :commands (alert)
  :init
  (setq alert-default-style 'notifier))
