;;; MYSERVER --- Edit Server for use with Chrome
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 19 March 2019
;;
;;; Commentary:
;;
;; Provide the edit-server necessary to allow editing certain text
;; areas from a browser (Google Chrome et. al.)  in emacs
;;
;;; Code:

(use-package edit-server
  :config
  (edit-server-start))

(provide 'myserver)
;;; server.el ends here
