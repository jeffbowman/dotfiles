;;; Summary --- convert tabs to spaces on save
;;;
;;; Commentary:
;;;
;;; Convert tabs to spaces when saving any file except a
;;; Makefile.  May not be the best way to accomplish this
;;;
;;; Code:

(defun untabify-on-save ()
  "This untabifies files on save, unless the file is a Makefile."
  (interactive)
  (unless (eql 'makefile-gmake-mode major-mode)
    (untabify (point-min) (point-max))))

(provide 'untabify)

;;; untabify.el ends here
