;;; Summary --- Time tracking tools
;;; Commentary:
;;; Code:

(use-package timeclock
  :defer t
  :ensure nil				; built-in
  :bind (("C-x ti" . timeclock-in)
	 ("C-x to" . timeclock-out)
	 ("C-x tc" . timeclock-change)
	 ("C-x tr" . timeclock-reread-log)
	 ("C-x tu" . timeclock-update-mode-line)
	 ("C-x tw" . timeclock-when-to-leave-string)))

(provide 'mytimeclock)

;;; mytimeclock.el ends here
