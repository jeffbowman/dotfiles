;;; MYOCAML --- OCaml Configuration
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 30 November 2018
;;
;;; Commentary:
;;
;; Using tuareg and lsp for OCaml development
;;
;;; Code:

(require 'myprogmode)
(require 'mylsp)
(require 'eldoc)

(use-package tuareg
  :defer 1)

(provide 'myocaml)
;;; myocaml.el ends here
