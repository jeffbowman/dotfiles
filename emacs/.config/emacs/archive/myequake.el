;;; MYEQUAKE --- A Quake style terminal running eshell
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 20 April 2020
;;
;;; Commentary:
;;    Bind to key in window manager to use it, but starts an Emacs
;;    client running a shell (eshell by default but others are
;;    possible)
;;
;;
;;; Code:

(require 'mypackage-config)
(mypackage-require-packages '(equake))

(require 'equake)
;; set width a bit less than full-screen (prevent 'overflow' on multi-monitor)
(customize-set-variable 'equake-size-width 0.99)

;; prevent accidental frame-closure
(define-key global-map (kbd "C-x C-c")
  #'equake-check-if-in-equake-frame-before-closing)


;; needed to detect the correct monitor in multi-head situations,
;; requires xdotool to be installed (pacman -S xdotool), pretty
;; sure this doesn't work in MS Windows.
;; TODO: guard this line to only run on Linux hosts.
(setq equake-use-xdotool-probe t)


;; set distinct face for Equake: white foreground with dark blue
;; background, and different font
(custom-set-faces (backquote (equake-buffer-face ((nil (:inherit default :family "DejaVu Sans Mono"
                                                                 :background "#000022" :foreground "white"))))))


(provide 'myequake)
;;; myequake.el ends here
