;;; myexwm --- Summary
;;;
;;; Commentary:
;;;
;;; Configuration for Emacs as a window manager. Lifted a *large*
;;; amount of code and ideas from the "Emacs from Scratch" YouTube
;;; series by David Wilson.
;;;
;;; GitHub: https://github.com/daviwil/emacs-from-scratch
;;; YouTube: https://www.youtube.com/channel/UCAiiOTio8Yu69c3XnR7nQBQ
;;;
;;; Code:

;; EXWM stuff
(use-package xelb
  :ensure t)

;; functions lifted from emacs-from-scratch project by GitHub user daviwil
(defun my/run-in-background (command)
  "Runs COMMAND in the background.

COMMAND is a string, the first word of which is assumed to be the
command to run and the remaining words the parameters for the command."
  
  (let ((command-parts (split-string command "[ ]+")))
    (apply #'call-process `(,(car command-parts) nil 0 nil ,@(cdr command-parts)))))

(defun my/exwm-update-class ()
  "Updates buffer name based on the window class name"
  (exwm-workspace-rename-buffer exwm-class-name))

(defun my/exwm-update-title ()
  "Updates window title"
  (pcase exwm-class-name
    ("firefox" (exwm-workspace-rename-buffer (format "Firefox: %s" exwm-title)))
    ("brave-browser" (exwm-workspace-rename-buffer (format "Brave: %s" exwm-title)))))

(defun my/position-window ()
  "Moves floating windows to top left corner."
  (let* ((pos (frame-position))
	 (pos-x (car pos))
	 (pos-y (cdr pos)))
    (exwm-floating-move (- pos-x) (- pos-y))))

(defun my/configure-window-by-class ()
  "Configure where some applications land when started."
  (interactive)
  (pcase exwm-class-name
    ;; move firefox to workspace 3
    ("firefox" (exwm-workspace-move-window 3))
    ;; move brave to workspace 4
    ("brave-browser" (exwm-workspace-move-window 4))))

(defun my/set-wallpaper ()
  "Set the desktop background wallpaper, uses feh

If `feh` is not installed, this function does nothing." 
  (interactive)
  (when (and (file-exists-p "/usr/bin/feh")
	     (file-executable-p "/usr/bin/feh"))
    (start-process-shell-command "feh" nil (concat "sh " (expand-file-name "~/.fehbg")))))

(defun my/update-displays ()
  "Handle multiple monitors

Uses autorandr to switch configurations between stand-alone and
multiple monitor mode. Currently the multiple monitor
configuration is called `'`home''"
  
  (my/run-in-background "autorandr --change --force")
  (message "Display config: %s"
	   (string-trim (shell-command-to-string "autorandr --current"))))

(defun my/autostart-default-apps ()
  "Start default apps

Apps to start each login
* slack
* keepassxc"
  (interactive)
  (split-window-right)
  (start-process "KeePassXC" nil "/usr/bin/keepassxc")
  (start-process "Slack" nil "/usr/bin/slack")
  (sleep-for 2)
  (switch-to-buffer-other-window "KeePassXC"))

(defun my/exwm-init-hook ()
  "Init-hook called from the exwm-init-hook

Handles autostarting some applications and some initial settings
not already performed elsewhere"
  
  ;; Set initial workspace
  (exwm-workspace-switch-create 0)

  ;; Show battery status
  (display-battery-mode 1)

  ;; Start polybar
  (my/start-panel)

  ;; Setup SSH environment
  (unless (getenv "SSH_AUTH_SOCK")
    (let ((ssh-process (start-process "ssh-agent.service" nil "/usr/bin/systemctl --user status ssh-agent.service")))
      (unless (= (process-exit-status ssh-process) 0)
	(start-process "ssh-agent.service" nil "/usr/bin/systemctl --user start ssh-agent.service")))
    (setenv "SSH_AUTH_SOCK" "/run/user/1002/ssh-agent.socket"))

  ;; Automatically start some apps
  (my/run-in-background "nm-applet")
  (my/run-in-background "megasync")
  (my/run-in-background "pasystray")
  (my/run-in-background (concat "rclone mount mygoogledrive: " (expand-file-name "~/GoogleDrive_JTB") " --vfs-cache-mode full --daemon"))

  ;; Sleep to give rclone a chance to sync
  (sleep-for 2)

  ;; Start default applications
  (my/autostart-default-apps))

(use-package exwm
  :ensure t
  :demand t

  :bind (([s-f7] . #'eshell)
	 ([C-s-x] . (lambda () (interactive) (start-process "" nil "/usr/bin/i3lock -c 000011")))
	 ([s-return] . (lambda () (interactive) (start-process "" nil "/usr/bin/kitty"))))

  :custom
  (exwm-workspace-number 10)
  (exwm-workspace-warp-cursor t)
  (mouse-autoselect-window t)
  (focus-follows-mouse t)
  (exwm-input-prefix-keys
   '(?\C-x
     ?\C-u
     ?\C-h
     ?\M-x
     ?\M-`
     ?\M-&
     ?\M-:))
  (exwm-input-global-keys
   `(
     ;; Reset to line mode (C-c C-k switches to char mode)
     ([?\s-r] . exwm-reset)

     ;; Move between windows
     ([s-left] . windmove-left)
     ([s-right] . windmove-right)
     ([s-up] . windmove-up)
     ([s-down] . windmove-down)

     ;; Launch applications via shell command
     ([?\s-&] . (lambda (command)
		  (interactive (list (read-shell-command "$ ")))
		  (start-process-shell-command command nil command)))

     ;; Switch workspace
     ([?\s-w] . exwm-workspace-switch)

     ;; s-N: Switch to certain workspace (super + number [0-9])
     ,@(mapcar (lambda (i)
		 `(,(kbd (format "s-%d" i)) .
		   (lambda ()
		     (interactive)
		     (exwm-workspace-switch-create ,i))))
	       (number-sequence 0 9))))
  :config
  ;; When window class updates, use it to set the buffer name
  (add-hook 'exwm-update-class-hook #'my/exwm-update-class)

  ;; When window title updates, use it to set the buffer name
  (add-hook 'exwm-update-title-hook #'my/exwm-update-title)

  ;; Configure windows as they are created
  (add-hook 'exwm-manage-finish-hook #'my/configure-window-by-class)

  ;; When EXWM starts up, do some extra configuration
  (add-hook 'exwm-init-hook #'my/exwm-init-hook)

  ;; Swap caps and control keys
  (start-process-shell-command "xmodmap" nil (concat "/usr/bin/xmodmap " (expand-file-name "~/.xmodmap.swapcaps")))

  ;; Set monitor locations, resolution, etc. 
  (require 'exwm-randr)
  (exwm-randr-enable)
  ;; Configure laptop monitor
  (start-process-shell-command
   "/usr/bin/xrandr" nil "xrandr --output LVDS1 --mode 1920x1080")

  ;; Configure HDMI monitor
  (start-process-shell-command
   "/usr/bin/xrandr" nil
   "xrandr --auto --output HDMI1 --mode 1920x1080 --right-of LVDS1")
  
  ;; Put a workspace on a particular monitor. 0-2 are on LVDS1 by
  ;; default (ie the laptop monitor).
  (setq exwm-randr-workspace-monitor-plist
	'(3 "HDMI1" 4 "HDMI1" 5 "HDMI1" 6 "HDMI1" 7 "HDMI1" 8 "HDMI1" 9 "HDMI1"))

  ;; Handle updating monitors when attaching/detaching HDMI cable
  (add-hook 'exwm-randr-screen-change-hook #'my/update-displays)
  (my/update-displays)

  ;; Ctrl-Q will enable the next key to be sent directly
  (define-key exwm-mode-map [?\C-q] 'exwm-input-send-next-key)

  (exwm-input-set-key (kbd "s-p") 'counsel-linux-app)
  (exwm-enable))

;; enables volume controls, etc
(use-package desktop-environment
  :ensure t
  :after exwm
  :config (desktop-environment-mode))

;; Polybar
(unless (daemonp)
  (server-start))

(defvar my/polybar-process nil
  "Holds the process of the running Polybar instance, if any")

(defun my/kill-panel ()
  "Stop a running polybar panel"
  (interactive)
  (when my/polybar-process
    (ignore-errors
      (kill-process my/polybar-process)))
  (setq my/polybar-process nil))

(defun my/start-panel ()
  "Start a polybar instance

As a side effect, retains the PID in the polybar-process."
  (interactive)
  (my/kill-panel)
  (setq my/polybar-process (start-process-shell-command "polybar" nil "polybar panel")))

(defun my/send-polybar-hook (module-name hook-index)
  "Sends a message to the polybar.

This can be used to configure the polybar to contain any bits of data needed."
  (start-process-shell-command "polybar-msg" nil (format "polybar-msg hook %s %s" module-name hook-index)))

(defun my/send-polybar-exwm-workspace ()
  "Update the workspace number on the polybar."
  (my/send-polybar-hook "exwm-workspace" 1))

(add-hook 'exwm-workspace-switch-hook #'my/send-polybar-exwm-workspace)


(provide 'myexwm)

;; Desktop entry in /usr/share/xsssions/exwm.desktop
;;
;; [Desktop Entry]
;; Name=EXWM
;; Comment=Emacs X Window Manager
;; TryExec=emacs
;; Exec=dbus-launch --exit-with-session emacs -mm --debug-init
;; Type=Application


;;; myexwm.el ends here
