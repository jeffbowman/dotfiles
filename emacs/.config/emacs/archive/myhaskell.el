;;; Summary --- Haskell configuration
;;; Commentary:
;;; Code:

(require 'myprogmode)
(require 'eldoc)

(use-package haskell-mode
  :config (progn
            (setq haskell-stylish-on-save t)
            (add-hook 'haskell-mode-hook (quote (haskell-indent-mode
                                                 highlight-uses-mode
                                                 interactive-haskell-mode
                                                 turn-on-askell-unicode-input-method)))
            (company-mode t)))

(use-package intero
  :config (setq intero-global-mode 1))

(use-package flycheck-haskell
  :after flycheck
  :config (progn
            (setq flycheck-ghc-args (quote ("-dynamic")))))

(use-package company-ghci
  :after company-mode
  :config (add-to-list 'company-backends 'company-ghci))

(provide 'myhaskell)
;;; myhaskell.el ends here
