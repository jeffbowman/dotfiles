;;; MYJULIA --- Julia Programming Configuration
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 26 April 2019
;;
;;; Commentary:
;;
;; Julia language programming configuration
;;
;;; Code:

(require 'mylsp)

(use-package flycheck-julia)
(use-package julia-mode)
(use-package julia-repl)
(use-package julia-shell)

;; configuration from :
;; https://discourse.julialang.org/t/emacs-based-workflow/19400/13
;;
;; more/different configuration at
;; https://github.com/JuliaEditorSupport/LanguageServer.jl/wiki/Emacs
;;
;; lsp-julia is not in elpa/melpa, so this won't quite work.
;; (use-package lsp-julia
;;   :init
;;   (setq lsp-prefer-flymake nil)

;;   :config
;;   (defun ff/lsp-eldoc-advice (orig-fun &rest args)
;;     "Show only first line of documentation.

;; Prevent long documentation showing up in the echo area from
;; messing up window configuration."
;;     (let ((msg (car args)))
;;       (if msg
;; 	  (funcall orig-fun (->> msg (s-trim-left)
;; 				 (s-split "\n")
;; 				 (first))))))
;;   (advice-add 'lsp--eldoc-message :around #'ff/lsp-eldoc-advice)

;;   (defun ff/lsp-disable-server-autorestart ()
;;     (setq lsp-restart nil))
;;   (add-hook 'kill-emacs-hook #'ff/lsp-disable-server-autorestart))

;; (use-package lsp-ui
;;   :ensure t
;;   :init
;;   (setq lsp-ui-doc-enable nil)

;;   :config
;;   (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
;;   (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references))

(provide 'myjulia)
;;; myjulia.el ends here
