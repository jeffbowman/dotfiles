;;; Summary --- Ansible configuration
;;; Commentary:
;;; Code:

(use-package yaml-mode)

(use-package ansible
  :config (company-mode))

(use-package ansible-vault)

(provide 'myansible)
;;; myansible.el ends here
