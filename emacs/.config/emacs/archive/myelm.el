;;; Summary --- Elm configuration
;;; Commentary:
;;; Code:

(require 'myprogmode)
(require 'eldoc)

(use-package elm-mode
  :config (yas-minor-mode t))

(use-package elm-yasnippets)

(provide 'myelm)
;;; myelm.el ends here
