;;; Summary --- Go Lang Configuration
;;; Commentary:
;;; Code:

(require 'myprogmode)
(require 'eldoc)

(use-package go-eldoc)
(use-package go-errcheck)
(use-package go-impl)

(use-package go-mode
  :hook (go-mode-hook . flycheck-mode))

(use-package company-go
  :hook go-mode
  :config (add-to-list 'company-backends 'company-go))

(provide 'mygo)

;;; mygo.el ends here
