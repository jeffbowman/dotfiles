;;; EARLY-INIT --- Early Customization
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created:  5 May 2021
;;
;;; Commentary:
;;
;; Some things to customize before Emacs initializes the GUI and runs init.el
;;
;;; Code:

;; turn off garbage collection, will turn it back on after Emacs has
;; finished initializing
(setq gc-cons-threshold most-positive-fixnum)

;; turn off the toolbar
(push '(tool-bar-lines . 0) default-frame-alist)

(cond
 ((string-equal "darwin" (symbol-name system-type))
  (custom-set-faces (backquote (default ((t (:family "Monaco" :height 180)))))))
 ((string-equal "windows-nt" (symbol-name system-type))
  (custom-set-faces (backquote (default ((t (:family "Lucida Console" :height 160)))))))
 (t                                     ; must be linux
  (custom-set-faces (backquote (default ((t (:family "Anonymous Pro" :height 160))))))))

(provide 'early-init)
;;; early-init.el ends here
