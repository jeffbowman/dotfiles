# GNU Emacs Configuration

## Overview

This is my emacs configuration. It follows similar patterns to [GNU
Emacs](https://www.gnu.org/software/emacs/)
[Prelude](https://github.com/bbatsov/prelude). I used prelude in the
past, but decided to try to speed up my configuration and learn a bit
more about customizing how I wanted [GNU
Emacs](https://www.gnu.org/software/emacs/) to work. This is the
result.

## Usage

Clone this repository. Look in the `sample` directory to find the
`sample-mymodule.el` and copy this file to the `personal` folder as
`mymodules.el`. Inside that file, find the list of "modules" to enable
or not by uncommenting the appropriate lines. If you have ever used
[GNU Emacs](https://www.gnu.org/software/emacs/)
[Prelude](https://github.com/bbatsov/prelude), this will be very
familiar. Key differences between this and Prelude:

* Well, it's **not** [Prelude](https://github.com/bbatsov/prelude)!
* It is _my_ configuration for the things that _I_ need.
* It is not a general solution that everyone will appreciate.

All that being said, if you find it useful, please use it! I don't
need to know if you make changes, but if you want to send a pull
request, that would be fine too. Use at your own risk or benefit. Copy
at will, or don't as you see fit. 

## Attribution

* Props to [Bozhidar Batsov](https://github.com/bbatsov) for a great
tool from which I took many concepts and learned many things.
* Props to the good folks who maintain [GNU
Emacs](https://www.gnu.org/software/emacs), the best and most fun tool
/ editor / environment / operating system / whatever I've used
