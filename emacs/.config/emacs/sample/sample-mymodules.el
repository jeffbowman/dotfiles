;;; MYMODULES --- Modules to load on init
;;
;; Author: Jeff Bowman 
;; Created: 13 September 2018
;;
;;; Commentary:
;;
;; Similar in concept to the method used by
;; [[https://github.com/bbatsov/prelude][Emacs Prelude]], load the
;; modules needed. Copy the sample from
;; ~/.emacs.d/sample/sample-mymodules.el ~/.emacs.d/personal/mymodules.el to load.
;;
;; N.B. This file is NOT tracked by version control
;;
;;; Code:

(require 'mymodule-config)

;; look in ../modules for more modules to activate.
;; here is an example activation.

(mymodule-activate "mylisp.el")

(let ((personal-dir (locate-user-emacs-file "personal/")))
  (when (file-exists-p (expand-file-name "mycustom.el" personal-dir))
    (require 'mycustom)))

(provide 'mymodules)
;;; mymodules.el ends here
