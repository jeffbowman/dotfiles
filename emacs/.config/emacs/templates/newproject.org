#+STARTUP: lognotereschedule logdrawer
#+SEQ_TODO: TODO(t/!) IN_PROGRESS(i/!) HOLD(h@/!) READY_FOR_QA(r@/!) | DONE(d@) CANCELED(c@)
#+COLUMNS: %TODO %50ITEM %CLOCKSUM_T(today) %CLOCKSUM(total)

* TODO Initial Project Sync 
** People
   - MuleSoft DM
   - Client POCs and roles
** Questions
   - Base level questions to get answered. Others added at this level.
*** Level of Experience
    - Has Mule been used before? (see next question as well)
    - How many in-house Mule Developers does this client have?
    - How long have they been developing Mule solutions? 
*** Existing Solutions
    - Has this client deployed any other Mule solutions?
*** Problem to Solve
    - What does this client need?
*** Platform
    - Mule 3 vs Mule 4
    - On-premise, CloudHub, Hybrid?
**** Connectors
     - Database
     - APIs
     - Remote systems (Workday, Concur, Medallia, etc)?
*** Infrastructure
    - Version control - assuming git, but hosted where?
    - Continuous build (Jenkins, Bamboo, Travis, CircleCI, etc)?
* Notes
  - Space for additional information before the contract starts.
* Access
  - VPN
  - Git
  - Confluence or other wiki type documentation site
  - Databases
  - Remote systems
* Work
** Time
** Meetings
*** Recurring
**** DSU
*** One-Time
** Deliverables
*** Documenation
