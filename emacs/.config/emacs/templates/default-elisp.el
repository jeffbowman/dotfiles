;;; `(upcase (file-name-nondirectory (file-name-sans-extension (buffer-file-name))))` --- $1
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: `(format-time-string "%e %B %Y")`
;;
;;; Commentary:
;;
;; $2
;;
;;; Code:

$0

(provide '`(file-name-nondirectory (file-name-sans-extension (buffer-file-name)))`)
;;; `(file-name-nondirectory (buffer-file-name))` ends here
