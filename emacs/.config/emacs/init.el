;;; init.el --- emacs configuration
;;; Commentary:
;;; Code:

(add-to-list 'load-path (expand-file-name "core" user-emacs-directory))

;; load packages, provides some package utilities
(require 'mypackage-config)

;; loads base configuration
(require 'mycore-config)

;; loads editor and ui configuration
(require 'myui-config)

;; loads personal customizations
(add-to-list 'load-path
             (expand-file-name "personal" user-emacs-directory))
(require 'mymodules)

;;; init.el ends here
