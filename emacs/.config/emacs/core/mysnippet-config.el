;;; MYSNIPPET-CONFIG --- yasnippet and autoinsert configuration
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 20 May 2021
;;
;;; Commentary:
;;
;; yasnippet provides expandable snippets to speed up typing in many
;; cases when needing common text/programming constructs.
;;
;; autoinsert uses yasnippet to provide templates for inserting
;; boilerplate contents for new files.
;;
;;; Code:

(run-with-idle-timer 1 nil #'require 'yasnippet nil t)
(with-eval-after-load "yasnippet"
  (diminish 'yas-minor-mode)
  (require 'yasnippet-snippets)
  (yas-global-mode)

  (defun mysnippet-autoinsert-yas-expand ()
    "Replace text in yasnippet template."
    (yas-expand-snippet (buffer-string) (point-min) (point-max)))

  (require 'autoinsert)
  (add-hook 'find-file-hook #'auto-insert)
  ;; Don't want to be prompted before insertion:
  (customize-set-variable 'auto-insert-query nil)
  (customize-set-variable 'auto-insert-directory
                          (locate-user-emacs-file "templates"))
  (define-auto-insert "\\.el?$" ["default-elisp.el" mysnippet-autoinsert-yas-expand])
  (define-auto-insert "pom.xml" ["default-pom.xml" mysnippet-autoinsert-yas-expand])
  (auto-insert-mode))


(provide 'mysnippet-config)
;;; mysnippet-config.el ends here
