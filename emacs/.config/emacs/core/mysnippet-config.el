;;; MYSNIPPET-CONFIG --- yasnippet and autoinsert configuration -*- lexical-binding: t; -*-
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 20 May 2021
;;
;;; Commentary:
;;
;; yasnippet provides expandable snippets to speed up typing in many
;; cases when needing common text/programming constructs.
;;
;; autoinsert uses yasnippet to provide templates for inserting
;; boilerplate contents for new files.
;;
;;; Code:

(run-with-idle-timer 1 nil #'require 'yasnippet nil t)
(with-eval-after-load "yasnippet"
  (diminish 'yas-minor-mode)
  (require 'yasnippet-snippets)
  (yas-global-mode)

  (defun mysnippet-autoinsert-yas-expand ()
    "Replace text in yasnippet template."
    (yas-expand-snippet (buffer-string) (point-min) (point-max)))

  (require 'autoinsert)
  (add-hook 'find-file-hook #'auto-insert)
  ;; Don't want to be prompted before insertion:
  (customize-set-variable 'auto-insert-query nil)
  (customize-set-variable 'auto-insert-directory
                          (locate-user-emacs-file "templates"))
  (define-auto-insert "\\.el?$" ["default-elisp.el" mysnippet-autoinsert-yas-expand])
  (define-auto-insert "pom.xml" ["default-pom.xml" mysnippet-autoinsert-yas-expand])
  (auto-insert-mode))

(when (featurep 'autoinsert)
  (auto-insert-mode))

;; uses builtin auto-insert mechanism, not yasnippets as above
(with-eval-after-load "autoinsert"
  ;; if it is an interview, load a blank interview file.
  (define-auto-insert
    ".*/interviews/[a-z_-]+[0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}.org" "blank-interview.org" nil)
  (define-auto-insert
    ".*/work/appnovation/\\(projects\\|mulesoft\\)/.*/.*\\.org" "newproject.org"))

;;; snippets
;; use built-in tempo mode to create snippets, a little more verbose
;; than yasnippet, but only barely, and used with abbrev-mode
;; turn on abbrev-mode everywhere
(setq-default abbrev-mode t)

;; use tempo mode to define snippets
(require 'tempo)
(customize-set-variable 'tempo-interactive t) ; allows for prompting
                                              ; when inserting
                                              ; snippets
;; define org header for new org files
(tempo-define-template
 "org-header"
 '("#+STARTUP: lognotereschedule logdrawer overview" n
   "#+SEQ_TODO: TODO(t/!) IN_PROGRESS(i/!) HOLD(h@/!) READY_FOR_QA(r@/!) | DONE(d) CANCELED(c@)" n
   "#+COLUMNS: %50ITEM %CLOCKSUM %CLOCKSUM_T" n)
 "orgheader"
 "Insert standard Org header"
 nil)

;; define a new blog entry (also found in my custom yasnippets)
(tempo-define-template
 "newblog"
 '("#+TITLE: " (p "Title: " title) n
   "#+OPTIONS: toc:nil num:nil todo:nil pri:nil tags:nil ^:nil" n
   "#+DESCRIPTION: " (p "Description: " description) n
   "#+DATE: " (org-time-stamp t) n n
   p
   n "Tags: " n
   (progn
     (tempo-forward-mark)
     (when (package-installed-p 'writefreely)
       (load-write-freely)
       (write-freely-mode)))))

;; define bills table for personal bill tracking
(tempo-define-template
 "billtable"
 '("| Account             | Due Date | Date Paid | Amount Due | Amount Paid | Total |" n>
   "|---------------------+----------+-----------+------------+-------------+-------|" n>
   "| OG&E                |          |           |            |             |       |" n>
   "| Capitol One         |          |           |            |             |       |" n>
   "| ONG                 |          |           |            |             |       |" n>
   "| College Tuition     |          |           |            |             |       |" n>
   "| USAA Visa           |          |           |            |             |       |" n>
   "| American Express    |          |           |            |             |       |" n>
   "|                     |          |           |            |             |       |" n>
   "| USAA Visa           |          |           |            |             |       |" n>
   "| American Express    |          |           |            |             |       |" n>
   "| OKC Utilities       |          |           |            |             |       |" n>
   "| TFCU                |          |           |            |             |       |" n>
   "| Nelnet Student Loan |          |           |            |             |       |" n>
   "|                     |          |           |            |             |       |" n>
   "#+TBLFM: @8$6=vsum(@2$5..@8$5)::@>$6=vsum(@9$5..@>>$5)" n
   ))

;; Set an abbrev to expand to the tempo snippet above, but have to
;; wrap these to avoid errors where the abbrev table is not created
;; when Emacs starts.
(with-eval-after-load "org"
  (define-abbrev org-mode-abbrev-table "billtable" "" 'tempo-template-billtable)
  (define-abbrev org-mode-abbrev-table "newblog" "" 'tempo-template-newblog)
  (define-abbrev org-mode-abbrev-table "orgheader" "" 'tempo-template-org-header))

(provide 'mysnippet-config)
;;; mysnippet-config.el ends here
