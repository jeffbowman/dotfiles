;;; MYORG-CONFIG --- org mode configuration
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 20 May 2021
;;
;;; Commentary:
;;
;; Default org-mode configuration
;;
;;; Code:

(require 'mypackage-config)

(mypackage-require-packages '(org org-contrib org-present verb))

(autoload 'org-mode "org" nil t)
;; not sure if I need this when org-contrib is available
;;  :ensure org-plus-contrib

;; keys I want to be able to jump to the agenda regardless of the
;; mode, sometimes I want to capture things to put into an org file,
;; also regardless of the mode, so bind these to a global leader (ie:
;; C-c o). 
(define-key global-map (kbd "C-c o a") #'org-agenda)
(define-key global-map (kbd "C-c o t") #'org-capture)

;; customizations
(customize-set-variable 'org-export-backends (quote (ascii html icalendar latex md odt)))
(customize-set-variable 'org-directory "~/org")
(customize-set-variable 'org-refile-allow-creating-parent-nodes 'confirm)
(customize-set-variable 'org-refile-targets (quote ((nil :maxlevel . 9)
                                                    (org-agenda-files :maxlevel . 9))))
(customize-set-variable 'org-refile-use-outline-path 'file)
(customize-set-variable 'org-outline-path-complete-in-steps nil)
(customize-set-variable 'org-todo-keyword-faces
                        '(("HOLD" . (:foreground "cyan" :weight "bold"))
                          ("READY_FOR_QA" . (:foreground "dark green" :underline t))
                          ("IN_PROGRESS" . (:foreground "DarkOliveGreen1" :weight "bold"))))
(customize-set-variable 'org-capture-templates
                        '(("n" "Note" entry (file+headline "~/.notes" "Notes")
                           "** %U %?\n%i")
                          ("t" "Todo" entry (file+headline "~/.notes" "Tasks")
                           "** TODO %?\n  %i\n  %a")
                          ("i" "Issue" entry (file+headline "~/.notes" "Issues")
                           "* IN_PROGRESS %?
:PROPERTIES:
:ONSET:
:DESCRIPTION:
:ACTION:
:COMPLETED:
:END:" :empty-lines 0)))


(with-eval-after-load "org"
  ;; N.B. This may need to be re-evaluated if another directory is
  ;; added to the org tree.
  ;;
  ;; N.B. 2 - This needs to be setq here rather than in the :custom
  ;; section as the org-agenda-file-regexp is not defined at the right
  ;; time for this to be used in :custom instead.
  ;;
  ;; Change the org-agenda-files to only reflect agenda from work
  ;; files. This should speed up both loading an org file as well as
  ;; the agenda view.
  (setq org-agenda-files
        (directory-files-recursively (expand-file-name "work" org-directory)
                                     org-agenda-file-regexp))

  ;; verb mode is for http/REST testing in org mode
  (autoload 'verb-mode "verb" nil t)
  (define-key org-mode-map (kbd "C-c r") verb-command-map)

  ;; #'org-mark-ring-goto is the corollary to
  ;; #'org-open-at-point, ie jump back to where we came from
  ;; when we pressed C-c C-o to follow an internal link
  ;;
  ;; This is bound to 'C-c &' by org mode, however, yas overwrites
  ;; this, using 'C-c &' as a leader prefix for yas related
  ;; functions. Moving this to org-mode-map from global-map since it
  ;; should needs to be bound there.
  (define-key org-mode-map (kbd "C-c b")   #'org-mark-ring-goto)

  ;; language support in org source blocks
  (org-babel-do-load-languages 'org-babel-load-languages
                               '((awk . t)
                                 (emacs-lisp . t)
                                 (shell . t))))

;; org mode presentations. Use M-x org-present to start the
;; presentation.
(eval-after-load "org-present"
  '(progn
     (add-hook 'org-present-mode-hook
               (lambda ()
                 (org-present-big)
                 (org-present-read-only)))
     (add-hook 'org-present-mode-quit-hook
               (lambda ()
                 (org-present-small)
                 (org-present-read-write)))))

(provide 'myorg-config)
;;; myorg-config.el ends here
