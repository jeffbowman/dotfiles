;;; MYGLOBAL-CONFIG --- global customizations -*- lexical-binding: t; -*-
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 20 May 2021
;;
;;; Commentary:
;;
;; Global variables, keybindings, etc.
;;
;;; Code:

;; When using chemacs, this seems to get changed somewhere. This value
;; is the default, just replacing it as the current setting.
(customize-set-variable 'abbrev-file-name
                        (expand-file-name "abbrev_defs" user-emacs-directory))
(customize-set-variable 'inhibit-startup-screen t)
(customize-set-variable 'display-time-24hr-format t)
(customize-set-variable 'use-file-dialog nil)
(customize-set-variable 'indent-tabs-mode nil)
(customize-set-variable 'load-prefer-newer t)
(customize-set-variable 'projectile-project-search-path '("~/Projects/"))
(customize-set-variable 'dired-dwim-target t)

(put 'narrow-to-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'scroll-left 'disabled nil)

;; global modes/functions
(column-number-mode t)
(delete-selection-mode t)
(display-time-mode t)
(electric-pair-mode t)
(global-display-line-numbers-mode)
(show-paren-mode t)

;; global key configurations
(define-key global-map (kbd "C-+")     #'text-scale-increase)
(define-key global-map (kbd "C--")     #'text-scale-decrease)
(define-key global-map (kbd "<f5>")    #'execute-extended-command)
(define-key global-map (kbd "<f6>")    #'isearch-forward)
(define-key global-map (kbd "M-Z")     #'zap-to-char)
(define-key global-map (kbd "M-z")     #'zap-up-to-char)
(define-key global-map (kbd "C-x C-b") #'ibuffer)
(define-key global-map (kbd "M-u")     #'upcase-dwim)
(define-key global-map (kbd "M-l")     #'downcase-dwim)

;; turn on repeat mode in Emacs 28+, allows repeating commands, ala
;; C-x { { to shrink a window rather than C-x { C-x {
(unless (version< emacs-version "28")
  ;; When user option 'repeat-keep-prefix' is non-nil, the prefix arg
  ;; of the previous command is kept.  This can be used to
  ;; e.g. reverse the window navigation direction with 'C-x o M-- o o'
  ;; or to set a new step with 'C-x { C-5 { { {', which will set the
  ;; window resizing step to 5 columns.
  ;;
  ;; HOWEVER: setting this to non-nil stops repeat mode from
  ;; activating the repeat keys.
  (customize-set-variable 'repeat-keep-prefix nil)
  (repeat-mode 1))

(provide 'myglobal-config)
;;; myglobal-config.el ends here
