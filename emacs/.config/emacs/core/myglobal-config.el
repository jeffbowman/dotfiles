;;; MYGLOBAL-CONFIG --- global customizations
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 20 May 2021
;;
;;; Commentary:
;;
;; Global variables, keybindings, etc.
;;
;;; Code:

(customize-set-variable 'inhibit-startup-screen t)
(customize-set-variable 'display-time-24hr-format t)
(customize-set-variable 'use-file-dialog nil)
(customize-set-variable 'ivy-use-selectable-prompt t)
(customize-set-variable 'indent-tabs-mode nil)
(customize-set-variable 'load-prefer-newer t)
(customize-set-variable 'projectile-project-search-path '("~/Projects/"))
(customize-set-variable 'dired-dwim-target t)

(put 'narrow-to-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'scroll-left 'disabled nil)

;; global modes/functions
(column-number-mode t)
(delete-selection-mode t)
(display-time-mode t)
(electric-pair-mode t)
(global-display-line-numbers-mode)
(show-paren-mode t)

;; global key configurations
(define-key global-map (kbd "C-+")     #'text-scale-increase)
(define-key global-map (kbd "C--")     #'text-scale-decrease)
(define-key global-map (kbd "<f5>")    #'execute-extended-command)
(define-key global-map (kbd "<f6>")    #'isearch-forward)
(define-key global-map (kbd "M-Z")     #'zap-to-char)
(define-key global-map (kbd "M-z")     #'zap-up-to-char)
(define-key global-map (kbd "C-x C-b") #'ibuffer)
(define-key global-map (kbd "M-u")     #'upcase-dwim)
(define-key global-map (kbd "M-l")     #'downcase-dwim)


(provide 'myglobal-config)
;;; myglobal-config.el ends here
