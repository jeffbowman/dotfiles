;;; MYMODULE-CONFIG --- module operations
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created:  3 June 2021
;;
;;; Commentary:
;;
;; Code to activate and deactivate modules. For use in the
;; =mymodules.el= file found in the =personal= directory.
;;
;; Available modules (found in =mymodule-available-dir=) are symlinked
;; (or copied in the case of Microsoft Windows) to the
;; =mymodule-active-dir= folder and loaded from there. The
;; =mymodule-active-dir= should be on the =load-path= while the
;; =mymodule-available-dir= should not.
;;
;;; Code:

;; should these be defcustom instead of defvar?
(defvar mymodule-available-dir (locate-user-emacs-file "modules/")
  "The location (a directory) where uninstalled modules are
  hosted. This location *should not* be on the LOAD-PATH")

(defvar mymodule-active-dir (locate-user-emacs-file "personal/")
  "The location (a directory) where modules will be
  installed. This directory should be on the LOAD-PATH so
  installed modules are loaded for future sessions")

(defmacro mymodule-activate (module)
  "Install symlink to source and compiled MODULE files

Gets MODULE from MODULE-AVAILABLE-DIR to create a symlink in
MODULE-ACTIVE-DIR, byte compiles the module after creating the
symlink and then loads the MODULE. If the MODULE is already
installed, just load it."

  ;; initially create names for use in the macro itself. This is not
  ;; really needed as the only input is MODULE which is a string, but
  ;; it means the symbols created can *only* be used here in this
  ;; macro.
  (let ((module-name (make-symbol (file-name-base module)))
        (load-name (make-symbol "load-name"))
        (available-module-file (make-symbol "available-module-file"))
        (active-module-file (make-symbol "active-module-file")))
    `(let* ((,module-name ,module)
            (,load-name (file-name-base ,module))
            (,available-module-file (expand-file-name ,module-name
                                                      mymodule-available-dir))
            (,active-module-file (expand-file-name ,module-name
                                                   mymodule-active-dir)))
       (if (file-exists-p ,active-module-file)
           (if (featurep ',module-name)
               (require ',module-name nil t)
             (load ,load-name t))
         (if (string= "windows-nt" system-type)
             ;; if this is Microsoft Windows, we can't use symlinks, so
             ;; copy the file instead.
             (copy-file ,available-module-file mymodule-active-dir)
           ;; Linux or Apple Mac can handle symlinks
           (make-symbolic-link ,available-module-file
                               mymodule-active-dir
                               t))
         (byte-compile-file ,active-module-file)
         (load ,load-name t)))))

(defun mymodule-deactivate (module)
  "Deactivate a MODULE by removing it from the active folder."

  (interactive "smodule: ")
  (let ((module-src-name (expand-file-name module module-active-dir))
        (module-compiled-name (expand-file-name
                               (concat (file-name-base module) ".elc")
                               module-active-dir)))
    (when (file-exists-p module-src-name)
      (delete-file module-src-name))
    (when (file-exists-p module-compiled-name)
      (delete-file module-compiled-name))))

(provide 'mymodule-config)
;;; mymodule.el ends here
