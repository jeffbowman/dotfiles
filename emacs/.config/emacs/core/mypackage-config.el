;;; MYPACKAGE-CONFIG --- package configuration and utilities
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 19 May 2021
;;
;;; Commentary:
;;
;; Some utilities and default settings for package.el.
;;
;; Some parts are Copyright © 2011-2021 Bozhidar Batsov, used with
;; permission
;;
;;; Code:

(require 'package)
(require 'cl-lib)

;; archives for getting packages.
(setq package-archives
        (quote
         (("gnu"          . "https://elpa.gnu.org/packages/")
          ("nongnu"       . "https://elpa.nongnu.org/nongnu/")
          ("melpa-stable" . "https://stable.melpa.org/packages/")
          ("melpa"        . "https://melpa.org/packages/")
          ;; [2021-03-31 Wed] org currently at 9.4.5, but...
          ;; this is about to go away after the release of org 9.5 org
          ;; will be distributed through GNU elpa and org-contrib will
          ;; be distributed through nongnu elpa (see above for both).
          ("org"    . "https://orgmode.org/elpa/"))))

;; priority for getting packages, higher numbers get priority over
;; lower numbers. the intention is to install versioned packages first
;; before eventually attempting to install from melpa which is based
;; on whatever the last commit in git is, and has a version number of
;; that timestamp.
(setq package-archive-priorities
      '(("gnu"          . 99)
        ("nongnu"       . 80)
        ("org"          . 75)           ; see comment above, this will
                                        ; be going away
        ("melpa-stable" . 70)
        ("melpa"        . 0)))

(add-to-list 'package-pinned-packages '('flycheck . "melpa"))

(if (file-exists-p package-user-dir)
    ;; don't activate all packages, instead they will be added via
    ;; mypackage-require, which will install if needed as well.
    (package-initialize t)
  ;; on a new system, need to activate everything initially
  (package-initialize)
  (package-refresh-contents))

;; The list of default packages I always want installed. 
(defvar mypackage-packages
  '(ack
    autoinsert
    avy
    counsel
    company
    company-quickhelp
    crux
    define-word
    diff-hl
    diminish
    dirtrack
    flyspell
    flyspell-correct
    flyspell-correct-ivy
    helpful
    ivy
    ivy-hydra
    ivy-rich
    key-chord
    magit
    move-text
    multiple-cursors
    operate-on-number
    popup
    smartrep
    swiper
    synosaurus
    try
    which-key
    whitespace
    windmove
    yasnippet
    yasnippet-snippets))

;; cloned from prelude, https://github.com/bbatsov/prelude/blob/master/core/prelude-packages.el
;; Author: Bozhidar Batsov <bozhidar@batsov.com>
;; Copyright © 2011-2021 Bozhidar Batsov, used with permission
(defun mypackage-require (package)
  "Install PACKAGE unless already installed"
  (unless (memq package mypackage-packages)
    (add-to-list 'mypackage-packages package))
  (unless (package-installed-p package)
    (package-install package)))

;; cloned from prelude, https://github.com/bbatsov/prelude/blob/master/core/prelude-packages.el
;; Author: Bozhidar Batsov <bozhidar@batsov.com>
;; Copyright © 2011-2021 Bozhidar Batsov, used with permission
(defun mypackage-require-packages (packages)
  "Ensure PACKAGES are installed.
Missing packages are installed automatically"
  (mapc #'mypackage-require packages))

;; cloned from prelude, https://github.com/bbatsov/prelude/blob/master/core/prelude-packages.el
;; Author: Bozhidar Batsov <bozhidar@batsov.com>
;; Copyright © 2011-2021 Bozhidar Batsov, used with permission
(defun mypackage-packages-installed-p ()
  "Check if all packages in `mypackage-packages' are installed."
  (cl-every #'package-installed-p mypackage-packages))

;; cloned from prelude, https://github.com/bbatsov/prelude/blob/master/core/prelude-packages.el
;; Author: Bozhidar Batsov <bozhidar@batsov.com>
;; Copyright © 2011-2021 Bozhidar Batsov, used with permission
(defun mypackage-install-packages ()
  "Install all packages listed in `mypackage-packages'."
  (unless (mypackage-packages-installed-p)
    ;; check for new packages (package versions)
    (message "%s" "Refreshing package database...")
    (package-refresh-contents)
    (message "%s" " done.")
    ;; install the missing packages
    (mypackage-require-packages mypackage-packages)))

;; ensure base packages are installed
(mypackage-install-packages)

(provide 'mypackage-config)
;;; mypackage-config.el ends here
