;;; MYUI-CONFIG --- Editor and UI configuration -*- lexical-binding: t; -*-
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 19 May 2021
;;
;;; Commentary:
;;
;; Editor and UI configuration
;;
;;; Code:

(require 'mypackage-config)

(mypackage-require-packages '(ample-theme diminish ibuffer-project modus-themes nano-theme whitespace))

(customize-set-variable 'modus-themes-syntax '(faint green-strings yellow-comments))
(customize-set-variable 'modus-themes-links '(faint background))

(modus-themes-load-themes)
(modus-themes-load-vivendi)

;;; whitespace
;; visualize whitespace in various places
(customize-set-variable 'whitespace-style
                        '(face tabs empty trailing tab-mark indentation::space))

;; what to do when visiting or saving a file
(customize-set-variable 'whitespace-action '(cleanup auto-cleanup))

;; turn on whitespace mode to get visualization and cleanup of
;; whitespace in buffers.
(global-whitespace-mode)


;; ibuffer configuration
(customize-set-variable 'ibuffer-movement-cycle nil)
(customize-set-variable 'ibuffer-old-time 24)
(add-hook 'ibuffer-hook
          (lambda ()
            (setq ibuffer-filter-groups (ibuffer-project-generate-filter-groups))
            (unless (eq ibuffer-sorting-mode 'project-file-relative)
              (ibuffer-do-sort-by-project-file-relative))))

;; manage the mode-line, cleanup lighters I'm not interested in.
(autoload 'diminish "diminish" nil t)
(with-eval-after-load "diminish"
  ;; (diminish 'company-mode)
  ;; (diminish 'counsel-mode)
  (diminish 'eldoc-mode)
  (diminish 'multiple-cursors-mode))


(provide 'myui-config)
;;; myui-config.el ends here
