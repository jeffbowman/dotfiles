;;; MYUI-CONFIG --- Editor and UI configuration
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 19 May 2021
;;
;;; Commentary:
;;
;; Editor and UI configuration
;;
;;; Code:

(require 'mypackage-config)

(mypackage-require-packages '(ample-theme whitespace diminish))

(load-theme 'ample t)
;; (custom-set-faces (backquote (default ((t (:family "Anonymous Pro" :height 160))))))

(autoload 'whitespace-mode "whitespace" nil t)
(with-eval-after-load "whitespace"
  (global-whitespace-mode))

(customize-set-variable 'whitespace-style
                        '(face tabs empty trailing lines-tail tab-mark))
(customize-set-variable 'whitespace-action '(auto-cleanup))

;; ibuffer configuration
(customize-set-variable 'ibuffer-movement-cycle nil)
(customize-set-variable 'ibuffer-old-time 24)

;; manage the mode-line, cleanup lighters I'm not interested in.
(autoload 'diminish "diminish" nil t)
(with-eval-after-load "diminish"
  (diminish 'company-mode)
  (diminish 'counsel-mode)
  (diminish 'eldoc-mode)
  (diminish 'ivy-mode)
  (diminish 'multiple-cursors-mode))


(provide 'myui-config)
;;; myui-config.el ends here
