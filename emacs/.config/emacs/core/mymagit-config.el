;;; Summary MYMAGIT-CONFIG --- magit configuration -*- lexical-binding: t; -*-
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 20 May 2021
;;
;;; Commentary:
;;
;; Magit configuration
;;
;;; Code:

(require 'mypackage-config)

(mypackage-require-packages '(magit diff-hl))

(autoload 'magit-status "magit-status" nil t)
(customize-set-variable 'magit-diff-refine-hunk 'all)
(define-key global-map (kbd "C-x g") #'magit-status)

;; From diff-hl.el
;; Provided commands:
;;
;; `diff-hl-diff-goto-hunk'  C-x v =
;; `diff-hl-revert-hunk'     C-x v n
;; `diff-hl-previous-hunk'   C-x v [
;; `diff-hl-next-hunk'       C-x v ]
;;
;; These are added to C-x v to match with normal vc-mode related keys.
(customize-set-variable 'diff-hl-command-prefix (kbd "C-x v"))
;; diff-hl is provided by Emacs, this is all that is needed to turn it
;; on.
(global-diff-hl-mode)
(add-hook 'magit-post-refresh-hook  #'diff-hl-magit-post-refresh)


(provide 'mymagit-config )
;;; mymagit-config.el ends here
