;;; MYCORE-CONFIG --- Core configuration  -*- lexical-binding: t; -*-
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 19 May 2021
;;
;;; Commentary:
;;
;; Core configuration, must be run after mypackage-configuration
;;
;;; Code:

;; Getting the path correct in MacOS is challenging. This bit of code
;; checks to make sure all the path pieces and parts are combined into
;; a single PATH environment variable without duplicates. The PATH
;; environment variable is used when Emacs runs in a shell, but uses
;; the `exec-path' for commands like spell checking etc. GUI
;; applications do not inherit the shell environment, but do read
;; system-wide variables from the $HOME/.MacOSX/environment.plist
;; file. So, to get what we want, combine both the shell environment
;; and the GUI environment variables into a single PATH environment
;; variable and also set that result to the `exec-path'.
(if (string-equal "darwin" (symbol-name system-type))
    (progn
      (setenv "PATH"
              (let ((p1 (split-string (getenv "PATH") path-separator))
                    (p2 (split-string (shell-command-to-string
                                       "/usr/bin/defaults read $HOME/.MacOSX/environment PATH")
                                      path-separator))
                    (p3 (split-string (shell-command-to-string "$SHELL --login -c 'echo $PATH'")
                                      path-separator)))
                (string-join (delete-dups
                              (mapcar #'(lambda (s) (string-trim (replace-regexp-in-string "\n" "" s)))
                                      (append p1 p2 p3)))
                             path-separator)))
      (setq exec-path (split-string (getenv "PATH") path-separator)))
  (setq exec-path (split-string (getenv "PATH") path-separator)))

;;
;; Profiling code, should be commented out when not in use.
;;
;; (package-install 'benchmark-init)
;; (require 'benchmark-init)
;; (add-hook 'after-init-hook 'benchmark-init/deactivate)

;; move customization to a file outside of version control to avoid
;; version conflicts, but allows emacs customization (like the list of
;; packages) to be updated during a session. It is loaded here because
;; other workflows like `org-agenda-file-to-front', `org-remove-file',
;; and usage of `.dir-locals.el' will sometimes write to this file, so
;; we need to load it to get those updates.
(customize-set-variable 'custom-file
                        (expand-file-name "custom.el" user-emacs-directory))
(when (file-readable-p custom-file)
  (load custom-file t))

;; global customization
(require 'myglobal-config)

(run-with-idle-timer 4 nil #'require 'recentf nil t)
(with-eval-after-load 'recentf
  (recentf-mode))

;; (autoload 'company-mode "company" nil t)
;; (require 'company)
;; (with-eval-after-load "company"
;;   (define-key company-mode-map (kbd "M-/")   #'company-complete)
;;   (define-key company-active-map (kbd "M-/") #'company-select-next)
;;   (define-key company-active-map (kbd "C-n") #'company-select-next)
;;   (define-key company-active-map (kbd "C-p") #'company-select-previous)
;;   (global-company-mode))

;; corfu replaces company above, closer to core Emacs, uses built-in
;; CAPF for completions, activated on TAB key.
(customize-set-variable 'completion-cycle-threshold 3)
(customize-set-variable 'tab-always-indent 'complete)
(customize-set-variable 'corfu-cycle t)
(customize-set-variable 'corfu-auto nil)
(customize-set-variable 'corfu-auto-prefix 2)
(customize-set-variable 'corfu-echo-documentation 0.25)
(global-corfu-mode)
;;; corfu
(define-key global-map (kbd "M-/")       #'dabbrev-expand)

;;; corfu-doc
(add-hook 'corfu-mode-hook #'corfu-doc-mode)
(define-key corfu-map  (kbd "M-p") #'corfu-doc-scroll-down)
(define-key corfu-map  (kbd "M-n") #'corfu-doc-scroll-up)
(define-key corfu-map  (kbd "M-d") #'corfu-doc-toggle)

;;; cape
(add-to-list 'completion-at-point-functions #'cape-file)
(add-to-list 'completion-at-point-functions #'cape-dabbrev)
(define-key global-map (kbd "C-c c p") #'completion-at-point)
(define-key global-map (kbd "C-c c l") #'cape-line)

(require 'orderless)
(customize-set-variable 'completion-styles
                        '(orderless partial-completion flex))

(require 'selectrum)
(require 'selectrum-prescient)
(customize-set-variable 'selectrum-highlight-candidates-function
                        #'orderless-highlight-matches)
(customize-set-variable 'orderless-skip-highlighting (lambda () selectrum-is-active))
(selectrum-mode +1)
;; FIXME: Should be customize-set-variable when available, does not
;; exist in v5.1 of selectrum-prescient.
(setq selectrum-prescient-enable-filtering nil)
(selectrum-prescient-mode +1)
(prescient-persist-mode +1)

(require 'consult)
(define-key global-map (kbd "C-s")       #'consult-line)
(define-key global-map (kbd "C-x b")     #'consult-buffer)
(define-key global-map (kbd "C-x B")     #'consult-buffer-other-window)
(define-key global-map [remap yank]      #'consult-yank-from-kill-ring)
(define-key global-map [remap yank-pop]  #'consult-yank-pop)
(define-key global-map [remap goto-line] #'consult-goto-line)

(require 'marginalia)
(marginalia-mode)

(require 'helpful)
(global-set-key [remap describe-function] #'helpful-function)
(global-set-key [remap describe-command]  #'helpful-command)
(global-set-key [remap describe-variable] #'helpful-variable)
(global-set-key [remap describe-key]      #'helpful-key)

;; (require 'company-quickhelp)
;; (customize-set-variable 'company-quickhelp-delay 1)
;; (company-quickhelp-mode 1)

;; N.B. Some of these keybindings only seem to work in a gui. Some
;; alternatives are provided.
(require 'multiple-cursors)
(with-eval-after-load "multiple-cursors"
  (define-key global-map (kbd "C-S-c C-S-c") #'mc/edit-lines)
  (define-key global-map (kbd "C-c M-e")     #'mc/edit-lines)
  (define-key global-map (kbd "C-S-c <")     #'mc/mark-sgml-tag-pair)
  (define-key global-map (kbd "C-c M-<")     #'mc/mark-sgml-tag-pair)
  (define-key global-map (kbd "C-S-c #")     #'mc/insert-numbers)
  (define-key global-map (kbd "C-c M-#")     #'mc/insert-numbers)
  (define-key global-map (kbd "C->")         #'mc/mark-next-like-this)
  (define-key global-map (kbd "C-c >")       #'mc/mark-next-like-this)
  (define-key global-map (kbd "C-c C->")     #'mc/skip-to-next-like-this)
  (define-key global-map (kbd "C-c . .")     #'mc/skip-to-next-like-this)
  (define-key global-map (kbd "C-<")         #'mc/mark-previous-like-this)
  (define-key global-map (kbd "C-c <")       #'mc/mark-previous-like-this)
  (define-key global-map (kbd "C-c C-<")     #'mc/skip-to-previous-like-this)
  (define-key global-map (kbd "C-c ,")       #'mc/skip-to-previous-like-this)
  (define-key global-map (kbd "C-S-c C-<")   #'mc/mark-all-like-this))

(require 'smartrep)
(with-eval-after-load "smartrep"
  (require 'operate-on-number)
  (smartrep-define-key global-map "C-c ."
    '(("+" . apply-operation-to-number-at-point)
      ("-" . apply-operation-to-number-at-point)
      ("*" . apply-operation-to-number-at-point)
      ("/" . apply-operation-to-number-at-point)
      ("\\" . apply-operation-to-number-at-point)
      ("^" . apply-operation-to-number-at-point)
      ("<" . apply-operation-to-number-at-point)
      (">" . apply-operation-to-number-at-point)
      ("#" . apply-operation-to-number-at-point)
      ("%" . apply-operation-to-number-at-point)
      ("'" . operate-on-number-at-point))))

(require 'avy)
(require 'crux)

(with-eval-after-load "avy"
  ;; Adds C-' to isearch mode (F6), provides nifty additional
  ;; functionality ala embark and others
  (avy-setup-default)
  (with-eval-after-load "crux"
    (require 'key-chord)
    (key-chord-define-global "ji" #'avy-goto-char-in-line)
    (key-chord-define-global "jj" #'avy-goto-word-1)
    (key-chord-define-global "jl" #'avy-goto-line)
    (key-chord-define-global "jk" #'avy-goto-char)
    (key-chord-define-global "JJ" #'crux-switch-to-previous-buffer)
    (key-chord-define-global "xx" #'execute-extended-command)
    (key-chord-mode +1)

    (define-key global-map (kbd "C-a")   #'crux-move-beginning-of-line)
    (define-key global-map (kbd "C-c d") #'crux-duplicate-current-line-or-region)
    (define-key global-map (kbd "C-c f") #'crux-recentf-find-file)
    (define-key global-map (kbd "C-c k") #'crux-kill-other-buffers)
    (define-key global-map (kbd "C-c n") #'crux-cleanup-buffer-or-region)))

(autoload 'move-text-up "move-text" nil t)
(autoload 'move-text-down "move-text" nil t)
(define-key global-map (kbd "<C-S-up>")   #'move-text-up)
(define-key global-map (kbd "<C-S-down>") #'move-text-down)

;; conflicts with org-shift{left,right,up,down}
;; (require 'windmove)
;; (if (string-equal "windows-nt" (symbol-name system-type))
;;     ;; default is "<shift>" however on Microsoft Windows, have to
;;     ;; specify the modifier here or it doesn't work.
;;     (windmove-default-keybindings "<shift>")
;;   (windmove-default-keybindings))

(defcustom my-windows-prefix-key "C-c w"
  "Configure the prefix key for `my-windows' bindings.")

(define-prefix-command 'my-windows-key-map)

(define-key 'my-windows-key-map (kbd "n") 'windmove-down)
(define-key 'my-windows-key-map (kbd "p") 'windmove-up)
(define-key 'my-windows-key-map (kbd "b") 'windmove-left)
(define-key 'my-windows-key-map (kbd "f") 'windmove-right)

(global-set-key (kbd my-windows-prefix-key) 'my-windows-key-map)

(autoload 'which-key-mode "which-key" nil t)
(customize-set-variable 'which-key-show-early-on-C-h t)
(customize-set-variable 'which-key-idle-delay 1.0)
(customize-set-variable 'which-key-idle-secondary-delay 0.05)
(which-key-mode)
(with-eval-after-load "which-key"
  (which-key-setup-side-window-right-bottom))

;; yasnippet and autoinsert
(require 'mysnippet-config)

(autoload 'flyspell-mode "flyspell" nil t)
(with-eval-after-load "flyspell"
  (require 'flyspell-correct)
  (define-key flyspell-mode-map (kbd "M-$") #'flyspell-correct-at-point)
  (define-key flyspell-mode-map (kbd "C-;") #'flyspell-correct-wrapper))
(add-hook 'text-mode #'flyspell-mode)
(add-hook 'latex-mode #'flyspell-mode)
(add-hook 'prog-mode #'flyspell-prog-mode)

(autoload 'synosaurus-mode "synosaurus")
(with-eval-after-load "synosaurus"
  (require 'popup)
  (customize-set-variable 'synosaurus-choose-method 'popup))

(autoload 'define-word-at-point "define-word" nil t)
(define-key global-map (kbd "s-\\") #'define-word-at-point)

;; Turn on global auto revert so buffers attached to files are updated
;; to reflect current contents if they are modified outside of Emacs
(customize-set-variable 'global-auto-revert-mode-text " AR ")
(global-auto-revert-mode)

(require 'dirtrack)
(dirtrack-mode)

;; magit
(require 'mymagit-config)

;; org
(require 'myorg-config)

;;;
;;; gpg config, uses pinentry from the minibuffer (or it should!)
;;;
(customize-set-variable 'epg-pinentry-mode 'loopback)
(pinentry-start)

;;;
;;; profiling
;;;
;; from https://blog.d46.us/advanced-emacs-startup/
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections"
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)

            ;; reset to ootb default configuration for garbage
            ;; collection
            (setq gc-cons-threshold 800000)))

(provide 'mycore-config)
;;; mycore-config.el ends here
