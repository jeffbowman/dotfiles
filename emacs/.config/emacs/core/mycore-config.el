;;; MYCORE-CONFIG --- Core configuration
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 19 May 2021
;;
;;; Commentary:
;;
;; Core configuration, must be run after mypackage-configuration
;;
;;; Code:

(if (string-equal "darwin" (symbol-name system-type))
    (let ((full-path (shell-command-to-string
                      "/usr/bin/defaults read $HOME/.MacOSX/environment PATH")))
      (setenv "PATH" full-path)
      ;; (setq exec-path (split-string full-path path-separator))
      ))

;;
;; Profiling code, should be commented out when not in use.
;;
;; (package-install 'benchmark-init)
;; (require 'benchmark-init)
;; (add-hook 'after-init-hook 'benchmark-init/deactivate)

;; move customization to a file outside of version control to avoid
;; version conflicts, but allows emacs customization (like the list of
;; packages) to be updated during a session, however, it is not loaded
;; by default. this is to avoid slower load times and duplication of
;; effort in reading settings which are managed in use-package
;; configurations.
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))


;; global customization
(require 'myglobal-config)

(run-with-idle-timer 4 nil #'require 'recentf nil t)
(with-eval-after-load 'recentf
  (recentf-mode))

(autoload 'company-mode "company" nil t)
(with-eval-after-load "company"
  (define-key company-mode-map (kbd "M-/")   #'company-complete)
  (define-key company-active-map (kbd "M-/") #'company-select-next)
  (define-key company-active-map (kbd "C-n") #'company-select-next)
  (define-key company-active-map (kbd "C-p") #'company-select-previous)
  (global-company-mode))

(require 'counsel)
(with-eval-after-load "counsel"


  (customize-set-variable 'counsel-ag-base-command
                          "ag --vimgrep --hidden %s")

  ;; counsel keys
  (define-key global-map (kbd "M-x")     #'counsel-M-x)
  (define-key global-map (kbd "C-x C-f") #'counsel-find-file)
  (define-key global-map (kbd "<f1> a")  #'counsel-ack)
  (define-key global-map (kbd "<f1> f")  #'counsel-describe-function)
  (define-key global-map (kbd "<f1> v")  #'counsel-describe-variable)
  (define-key global-map (kbd "<f1> l")  #'counsel-find-library)
  (define-key global-map (kbd "<f2> i")  #'counsel-info-lookup-symbol)
  (define-key global-map (kbd "<f2> u")  #'counsel-unicode-char)

  (require 'helpful)
  (require 'company-quickhelp)

  (customize-set-variable 'counsel-describe-function-function #'helpful-callable)
  (customize-set-variable 'counsel-describe-variable-function #'helpful-variable)

  (global-set-key [remap describe-function] #'counsel-descbinds-function)
  (global-set-key [remap describe-command]  #'helpful-command)
  (global-set-key [remap describe-variable] #'counsel-describe-variable)
  (global-set-key [remap describe-key]      #'helpful-key)

  (customize-set-variable 'company-quickhelp-delay 1)
  (company-quickhelp-mode 1))


(require 'ivy)
(with-eval-after-load "ivy"

  (require 'ivy-hydra)

  (define-key global-map (kbd "C-c C-r") #'ivy-resume)
  (define-key global-map (kbd "C-x b")   #'ivy-switch-buffer)
  (define-key global-map (kbd "C-x B")   #'ivy-switch-buffer-other-window)

  (customize-set-variable 'ivy-count-format "(%d/%d) ")
  (customize-set-variable 'ivy-display-style 'fancy)
  (customize-set-variable 'ivy-use-virtual-buffers t)
    ;; suggested on
  ;; https://github.com/abo-abo/swiper/issues/2694#issuecomment-708418422
  ;; which seems to work for me.
  (customize-set-variable 'ivy-read-action-function 'ivy-hydra-read-action)
  (ivy-mode)

  (require 'ivy-rich)
  (customize-set-variable 'ivy-virtual-abbreviate 'full)
  (customize-set-variable 'ivy-rich-switch-buffer-align-virtual-buffer t)
  (customize-set-variable 'ivy-rich-path-style 'abbrev)

  (require 'swiper)
  (define-key global-map (kbd "C-s") #'swiper)
  (customize-set-variable 'swiper-include-line-number-in-search t))

;; N.B. These keybindings only seem to work in a gui.
(when (display-graphic-p)
  (autoload 'mc/edit-lines                 "mc-edit-lines"          nil t)
  (autoload 'mc/mark-sgml-tag-pair         "mc-mark-more"           nil t)
  (autoload 'mc/insert-numbers             "mc-separate-operations" nil t)
  (autoload 'mc/mark-next-like-this        "mc-mark-more"           nil t)
  (autoload 'mc/skip-to-next-like-this     "mc-mark-more"           nil t)
  (autoload 'mc/mark-previous-like-this    "mc-mark-more"           nil t)
  (autoload 'mc/skip-to-previous-like-this "mc-mark-more"           nil t)
  (autoload 'mc/mark-all-like-this         "mc-mark-more"           nil t)
  ;; eval after load mc-mark-more because this is the highest
  ;; percentage likely case, the embedded require will gather the
  ;; rest.
  (require 'multiple-cursors)
  (with-eval-after-load "mc-mark-more"
    (define-key global-map (kbd "C-S-c C-S-c") #'mc/edit-lines)
    (define-key global-map (kbd "C-S-c <")     #'mc/mark-sgml-tag-pair)
    (define-key global-map (kbd "C-S-c #")     #'mc/insert-numbers)
    (define-key global-map (kbd "C->")         #'mc/mark-next-like-this)
    (define-key global-map (kbd "C-c C->")     #'mc/skip-to-next-like-this)
    (define-key global-map (kbd "C-<")         #'mc/mark-previous-like-this)
    (define-key global-map (kbd "C-c C-<")     #'mc/skip-to-previous-like-this)
    (define-key global-map (kbd "C-S-c C-<")   #'mc/mark-all-like-this)))

(require 'smartrep)
(with-eval-after-load "smartrep"
  (require 'operate-on-number)
  (smartrep-define-key global-map "C-c ."
    '(("+" . apply-operation-to-number-at-point)
      ("-" . apply-operation-to-number-at-point)
      ("*" . apply-operation-to-number-at-point)
      ("/" . apply-operation-to-number-at-point)
      ("\\" . apply-operation-to-number-at-point)
      ("^" . apply-operation-to-number-at-point)
      ("<" . apply-operation-to-number-at-point)
      (">" . apply-operation-to-number-at-point)
      ("#" . apply-operation-to-number-at-point)
      ("%" . apply-operation-to-number-at-point)
      ("'" . operate-on-number-at-point))))

(require 'avy)
(require 'crux)

(with-eval-after-load "avy"
  (with-eval-after-load "crux"
    (require 'key-chord)
    (key-chord-define-global "ji" #'avy-goto-char-in-line)
    (key-chord-define-global "jj" #'avy-goto-word-1)
    (key-chord-define-global "jl" #'avy-goto-line)
    (key-chord-define-global "jk" #'avy-goto-char)
    (key-chord-define-global "JJ" #'crux-switch-to-previous-buffer)
    (key-chord-define-global "xx" #'execute-extended-command)
    (key-chord-mode +1)

    (define-key global-map (kbd "C-a")   #'crux-move-beginning-of-line)
    (define-key global-map (kbd "C-c d") #'crux-duplicate-current-line-or-region)
    (define-key global-map (kbd "C-c f") #'crux-recentf-find-file)
    (define-key global-map (kbd "C-c k") #'crux-kill-other-buffers)
    (define-key global-map (kbd "C-c n") #'crux-cleanup-buffer-or-region)))

(autoload 'move-text-up "move-text" nil t)
(autoload 'move-text-down "move-text" nil t)
(define-key global-map (kbd "<C-S-up>")   #'move-text-up)
(define-key global-map (kbd "<C-S-down>") #'move-text-down)

(require 'windmove)
(if (string-equal "windows-nt" (symbol-name system-type))
    ;; default is "<shift>" however on Microsoft Windows, have to
    ;; specify the modifier here or it doesn't work.
    (windmove-default-keybindings "<shift>")
  (windmove-default-keybindings))

(autoload 'which-key-mode "which-key" nil t)
(customize-set-variable 'which-key-show-early-on-C-h t)
(customize-set-variable 'which-key-idle-delay 1.0)
(customize-set-variable 'which-key-idle-secondary-delay 0.05)
(which-key-mode)
(with-eval-after-load "which-key"
  (which-key-setup-side-window-right-bottom))
;; (which-key-mode)

;; yasnippet and autoinsert
(require 'mysnippet-config)

(autoload 'flyspell-mode "flyspell" nil t)
(with-eval-after-load "flyspell"
  (require 'flyspell-correct)
  (require 'flyspell-correct-ivy)
  (define-key flyspell-mode-map (kbd "M-$") #'flyspell-correct-at-point)
  (define-key flyspell-mode-map (kbd "C-;") #'flyspell-correct-wrapper))
(add-hook 'text-mode #'flyspell-mode)
(add-hook 'prog-mode #'flyspell-prog-mode)

(autoload 'synosaurus-mode "synosaurus")
(with-eval-after-load "synosaurus"
  (require 'popup)
  (customize-set-variable 'synosaurus-choose-method 'popup))

(autoload 'define-word-at-point "define-word" nil t)
(define-key global-map (kbd "s-\\") #'define-word-at-point)

(require 'dirtrack)
(dirtrack-mode)

;; magit
(require 'mymagit-config)

;; org
(require 'myorg-config)

;;;
;;; profiling
;;;
;; from https://blog.d46.us/advanced-emacs-startup/
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections"
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)

            ;; reset to ootb default configuration for garbage
            ;; collection
            (setq gc-cons-threshold 800000)))

(provide 'mycore-config)
;;; mycore-config.el ends here
