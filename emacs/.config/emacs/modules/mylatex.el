;;; MYLATEX --- LaTeX configuration
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 18 August 2021
;;
;;; Commentary:
;;
;; Hooks to turn on when editing LaTeX files. Also, requires MELPA
;; version of flycheck.
;;
;;; Code:

(mypackage-require-packages '(flycheck-grammarly
                              lsp-grammarly))

(defun my-latex-mode ()
  (require 'flycheck-grammarly)
  (require 'lsp-grammarly)
  (flyspell-mode-on)
  (flycheck-mode)
  (lsp))

(add-hook 'latex-mode-hook #'my-latex-mode)

(provide 'mylatex)
;;; mylatex.el ends here
