;;; Summary --- List/Scheme/Clojure configuration
;;; Commentary:
;;; Code:

(require 'mypackage-config)
(require 'mymodule-config)
(mypackage-require-packages '(cider
                              clj-refactor
                              clojure-mode
                              clojure-snippets
                              eldoc
                              flycheck-clojure
                              geiser
                              geiser-gauche
                              sly
                              sly-asdf
                              sly-quicklisp
                              sly-repl-ansi-color
                              smartparens))

(mymodule-activate "myprogmode.el")
(require 'eldoc)

;; use Chez scheme (others would be Chicken, Bigloo, Gauche(Gosh))
(setq scheme-program-name "scheme")

(require 'smartparens)
(add-hook 'lisp-mode-hook       #'turn-on-smartparens-mode)
(add-hook 'scheme-mode-hook     #'turn-on-smartparens-mode)
(add-hook 'emacs-lisp-mode-hook #'turn-on-smartparens-mode)
(add-hook 'clojure-mode-hook    #'turn-on-smartparens-mode)

(customize-set-variable 'sp-base-key-bindings 'paredit)
(customize-set-variable 'sp-autoskip-closing-pair 'always)
(customize-set-variable 'sp-hybrid-kill-entire-symbol nil)

(sp-use-paredit-bindings)
(with-eval-after-load "smartparens"
  (require 'smartparens-config)
  (show-smartparens-global-mode))

(with-eval-after-load "clojure-mode"
  (require 'cider)
  (require 'clojure-snippets)
  (yas-minor-mode-on)
  (aggressive-indent-mode)

  (require 'clj-refactor)
  (add-hook 'clojure-mode-hook (lambda ()
                                 (clj-refactor-mode 1)
                                 (cljr-add-keybindings-with-prefix "C-c m")))

  (with-eval-after-load "flycheck"
    (require 'flycheck-clojure)
    (flycheck-clojure-setup)))

(with-eval-after-load 'geiser-impl
  (add-to-list 'geiser-active-implementations 'gauche))

;; for common lisp
;; SLY replaces SLIME
(unless (fboundp 'sly-editing-mode)
  (autoload #'sly-editing-mode "sly" nil t))

(with-eval-after-load 'sly
  (setq inferior-lisp-program "/usr/bin/sbcl")
  (require 'sly-quicklisp)
  (require 'sly-repl-ansi-color)
  (require 'sly-asdf))

(add-hook 'lisp-mode-hook #'sly-editing-mode)

(provide 'mylisp)

;;; mylisp.el ends here
