;;; Summary --- Generic programming mode configuration
;;; Commentary:
;;;   Add as a require on other prog modes
;;; Code:

(require 'mypackage-config)
(mypackage-require-packages '(avy-flycheck
                              editorconfig
                              eglot
                              flycheck
                              flycheck-tip
                              highlight-indent-guides
                              hideshow))

(autoload 'flycheck-mode "flycheck" nil t)
(with-eval-after-load "flycheck"
  (require 'flycheck-tip)
  (require 'avy-flycheck))

(autoload 'eglot "eglot")

(require 'editorconfig)
(diminish 'editorconfig-mode "EC")
(editorconfig-mode)

(autoload 'highlight-indent-guides-mode "highlight-indent-guides")
(with-eval-after-load "highlight-indent-guides"
  (diminish 'highlight-indent-guides-mode)
  (customize-set-variable 'highlight-indent-guides-method 'character)
  (customize-set-variable 'highlight-indent-guides-character ?\|))

(require 'hideshow)
(add-to-list 'hs-special-modes-alist
             '(nxml-mode
                 "<!--\\|<[^/>]*[^/]>"
                 "-->\\|</[^/>]*[^/]>"

                 "<!--"
                 sgml-skip-tag-forward
                 nil))

;; needed?? see myjson.el
;; (add-hook 'javascript-mode-hook #'hs-minor-mode)

;; hooks for built-in modes
(add-hook 'nxml-mode-hook #'hs-minor-mode)
(add-hook 'prog-mode-hook #'hs-minor-mode)
(add-hook 'prog-mode-hook #'highlight-indent-guides-mode)

(provide 'myprogmode)

;;; myprogmode.el ends here
