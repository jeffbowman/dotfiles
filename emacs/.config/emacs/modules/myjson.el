;;; Summary --- JSON configuration
;;; Commentary:
;;; Code:

(require 'mypackage-config)
(require 'mymodule-config)
(mypackage-require-packages '(js2-mode))

(require 'eldoc)
(mymodule-activate "myprogmode.el")

(defun my-js2-mode-setup ()
  (add-hook 'xref-backend-functions #'xref-js2-xref-backend nil t)
  (js2-imenu-extras-mode nil)
  (flycheck-mode t)
  (when (executable-find "eslint")
    (flycheck-select-checker 'javascript-eslint)))

(unless (fboundp 'js2-mode)
  (autoload #'js2-mode "js2-mode" nil t)
  (with-eval-after-load "js2-mode"
    (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))))

(add-hook 'js2-mode-hook #'my-js2-mode-setup)
;; needed? added to prog-mode-hook, so may be redundant here
(add-hook 'js2-mode-hook #'hs-minor-mode)

(provide 'myjson)

;;; myjson.el ends here
