;;; Summary --- Lua configuration
;;; Commentary:
;;; Code:

(require 'mypackage-config)
(require 'mymodule-config)
(mypackage-require-packages '(lua-mode))

(require 'eldoc)
(mymodule-activate "myprogmode.el")

(autoload 'lua-mode "lua-mode" nil t)

(provide 'mylua)

;;; myjson.el ends here
