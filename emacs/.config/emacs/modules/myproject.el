;;; Summary --- Project mode configuration
;;; Commentary:
;;;     Project related packages, projectile and emacs code browser (ecb)
;;;
;;; Code:

(require 'mypackage-config)
(mypackage-require-packages '(ag
                              counsel-projectile
                              noccur
                              org-projectile
                              projectile))
(autoload 'ag "ag" nil t)
(autoload 'noccur-project "noccur" nil t)

(autoload 'projectile-mode "projectile" nil t)

(defun maybe-load-projectile ()
  (interactive)
  (unless (featurep 'projectile)
    ;; should get rebound when projectile loads
    (global-unset-key (kbd "C-c p"))
    (projectile-mode)
    (message "Projectile loaded, press C-c p prefix again")))

(define-key global-map (kbd "C-c p") #'maybe-load-projectile)

(with-eval-after-load "projectile"
  (require 'org-projectile)
  (require 'counsel-projectile)

  (customize-set-variable 'projectile-mode-line-prefix " Prj")
  (customize-set-variable 'projectile-completion-system 'ivy)
  (customize-set-variable 'projectile-sort-order 'modification-time)
  (customize-set-variable 'projectile-switch-project-action 'projectile-dired)

  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

  (counsel-projectile-mode))

;; (require 'sr-speedbar)
;; (define-key global-map (kbd "C-c s") #'sr-speedbar-toggle)
;; (customize-set-variable 'speedbar-show-unknown-files t)
;; (customize-set-variable 'sr-speedbar-right-side nil)

(provide 'myproject)

;;; myproject.el ends here
