;;; MYBLOG --- Configuration for writing blog posts
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 25 April 2021
;;
;;; Commentary:
;;
;; Based on a comment from here:
;; https://www.reddit.com/r/emacs/comments/mxdite/blog_hosting_advice_request/gvnusrb?utm_source=share&utm_medium=web2x&context=3
;; I chose to use WriteFreely (https://write.as).
;;
;;; Code:

(require 'mypackage-config)
(mypackage-require-packages '(writefreely))

(require 'myorg-config)

;; This is called in the snippet to create a new blog entry.
(defun load-write-freely ()
  "Loads writefreely. Use similar to a hook function"
  (interactive)
  (unless (featurep 'writefreely)
    (package-activate 'writefreely))
  (autoload #'writefreely-mode "writefreely" nil t))

;; Once writefreely is loaded, get the auth-token so blogs published
;; will show up correctly.
(with-eval-after-load "writefreely"
  (require 'auth-source)
  (setq writefreely-auth-token
        (auth-source-pick-first-password :host "write.as")))

(provide 'myblog)
;;; myblog.el ends here
