;;; MYREDDIT --- reddit configuration using md4rd
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 24 May 2021
;;
;;; Commentary:
;;
;; md4rd aka "mode for reddit" allows reading reddit in emacs
;;
;;; Code:

(require 'mypackage-config)

(mypackage-require-packages '(md4rd))

(defvar token "bK1pwEgVP2CPkdQL8LRqTynBU3GP7Q")

(autoload 'md4rd "md4rd" nil t)
(with-eval-after-load "md4rd"
  (setq md4rd-subs-active '(emacs planetemacs))
  (setq md4rd--oauth-access-token token))

(add-hook 'md4rd-mode-hook 'md4rd-indent-all-the-lines)

;; (setq md4rd--oauth-refresh-token "your-refresh-token-here")
;; (run-with-timer 0 3540 'md4rd-refresh-login)

(provide 'myreddit)
;;; myreddit.el ends here
