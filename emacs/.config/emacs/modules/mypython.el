;;; Summary --- Python configuration
;;;
;;; Commentary:
;;;   Configuration for Python development
;;;
;;; Code:

(require 'mypackage-config)
(require 'mymodule-config)
(mypackage-require-packages '(python-mode elpy company-jedi))

(require 'eldoc)
(mymodule-activate "myprogmode.el")

(autoload 'python-mode "python-mode" nil t)
(add-hook 'python-mode-hook #'company-mode)

(with-eval-after-load "python-mode"
  (require 'elpy)
  ;; from https://github.com/jorgenschaefer/elpy
  ;; non deferred example
  ;; (elpy-enable)

  ;; https://elpy.readthedocs.io/en/latest/introduction.html
  ;; use when want to defer Elpy loading (which I do).
  (advice-add 'python-mode :before 'elpy-enable))

;; taken from https://linuxhint.com/configuring_emacs_python/

;;
;; standard interpreter
;; ====================
;; (setq python-shell-interpreter "python"
;;      python-shell-interpreter-args "-i")

;; jupyter
;; =======
;; (setq python-shell-interpreter "jupyter"
;;      python-shell-interpreter-args "console --simple-prompt"
;;      pythong-shell-prompt-detect-failure-warning nil)
;; (add-to-list 'python-shell-completion-native-disabled-interpreters "jupyter")

;; iPython
;; =======
(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "-i --simple-prompt")

;; per the documentation, don't use this when using company mode,
;; prefer company-jedi as below
;; (use-package jedi
;;   :hook python-mode
;;   :config (jedi:init-server))

(with-eval-after-load "python-mode"
  (require 'company-jedi))

(add-hook 'python-mode #'company-jedi-mode)
(add-to-list 'company-backends 'company-jedi)

;; (use-package pipenv
;;   :hook python-mode
;;   :init
;;   (setq
;;    pipenv-projectile-after-switch-function
;;    #'pipenv-projectile-after-switch-extended))

;; does this conflict with pipenv above??
;; this redundant with elpy
;; (use-package auto-virtualenv
;; :hook ((python-mode projectile-after-switch-project) . auto-virtualenv-set-virtualenv)) ;

(provide 'mypython)

;;; mypython.el ends here
