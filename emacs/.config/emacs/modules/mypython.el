;;; Summary --- Python configuration      -*- lexical-binding: t; -*-
;;;
;;; Commentary:

;; Python development environment configuration.  Several python
;; packages can be installed with `pip' which can be used here as well:

;; * autopep8      -- automatically formats python code to conform to
;;                    PEP 8 style guide
;; * black         -- uncompromising code formatter
;; * flake8        -- style guide enforcement
;; * importmagic   -- automatically add, remove, manage imports
;; * ipython       -- interactive python shell
;; * jedi          -- static analysis tool typically used in IDEs,
;;                    provides autocomplete and navigation
;; * yapf          -- formatter for python code

;; Emacs packages to support python development:
;; * anaconda      -- code navigation, documentation and completion
;; * blacken       -- buffer formatting on save using black
;; * eglot         -- language server integration
;;                    (need to pip install pyright)
;; * numpydoc      -- python doc templates, uses `yasnippets'
;; * pythonic      -- utility packages for running python in different
;;                    environments (dependency of anaconda)
;; * pyvenv        -- virtualenv wrapper

;;; Code:

(require 'mypackage-config)
(require 'mymodule-config)
(mypackage-require-packages '(anaconda-mode blacken numpydoc pyvenv))

(require 'eldoc)
;; for eglot
(mymodule-activate "myprogmode.el")

;; hooks
(add-hook 'python-mode-hook #'anaconda-mode)
(add-hook 'python-mode-hook #'blacken-mode)
(add-hook 'python-mode-hook #'eldoc-mode)
(add-hook 'python-mode-hook #'pyvenv-mode)
(add-hook 'python-mode-hook #'pyvenv-tracking-mode)
(add-hook 'pyvenv-post-activate-hooks #'pyvenv-restart-python)

;; customizations
(customize-set-variable 'numpydoc-insert-examples-block nil)
(customize-set-variable 'numpydoc-template-long nil)
(customize-set-variable 'pyvenv-default-virtual-env-name "venv")
(customize-set-variable 'python-indent-guess-indent-offset-verbose nil)

(with-eval-after-load "python"
  (define-key python-mode-map (kbd "C-c C-n") #'numpydoc-generate)
  (define-key python-mode-map (kbd "C-c e n") #'flymake-goto-next-error)
  (define-key python-mode-map (kbd "C-c e p") #'flymake-goto-prev-error))

(with-eval-after-load "pyvenv"
  (define-key pyvenv-mode-map (kbd "C-c p a") #'pyvenv-activate)
  (define-key pyvenv-mode-map (kbd "C-c p d") #'pyvenv-deactivate)
  (define-key pyvenv-mode-map (kbd "C-c p w") #'pyvenv-workon))

;; taken from https://linuxhint.com/configuring_emacs_python/
;;
;; standard interpreter (default)
;; ====================
;; (customize-set-variable 'python-shell-interpreter "python")
;; (customize-set-variable 'python-shell-interpreter-args "-i")

;; jupyter
;; =======
;; (customize-set-variable 'python-shell-interpreter "jupyter")
;; (customize-set-variable 'python-shell-interpreter-args "console --simple-prompt")
;; (customize-set-variable 'python-shell-prompt-detect-failure-warning nil)
;; (add-to-list 'python-shell-completion-native-disabled-interpreters "jupyter")

;; iPython
;; =======
;; (customize-set-variable 'python-shell-interpreter "ipython")
;; (customize-set-variable 'python-shell-interpreter-args "-i --simple-prompt")

(provide 'mypython)

;;; mypython.el ends here
