;;; MYDIAGRAM --- Modes to handle diagrams
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 28 June 2019
;;
;;; Commentary:
;;
;;    PlantUML and WebSequenceDiagrams provide useful syntax and
;;    generators for different kinds of diagrams.
;;
;;    WebSequenceDiagrams provides simple syntax and uses a web
;;    service to connect to https://www.websequencediagrams.com/ which
;;    will generate a png file.
;;
;;    PlantUML provides a similar syntax, but allows more kinds of
;;    diagrams than wsd. A jar file is needed from
;;    http://plantuml.com/download which should be placed in the home
;;    directory or customized to be elsewhere.
;;
;;; Code:

(require 'mypackage-config)
(mypackage-require-packages '(flycheck-plantuml
                              plantuml-mode
                              wsd-mode))

(autoload 'plantuml-mode "plantuml-mode")
(add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode))

(with-eval-after-load "plantuml-mode"
  (require 'flycheck-plantuml)
  (flycheck-plantuml-setup))

(autoload 'wsd-mode "wsd-mode")
(add-hook 'wsd-mode-hook 'company-mode)

(provide 'mydiagram)
;;; mydiagram.el ends here
