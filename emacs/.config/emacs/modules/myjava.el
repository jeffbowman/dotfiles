;;; MYJAVA --- java mode configuration
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created:  6 July 2018
;;
;;; Commentary:
;;
;; Use LSP for java mode. Mostly copied/inspired by github page:
;; https://github.com/emacs-lsp/lsp-java
;;
;;; Code:

(require 'mypackage-config)
(mypackage-require-packages '(lsp-java
                              java-snippets))

(mymodule-activate "mylsp.el")

(defun my-java-mode-hook ()
  (lsp)
  (setq indent-tabs-mode nil))

(with-eval-after-load "lsp-mode"
  (autoload 'lsp-java "lsp-java" nil t)
  (with-eval-after-load "lsp-java"
    (diminish 'lsp-java)
    (add-hook 'java-mode-hook #'my-java-mode-hook)))

(with-eval-after-load "java-mode"
  (require 'java-snippets))

(add-hook 'java-mode-hook 'yas-minor-mode)


(provide 'myjava)
;;; myjava.el ends here
