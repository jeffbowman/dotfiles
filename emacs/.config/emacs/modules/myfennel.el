;;; MYFENNEL --- Fennel configuration
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 25 November 2021
;;
;;; Commentary:
;;
;; Fennel is a Lisp language for Lua programming.  It compiles as Lua
;; code and can be used where Lua would be used.  It is inspired by
;; Clojure syntax.
;;
;;; Code:

(require 'mypackage-config)
(require 'mymodule-config)
(mypackage-require-packages '(fennel-mode))

(require 'eldoc)
(mymodule-activate "myprogmode.el")

;; TODO: Is this needed?
;; (autoload 'fennel-mode (expand-file-name "fennel-mode/fennel-mode" package-user-dir) nil t)
(add-to-list 'auto-mode-alist '("\\.fnl\\'" . fennel-mode))

(provide 'myfennel)
;;; myfennel.el ends here
