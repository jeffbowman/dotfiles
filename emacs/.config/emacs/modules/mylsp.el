;;; MYLSP --- lsp mode for use with various programming modes
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 23 April 2020
;;
;;; Commentary:
;;
;; require this module in the appropriate myXXX.el module to get lsp
;; configuration.
;;
(require 'mypackage-config)
(mypackage-require-packages '(helm-lsp
                              lsp-mode
                              lsp-ivy
                              lsp-ui
                              lsp-treemacs))

;; This needs to be here because evaluating it *after* lsp is loaded
;; doesn't work.
(customize-set-variable 'lsp-keymap-prefix "C-c l")

;; customize variables before loading lsp-mode
(customize-set-variable 'lsp-headerline-breadcrumb-enable t)
(customize-set-variable 'lsp-auto-guess-root nil)
(customize-set-variable 'lsp-prefer-flymake nil)

(autoload 'lsp "lsp-mode" nil t)
(add-hook 'lsp-mode-hook #'lsp-enable-which-key-integration)

(with-eval-after-load "lsp-mode"
  (diminish 'lsp-mode "LSP")
  (lsp-headerline-breadcrumb-mode -1)

  (require 'lsp-treemacs)
  (require 'lsp-ivy)

  (require 'lsp-ui)
  (diminish 'lsp-ui-mode)

  ;; faces
  (custom-set-faces (backquote (lsp-ui-doc-background ((t (:background nil))))))
  (custom-set-faces (backquote (lsp-ui-doc-header ((t (:inherit (font-lock-string-face italic)))))))
  (custom-set-faces (backquote (lsp-headerline-breadcrumb-prefix-face ((t (:inherit (font-lock-keyword-face)))))))
  (custom-set-faces (backquote (lsp-headerline-breadcrumb-project-prefix-face ((t (:inherit (font-lock-keyword-face bold)))))))

  ;; keybindings
  (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
  (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references)
  (define-key lsp-ui-mode-map (kbd "C-c u") #'lsp-ui-imenu)

  ;; customizations
  (customize-set-variable 'lsp-ui-doc-enable t)
  (customize-set-variable 'lsp-ui-doc-header t)
  (customize-set-variable 'lsp-ui-doc-include-signature t)
  (customize-set-variable 'lsp-ui-doc-position 'top)
  (customize-set-variable 'lsp-ui-doc-border (face-foreground 'default))
  (customize-set-variable 'lsp-ui-sideline-enable nil)
  (customize-set-variable 'lsp-ui-sideline-ignore-duplicate t)
  (customize-set-variable 'lsp-ui-sideline-show-code-actions nil)

  ;; configuration
  (setq lsp-ui-doc-use-webkit t)
  ;; WORKAROUND Hide mode-line of the lsp-ui-imenu buffer
  ;; https://github.com/emacs-lsp/lsp-ui/issues/243
  (defadvice lsp-ui-imenu (after hide-lsp-ui-imenu-mode-line activate)
    (setq mode-line-format nil)))

;; Pulled from 'myjava configuration as I want this for all LSP
;; implmentations.
(defun my-lsp-mode-hook ()
  "Use helm-lsp for code actions and workspace symbols."

  ;; helm-lsp is used for code actions like refactoring; adding final;
  ;; generating setters/getters, constructors, equals, hashcode,
  ;; toString, etc. This seems to be the way forward, using other
  ;; tools like ivy is not well documented and I couldn't get it to
  ;; work. This is the only part of helm I use and it works well
  ;; enough.
  (require 'helm-lsp)
  (unless (fboundp 'helm-lsp-code-actions)
    (autoload #'helm-lsp-code-actions "helm-lsp" nil t))

  (unless (fboundp 'helm-lsp-workspace-symbol)
    (autoload #'helm-lsp-workspace-symbol "helm-lsp" nil t))

  (define-key lsp-mode-map [remap lsp-execute-code-action] #'helm-lsp-code-actions)
  (define-key lsp-mode-map [remap xref-find-apropos] #'helm-lsp-workspace-symbol))

(add-hook 'lsp-mode-hook #'my-lsp-mode-hook)

(provide 'mylsp)
;;; mylsp.el ends here
