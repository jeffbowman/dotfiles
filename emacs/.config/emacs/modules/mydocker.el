;;; Summary --- Docker configuration
;;; Commentary:
;;
;; Loads the docker packages.
;;
;;; Code:

(require 'mypackage-config)
(mypackage-require-packages '(docker
                              docker-compose-mode
                              docker-tramp
                              dockerfile-mode))

(autoload 'docker "docker" nil t)
(with-eval-after-load "docker"
  (require 'docker-tramp)
  (require 'docker-compose-mode)
  (require 'dockerfile-mode))

(provide 'mydocker)
;;; mydocker.el ends here
