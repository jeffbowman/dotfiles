;;; myutils.el --- Summary
;;;
;;; Commentary:
;;; A place to put various utility functions used from
;;; time to time.
;;;
;;; Code:

(defun efix-name (name)
  "Convert NAME with spaces to NAME with dashes rather than spaces.

NAME is a string, from which the spaces are removed and the
result is joined with '-', downcased and stored in the kill ring"
  (interactive "sName: ")
  (let* ((parts (thread-last name
		  (split-string)
		  (mapcar #'downcase)))
	 (new-name (seq-reduce (lambda (acc str) (concat acc "-" str))
			       (cdr parts)
			       (car parts))))
    (message new-name)
    (with-current-buffer "*Messages*"      
      (save-excursion
	(goto-char (point-max))
	(re-search-backward new-name)
	(kill-ring-save (point) (progn (end-of-line) (point)))))))

(random t)

(defun insert-random-uuid ()
  "Insert a random UUID
Example of a UUID: 1df63142-a513-c850-31a3-535fc3520c3d

WARNING: this is a simple implementation. The chance of
generating the same UUID is much higher than a robust
algorithm.."

  (interactive)
  (insert
   (format "%04x%04x-%04x-%04x-%04x-%06x%06x"
	   (random (expt 16 4))
	   (random (expt 16 4))
	   (random (expt 16 4))
	   (random (expt 16 4))
	   (random (expt 16 4))
	   (random (expt 16 4))
	   (random (expt 16 6))
	   (random (expt 16 6)))))


(defun myutils-package-byte-recompile-dir (pkg)
  "Byte recompile directory for PKG.

PKG is a symbol for the package to recompile. This is needed
sometimes when packages are upgraded and errors
occur. `package.el' does not seem to handle this well. This will
recompile the package, but not necessarily reload it. Emacs may
need to be restarted. "
  (interactive)
  (let ((pkg-dir (mycustom--package-full-directory-name pkg)))
    (when (file-exists-p pkg-dir)
      (byte-recompile-directory pkg-dir 0 t))))

(provide 'myutils)

;;; myutils.el ends here
