;;; MYRUST --- configuration for the rust programming language
;;
;; Author: Jeff Bowman <jeff@bowmansarrow.us>
;; Created: 29 May 2021
;;
;;; Commentary:
;;
;; Uses =rustic= configure a rust development environment, which
;; includes a LSP configuration. Require my LSP configuration here for
;; my settings, but then rustic takes over and autoconfigures LSP and
;; a few other things, just by switching to =rustic-mode=
;;
;;; Code:

(require 'mypackage-config)
(require 'mymodule-config)
(mypackage-require-packages '(flycheck-rust
                              rust-auto-use
                              rustic))

(mymodule-activate "mylsp.el")

(with-eval-after-load "lsp-mode"
  (autoload #'rustic-mode "rustic" nil t)
  (customize-set-variable 'lsp-rust-analyzer-server-display-inlay-hints t)
  (with-eval-after-load "rustic"
    (setq rustic-format-on-save t)
    (define-key rustic-mode-map (kbd "C-c C-c a") #'rust-auto-use)
    (define-key rustic-mode-map (kbd "C-c C-c l") #'flycheck-list-errors)
    (define-key rustic-mode-map (kbd "C-c C-c r") #'lsp-rename)
    (define-key rustic-mode-map (kbd "C-c C-c s") #'lsp-rust-analyzer-status)))

(provide 'myrust)
;;; myrust.el ends here
