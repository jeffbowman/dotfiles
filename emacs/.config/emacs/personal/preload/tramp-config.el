;;; Summary --- tramp-config.el
;;;
;;; Commentary:
;;;
;;;    tramp hangs on load, these settings prevent that
;;;    allowing emacs to start much faster
;;;
;;; Code:

;; tramp and zsh don't play well together, reset the $SHELL variable to bash
(eval-after-load 'tramp '(setenv "SHELL" "/bin/bash"))

;; not sure what this does, seems to be related to ssh prompts not
;; matching, but more info is here:
;; http://emacs.stackexchange.com/questions/18438/emacs-suspend-at-startup-ssh-connection-issue
(setq tramp-ssh-controlmaster-options
      "-o ControlMaster=auto -o ControlPath='tramp.%%C' -o ControlPersist=no")

;;; tramp-config.el ends here
