;;; Summary --- change default prelude theme
;;;
;;; Commentary:
;;;   Use a different theme
;;;
;;; Code:

(setq prelude-theme 'jb-simple)

;;; theme.el ends here
