# Personal Directory

This directory contains the active modules in use and the
`mymodules.el` file which loads them. Another file here is the
`mycustom.el` file which, conceptually, is like the `custom.el` file
in the parent directory, and is just another place to collect general
customizations which don't seem to belong in their own module.

## 2021-06-02

After a rewrite to stop using `use-package`, modules currently in use
are examples of the rewrite while modules in the `../archive` folder have
yet to be rewritten from the `use-package` approach. Additionally,
they represent short lived efforts which I don't want to
lose. Examples would be my short lived attempts at `go`, `rust`, using
`exwm` for my window manager, `erlang` from a class, etc.

To use one of the modules in the `../archive` folder, move it here and
rewrite it to match the current approach. As a possible, quick-fix the
following can be used at the top of the module until the `use-package`
configuration can be replaced:

```
(require 'mypackage-config)
(mypackage-require-packages '(use-package))
```
## 2021-06-03

Available modules reside in `../modules`, to keep anything from
`../archive`, move it to `../modules` and make appropriate updates to
remove `use-package` (or see above). See `../core/mymodule-config.el`
for code related to how modules are handled, see `mymodules.el` here
for example on usage.
