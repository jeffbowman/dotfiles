;; Early init customizations for rational emacs

;; Set the default font face before displaying the frame to avoid
;; resize issues.
(cond
 ((string-equal (symbol-name system-type) "windows-nt")
  ;; Windows does not have Anonymous Pro, use Courier New instead
  (custom-set-faces (backquote (default ((t (:family "Courier New" :height 140)))))
                    (backquote (fixed-pitch ((t (:height 140 :inherit (default))))))
                    (backquote (fixed-pitch-serif ((t (:height 140 :inherit (default))))))))
 ((string-equal (symbol-name system-type) "darwin")
  ;; MacOS does not have Anonymous Pro, use Monaco instead
  (custom-set-faces (backquote (default ((t (:family "Monaco" :height 180)))))
                    (backquote (fixed-pitch ((t (:height 180 :inherit (default))))))
                    (backquote (fixed-pitch-serif ((t (:height 180 :inherit (default))))))))
 (t
  ;; Linux systems, can easily install this font
  (custom-set-faces (backquote (default ((t (:family "Iosevka Term" :weight light :height 180)))))
                    (backquote (fixed-pitch ((t (:height 180 :inherit (default))))))
                    (backquote (fixed-pitch-serif ((t (:height 180 :inherit (default)))))))))

;; (custom-set-faces
;;  '(default ((t (:family "Anonymous Pro" :height 180))))
;;  '(fixed-pitch-serif ((t (:height 160 :family "Monospace Serif")))))

;; Make the *scratch* buffer load with the lisp interaction mode
(customize-set-variable 'initial-major-mode #'lisp-interaction-mode)

;; turn menubar back on
(setf (alist-get 'menu-bar-lines default-frame-alist) 1)

;; remove default fore/background colors so theme values loaded in
;; config.el take effect
(setf default-frame-alist (assq-delete-all 'background-color default-frame-alist))
(setf default-frame-alist (assq-delete-all 'foreground-color default-frame-alist))

(if (version<= "28.0" emacs-version)
    (progn
      (disable-theme 'deeper-blue)
      (load-theme 'modus-vivendi t)
      (customize-set-variable 'modus-themes-syntax '(faint green-strings yellow-comments))
      (customize-set-variable 'modus-themes-links '(faint background)))
  (load-theme 'deeper-blue))

;; packages
(add-to-list 'package-pinned-packages (cons 'embark-consult "stable"))

(provide 'early-config)
;; early-config.el ends here
