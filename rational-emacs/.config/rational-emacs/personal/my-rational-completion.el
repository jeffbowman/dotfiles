;;; my-rational-completion.el --- completion frameworks  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  
;; SPDX-License-Identifier: MIT

;; Author:  Jeff Bowman

;;; Commentary:

;; My overrides and customizations for "completion" frameworks. This
;; is different from `corfu', as this is not related to code
;; completions, rather this is for minibuffer completions.

;;; Code:

(require 'rational-package)

(let ((packages (list 'vertico 'consult 'orderless 'marginalia 'embark)))
  (unless (check-all-installed packages)
    (rational-install-packages packages)))

(defun rational-completion/minibuffer-backward-kill (arg)
  "When minibuffer is completing a file name delete up to parent
folder, otherwise delete a word"
  (interactive "p")
  (if minibuffer-completing-file-name
      ;; Borrowed from https://github.com/raxod502/selectrum/issues/498#issuecomment-803283608
      (if (string-match-p "/." (minibuffer-contents))
          (zap-up-to-char (- arg) ?/)
        (delete-minibuffer-contents))
    (backward-kill-word arg)))

;;;; Vertico

(require 'vertico)
(require 'vertico-directory)

;; Cycle back to top/bottom result when the edge is reached
(customize-set-variable 'vertico-cycle t)

;; Start Vertico
(vertico-mode 1)

;;;; Marginalia

;; Configure Marginalia
(require 'marginalia)
(setq marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
(marginalia-mode 1)

;; Set some consult bindings
(global-set-key (kbd "C-s") 'consult-line)
(define-key minibuffer-local-map (kbd "C-r") 'consult-history)

(setq completion-in-region-function #'consult-completion-in-region)

;;;; Orderless

;; Set up Orderless for better fuzzy matching
(require 'orderless)
(customize-set-variable 'completion-styles '(orderless))
(customize-set-variable 'completion-category-overrides '((file (styles . (partial-completion)))))
(setq completion-category-defaults nil)

;;;; Embark

(global-set-key [remap describe-bindings] #'embark-bindings)
(global-set-key (kbd "C-.") 'embark-act)

;; Use Embark to show bindings in a key prefix with `C-h`
(setq prefix-help-command #'embark-prefix-help-command)

(provide 'my-rational-completion)
;;; my-rational-completion.el ends here
