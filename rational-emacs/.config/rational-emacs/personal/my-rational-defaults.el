;;;; my-rational-defaults.el --- My rational defaults   -*- lexical-binding: t; -*-

;; Copyright (C) 2022
;; SPDX-License-Identifier: MIT

;; Author: System Crafters Community

;;; Commentary:

;; Overrides and additions for rational-defaults specific to my
;; configuration.

;;; Code:

(require 'rational-defaults)

;; Provided in rational-defaults
;; use yes/no for confirm/negate prompts
;; (if (boundp 'use-short-answers)
;;     (setq use-short-answers nil)
;;   (advice-remove 'yes-or-no-p #'y-or-n-p))
(if (boundp 'use-short-answers)
    (customize-set-variable 'use-short-answers nil)
  (advice-remove 'yes-or-no-p #'y-or-n-p))

(put 'narrow-to-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'scroll-left 'disabled nil)

(delete-selection-mode t)

;; use ibuffer as a buffer switcher
(define-key global-map [remap list-buffers] #'ibuffer)

;; ibuffer configuration
(add-hook 'ibuffer-hook
          (lambda ()
            (setq ibuffer-filter-groups (ibuffer-project-generate-filter-groups))
            (unless (eq ibuffer-sorting-mode 'project-file-relative)
              (ibuffer-do-sort-by-project-file-relative))))

(provide 'my-rational-defaults)
;;; rational-defaults.el ends here
