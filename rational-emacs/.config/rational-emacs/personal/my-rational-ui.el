;;;; my-rational-ui.el --- My rational UI custom config -*- lexical-binding: t; -*-

;; Copyright (C) 2022
;; SPDX-License-Identifier: MIT

;; Author: System Crafters Community

;;; Commentary:

;; Overrides and additions for rational-ui specific to my
;; configuration.

;;; Code:

(require 'rational-ui)

(with-eval-after-load "rational-ui"
  (customize-set-variable 'rational-ui-display-line-numbers t)
  (add-to-list 'rational-ui-line-numbers-enabled-modes 'emacs-lisp-mode))

(provide 'my-rational-ui)
;;; my-rational-ui.el ends here
