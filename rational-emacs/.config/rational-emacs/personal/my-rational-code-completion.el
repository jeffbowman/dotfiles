;;;; my-rational-code-completion.el --- My code completion config  -*- lexical-binding: t; -*-

;; Copyright (C) 2022
;; SPDX-License-Identifier: MIT

;; Author: System Crafters Community

;;; Commentary:

;; Overrides and additions for rational-code-completion specific to my
;; configuration.
;;
;; * Uses Corfu for completion ui
;; * Congfigures tempel for snippet expansion using tempo language.

;;; Code:
(require 'rational-package)
(require 'rational-code-completion nil t)

(let ((packages (list 'corfu 'tempel)))
  (unless (check-all-installed packages)
    (rational-install-packages packages)))

;; does not seem to be in melpa, but quelpa can fetch it from git.
(unless (package-installed-p 'corfu-doc)
  (quelpa '(corfu-doc :fetcher github :repo "galeo/corfu-doc")))

;; corfu
(corfu-global-mode)
(customize-set-variable 'completion-cycle-threshold 3)
(customize-set-variable 'tab-always-indent 'complete)
(customize-set-variable 'corfu-auto nil) ; turn off auto empletion
(customize-set-variable 'corfu-echo-documentation 0.25)

;; reset default dabbrev/corfu keybindings
(define-key global-map (kbd "M-/") #'dabbrev-expand)
(define-key corfu-map  (kbd "TAB") #'corfu-complete)
(define-key corfu-map  [tab]       #'corfu-complete)

;; corfu-doc
(add-hook 'corfu-mode-hook #'corfu-doc-mode)
(define-key corfu-map (kbd "M-p") #'corfu-doc-scroll-down) ; corfu-next
(define-key corfu-map (kbd "M-n") #'corfu-doc-scroll-up)   ; corfu-previous
(define-key corfu-map (kbd "M-d") #'corfu-doc-toggle)

;; tempel
(require 'tempel)
;; Allows expand-abbrev (kbd "C-x '") to expand tempel templates
(tempel-global-abbrev-mode)

;; (add-to-list 'completion-at-point-functions #'tempel-complete)
;; (add-to-list 'completion-at-point-functions #'tempel-expand)

(defun tempel-setup-capf ()
  "Setup CAPF functions from tempel"
  (setq-local completion-at-point-functions
              (cons #'tempel-expand
                    completion-at-point-functions)))

(add-hook 'prog-mode-hook #'tempel-setup-capf)
(add-hook 'text-mode-hook #'tempel-setup-capf)

(provide 'my-rational-code-completion)
;;; my-rational-code-completion.el ends here
