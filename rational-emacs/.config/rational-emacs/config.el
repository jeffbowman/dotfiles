;;;; config.el --- rational emacs custom config      -*- mode: emacs-lisp; mode: outline-minor; lexical-binding: t; -*-

;; Copyright (C) 2022

;; Author:  Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Personal configuration. Use C-x [ to page forward or C-x ] to page
;; backward through the sections. Outline minor mode is also active,
;; C-c @ is the key prefix.

;;; Code:


;;; Rational Emacs Modules
;; (require 'rational-compile)
;; (require 'rational-completion)
(require 'rational-defaults)
(require 'rational-editing)
(require 'rational-ide)
(require 'rational-lisp)
(require 'rational-python)
(require 'rational-ui)

;; Custom Modules
(require 'my-rational-completion)
(require 'my-rational-org)


;;; module configuration

(with-eval-after-load "rational-completion"
  (add-hook 'prog-mode-hook
            (lambda ()
              (define-key prog-mode-map (kbd "C-c '") #'consult-imenu))))

(with-eval-after-load "rational-defaults"
  (if (boundp 'use-short-answers)
      (setq use-short-answers nil)
    (advice-remove 'yes-or-no-p #'y-or-n-p))

  ;; FIXME: Remove after PR #158 is merged
  ;; Keep state files in `rational-config-var-directory' by default
  ;; we use `with-eval-after-load' to only affect what is being used.
  (with-eval-after-load 'saveplace
    (customize-set-variable 'save-place-file
                            (expand-file-name "places" rational-config-var-directory)))
  (with-eval-after-load 'bookmark
    (customize-set-variable 'bookmark-default-file
                            (expand-file-name "bookmarks" rational-config-var-directory)))
  (with-eval-after-load 'tramp
    (customize-set-variable 'tramp-persistency-file-name
                            (expand-file-name "tramp" rational-config-var-directory)))
  (with-eval-after-load 'org-id
    (customize-set-variable 'org-id-locations-file
                            (expand-file-name "org-id-locations" rational-config-var-directory)))
  (with-eval-after-load 'nsm
    (customize-set-variable 'nsm-settings-file
                            (expand-file-name "network-security.data" rational-config-var-directory)))
  (with-eval-after-load 'project
    (customize-set-variable 'project-list-file
                            (expand-file-name "projects" rational-config-var-directory)))

  (customize-set-variable 'eudc-options-file
                          (expand-file-name "eudc-options" rational-config-etc-directory))

  (when (package-installed-p 'bbdb)
    (with-eval-after-load "bbdb"
      (customize-set-variable 'bbdb-file
                              (expand-file-name "bbdb" rational-config-var-directory))))


  (with-eval-after-load "calendar"
    (customize-set-variable 'diary-file
                            (expand-file-name "diary" rational-config-etc-directory)))

  )

(with-eval-after-load "rational-compile"
  (add-to-list 'rational-compile-module-list 'rational-ui)
  (add-to-list 'rational-compile-module-list 'rational-defaults)
  (add-to-list 'rational-compile-module-list 'rational-completion)
  (add-to-list 'rational-compile-module-list 'rational-compile))

(with-eval-after-load "rational-editing"
  (add-to-list 'rational-editing-whitespace-cleanup-enabled-modes 'prog-mode)
  (add-to-list 'rational-editing-whitespace-cleanup-enabled-modes 'text-mode))

(with-eval-after-load "rational-lisp"
  (rational-package-install-package 'geiser-gauche)
  (rational-package-install-package 'fennel-mode)

  (with-eval-after-load 'geiser-impl
    (add-to-list 'geiser-active-implementations 'gauche))

  (add-to-list 'auto-mode-alist '("\\.fnl\\'" . fennel-mode)))

(with-eval-after-load "rational-ui"
  (customize-set-variable 'rational-ui-display-line-numbers t)
  (remove-hook 'after-init-hook 'doom-modeline-mode))


;;; general customization
(customize-set-variable 'dired-dwim-target t)

(column-number-mode t)
(delete-selection-mode t)
(electric-pair-mode t)
(show-paren-mode t)
(hl-line-mode t)

(put 'narrow-to-region 'disabled nil)
(put 'upcase-region    'disabled nil)
(put 'downcase-region  'disabled nil)
(put 'scroll-left      'disabled nil)

(unless (version< emacs-version "28")
  (repeat-mode 1))

(customize-set-variable 'ispell-program-name "/usr/bin/aspell")
(customize-set-variable
 'ispell-personal-dictionary (expand-file-name ".aspell-personal"
                                               rational-config-var-directory))

;;; tempo snippets, autoinsert and skeletons
(when (featurep 'autoinsert)
  (auto-insert-mode)

  (add-hook 'find-file-hook #'auto-insert)

  ;; Don't want to be prompted before insertion:
  (customize-set-variable 'auto-insert-query nil)
  (customize-set-variable 'auto-insert-directory
                          (expand-file-name "templates" rational-config-etc-directory))
  ;; (define-auto-insert "\\.el?$" ["default-elisp.el" mysnippet-autoinsert-yas-expand])
  ;; (define-auto-insert "pom.xml" ["default-pom.xml" mysnippet-autoinsert-yas-expand])

  ;; uses builtin auto-insert mechanism, not yasnippets as above
  (with-eval-after-load "autoinsert"
    ;; if it is an interview, load a blank interview file.
    (define-auto-insert
      ".*/interviews/[a-z_-]+[0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}.org" "blank-interview.org" nil)
    (define-auto-insert
      ".*/work/appnovation/\\(projects\\|mulesoft\\)/.*/.*\\.org" "newproject.org")))

;;; snippets
;; use built-in tempo mode to create snippets, a little more verbose
;; than yasnippet, but only barely, and used with abbrev-mode
;; turn on abbrev-mode everywhere
(setq-default abbrev-mode t)
(customize-set-variable 'abbrev-file-name
                        (expand-file-name "abbrev_defs" rational-config-var-directory))

;; use tempo mode to define snippets
(require 'tempo)
(customize-set-variable 'tempo-interactive t) ; allows for prompting
                                              ; when inserting
                                              ; snippets
;; define org header for new org files
(tempo-define-template
 "org-header"
 '("#+STARTUP: lognotereschedule logdrawer overview" n
   "#+SEQ_TODO: TODO(t/!) IN_PROGRESS(i/!) HOLD(h@/!) READY_FOR_QA(r@/!) | DONE(d) CANCELED(c@)" n
   "#+COLUMNS: %50ITEM %CLOCKSUM %CLOCKSUM_T" n)
 "orgheader"
 "Insert standard Org header"
 nil)

;; define a new blog entry (also found in my custom yasnippets)
(tempo-define-template
 "newblog"
 '("#+TITLE: " (p "Title: " title) n
   "#+OPTIONS: toc:nil num:nil todo:nil pri:nil tags:nil ^:nil" n
   "#+DESCRIPTION: " (p "Description: " description) n
   "#+DATE: " (org-time-stamp t) n n
   p
   n "Tags: " n
   (progn
     (tempo-forward-mark)
     (when (package-installed-p 'writefreely)
       (load-write-freely)
       (write-freely-mode)))))

(defun my-calendar-week-number ()
  "Return the current week of the year number as a string."
  (number-to-string
   (car (calendar-iso-from-absolute
         (string-to-number (cadr (split-string
                                  (calendar-day-of-year-string (calendar-current-date)))))))))

(defun my-calendar-current-year ()
  "Return the curren year as a string"
  (number-to-string (caddr (calendar-current-date))))

;; define weektime for tracking time in projects
(tempo-define-template
 "weektime"
 '("#+BEGIN: clocktable :scope file :block " (my-calendar-current-year) "-w" (my-calendar-week-number)
   " :step day :maxlevel 5 :compact t :emphasize t :stepskip0 t" n
   "#+END:")
 "weektime"
 "Insert standard week timeclock summary"
 nil)

;; define bills table for personal bill tracking
(tempo-define-template
 "billtable"
 '("| Account             | Due Date | Date Paid | Amount Due | Amount Paid | Total |" n>
   "|---------------------+----------+-----------+------------+-------------+-------|" n>
   "| OG&E                |          |           |            |             |       |" n>
   "| Capitol One         |          |           |            |             |       |" n>
   "| ONG                 |          |           |            |             |       |" n>
   "| College Tuition     |          |           |            |             |       |" n>
   "| USAA Visa           |          |           |            |             |       |" n>
   "| American Express    |          |           |            |             |       |" n>
   "|                     |          |           |            |             |       |" n>
   "| USAA Visa           |          |           |            |             |       |" n>
   "| American Express    |          |           |            |             |       |" n>
   "| OKC Utilities       |          |           |            |             |       |" n>
   "| TFCU                |          |           |            |             |       |" n>
   "| Nelnet Student Loan |          |           |            |             |       |" n>
   "|                     |          |           |            |             |       |" n>
   "#+TBLFM: @8$6=vsum(@2$5..@8$5)::@>$6=vsum(@9$5..@>>$5)" n
   ))



;; Set an abbrev to expand to the tempo snippet above, but have to
;; wrap these to avoid errors where the abbrev table is not created
;; when Emacs starts.
(with-eval-after-load "org"
  (define-abbrev org-mode-abbrev-table "billtable" "" 'tempo-template-billtable)
  (define-abbrev org-mode-abbrev-table "newblog" "" 'tempo-template-newblog)
  (define-abbrev org-mode-abbrev-table "orgheader" "" 'tempo-template-org-header)
  (define-abbrev org-mode-abbrev-table "weektime" "" 'tempo-template-weektime))


;;; general keybindings

;; zap-up-to-char is more useful
(define-key global-map (kbd "M-z") #'zap-up-to-char)
(define-key global-map (kbd "C-+")           #'text-scale-increase)
(define-key global-map (kbd "C--")           #'text-scale-decrease)
(define-key global-map (kbd "<f5>")          #'execute-extended-command)
(define-key global-map (kbd "<f6>")          #'isearch-forward)
(define-key global-map [remap zap-to-char]   #'zap-up-to-char)
(define-key global-map [remap upcase-word]   #'upcase-dwim)
(define-key global-map [remap downcase-word] #'downcase-dwim)

(defcustom my-windows-prefix-key "C-c w"
  "Configure the prefix key for `my-windows' bindings.")

(define-prefix-command 'my-windows-key-map)

(define-key 'my-windows-key-map (kbd "n") 'windmove-down)
(define-key 'my-windows-key-map (kbd "p") 'windmove-up)
(define-key 'my-windows-key-map (kbd "b") 'windmove-left)
(define-key 'my-windows-key-map (kbd "f") 'windmove-right)

(global-set-key (kbd my-windows-prefix-key) 'my-windows-key-map)


;;; initial package installation
(rational-package-install-package 'avy)
(rational-package-install-package 'crux)
(rational-package-install-package 'diff-hl)
(rational-package-install-package 'diminish)
(rational-package-install-package 'editorconfig)
(rational-package-install-package 'eldoc)
(rational-package-install-package 'ibuffer-project)
(rational-package-install-package 'indent-tools)
(rational-package-install-package 'key-chord)
(rational-package-install-package 'magit)
(rational-package-install-package 'pinentry)
(rational-package-install-package 'verb)
(rational-package-install-package 'which-key)
(rational-package-install-package 'writefreely)
(rational-package-install-package 'yaml-mode)


;;; installed package configuration
(add-hook 'prog-mode
          (lambda ()
            (hs-minor-mode)
            (flymake-mode)
            (editorconfig-mode)))

(with-eval-after-load "editorconfig"
  (diminish 'editorconfig-mode "EC"))

;;; bbdb - big brother insidious database contacts rolodex
(when (featurep 'bbdb)
  (bbdb-initialize 'gnus 'message))

;;; eudc - ldap like lookup for contacts etc
(with-eval-after-load "message"
  (define-key message-mode-map [(control ?c) (tab)] #'eudc-expand-inline))
(with-eval-after-load "sendmail"
  (define-key message-mode-map [(control ?c) (tab)] #'eudc-expand-inline))

(customize-set-variable 'eudc-server-hotlist
                        '(("" . bbdb)))

;;; ibuffer for list-buffers
(customize-set-variable 'ibuffer-movement-cycle nil)
(customize-set-variable 'ibuffer-old-time 24)
(add-hook 'ibuffer-hook
          (lambda ()
            (setq ibuffer-filter-groups (ibuffer-project-generate-filter-groups))
            (unless (eq ibuffer-sorting-mode 'project-file-relative)
              (ibuffer-do-sort-by-project-file-relative))))
(define-key global-map (kbd "C-x C-b") #'ibuffer)

;; navigation / buffer management
;;
;; crux is for some nice utilities
(require 'crux)
(with-eval-after-load "avy"
  (avy-setup-default))
(with-eval-after-load "crux"
  (key-chord-define-global "ji" #'avy-goto-char-in-line)
  (key-chord-define-global "jj" #'avy-goto-word-1)
  (key-chord-define-global "jl" #'avy-goto-line)
  (key-chord-define-global "jk" #'avy-goto-char)
  (key-chord-define-global "JJ" #'crux-switch-to-previous-buffer)
  (key-chord-define-global "xx" #'execute-extended-command)
  (key-chord-mode +1)

  (define-key global-map [remap move-beginning-of-line] #'crux-move-beginning-of-line)
  (define-key global-map (kbd "C-c d") #'crux-duplicate-current-line-or-region)
  (define-key global-map (kbd "C-c f") #'crux-recentf-find-file)
  (define-key global-map (kbd "C-c k") #'crux-kill-other-buffers)
  (define-key global-map (kbd "C-c n") #'crux-cleanup-buffer-or-region))

;;; which-key
(customize-set-variable 'which-key-show-early-on-C-h t)
(customize-set-variable 'which-key-idle-delay 1.0)
(customize-set-variable 'which-key-idel-secondary-delay 0.05)
(which-key-mode)
(with-eval-after-load "which-key"
  (which-key-setup-side-window-right-bottom))

;;; writefreely

;; This is called in the yasnippet snippet to create a new blog entry.
(defun load-write-freely ()
  "Loads writefreely.  Use similar to a hook function"
  (interactive)
  (unless (featurep 'writefreely)
    (package-activate 'writefreely))
  (writefreely-mode))

;; Once writefreely is loaded, get the auth-token so blogs published
;; will show up correctly.
(with-eval-after-load "writefreely"
  (require 'auth-source)
  (customize-set-variable 'writefreely-auth-token
                          (auth-source-pick-first-password :host "write.as")))


;;; pinentry -- used by vc-mode & magit
;; gpg config to use pinentry from the minibuffer
(customize-set-variable 'epg-pinentry-mode 'loopback)

;; MS Windows does not support local sockets, so this won't work
;; there.
(unless ON-WINDOWS (pinentry-start))

;;; magit & diff-hl
(customize-set-variable 'magit-diff-refine-hunk 'all)
(define-key global-map (kbd "C-x g") #'magit-status)

;; Add diff-hl to vc-mode command prefix
(customize-set-variable 'diff-hl-command-prefix (kbd "C-x v"))
(global-diff-hl-mode)
(add-hook 'magit-process-find-password-functions
          #'magit-process-password-auth-source)
(add-hook 'magit-post-refresh-hook #'diff-hl-magit-post-refresh)


;;; Python configuration
;; put the anaconda python files in the rational-config-var-directory
(customize-set-variable 'anaconda-mode-installation-directory
                        (expand-file-name "anaconda-mode" rational-config-var-directory))

;; Suggested additional keybindings for python mode
(with-eval-after-load "python"
  (define-key python-mode-map (kbd "C-c C-n") #'numpydoc-generate)
  (define-key python-mode-map (kbd "C-c e n") #'flymake-goto-next-error)
  (define-key python-mode-map (kbd "C-c e p") #'flymake-goto-prev-error))

;; Suggested keybindings for pyvenv mode
(with-eval-after-load "pyvenv"
  (define-key pyvenv-mode-map (kbd "C-c p a") #'pyvenv-activate)
  (define-key pyvenv-mode-map (kbd "C-c p d") #'pyvenv-deactivate)
  (define-key pyvenv-mode-map (kbd "C-c p w") #'pyvenv-workon))


(provide 'config)
;;; config.el ends here
