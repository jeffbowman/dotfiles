;;; my-rational-completion.el -*- mode: emacs-lisp; mode: outline-minor; lexical-binding: t; -*-

;; Copyright (C) 2022

;; Author:  Jeff Bowman <jeff.t.bowman@gmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary

;; My preferences for completion, layered with rational-completion,
;; but here using selectrum and prescient instead of vertico

;;; Code:
(require 'rational-completion)
(vertico-mode -1)

(rational-package-install-package 'prescient)
(rational-package-install-package 'selectrum)
(rational-package-install-package 'selectrum-prescient)

(customize-set-variable 'prescient-save-file
                        (expand-file-name "prescient-save.el" rational-config-var-directory))


;;; Selectrum
(require 'selectrum)
(require 'selectrum-prescient)
(customize-set-variable 'selectrum-highlight-candidates-function
                        #'orderless-highlight-matches)
(customize-set-variable 'orderless-skip-highlighting (lambda () selectrum-is-active))
(selectrum-mode +1)

;; use this to layer prescient with orderless
;; see: https://github.com/radian-software/selectrum
(customize-set-variable 'selectrum-prescient-enable-filtering nil)
(selectrum-prescient-mode +1)
(prescient-persist-mode +1)


;;; Corfu
(customize-set-variable 'completion-cycle-threshold 3)
(customize-set-variable 'tab-always-indent 'complete)
;; disables auto completion, maybe I don't want to do that?
;; (customize-set-variable 'corfu-auto nil)

(define-key global-map [remap dabbrev-expand] #'hippie-expand)


;;; Cape
(require 'cape)
(define-key global-map (kbd "C-c c p") #'completion-at-point)
(define-key global-map (kbd "C-c c l") #'cape-line)


;;; Embark
;; additional keybinding for terminal based Emacs
(global-set-key (kbd "C-c .") 'embark-act)

(provide 'my-rational-completion)
;;; rational-completion.el ends here
