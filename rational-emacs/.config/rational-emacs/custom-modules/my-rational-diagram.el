;;; my-rational-diagram.el --- Modes to handle diagrams  -*- lexical-binding: t; -*-

;; Copyright (C) 2022

;; Author:  <vagrant@archlinux>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; PlantUML and WebSequenceDiagrams provide useful syntax and
;; genertors for different kinds of diagrams.

;; WebSequenceDiagrams provides simple syntax and uses a web service
;; to connect to https://www.websequencediagrams.com which wil
;; generate a png file.

;; PlantUML provides a similar syntax, but allows more kinds of
;; diagrams than a wsd.  A jar file is needed from
;; http://plantuml.com/download which should be placed in the home
;; directory or customized to be elsewhwere.

;;; Code:

(rational-package-install-package 'plantuml-mode)
(rational-package-install-package 'wsd-mode)

(add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode))

(add-to-list 'auto-mode-alist '("\\.wsd.\\'" . wsd-mode))

(provide 'my-rational-diagram)
;;; my-rational-diagram.el ends here
