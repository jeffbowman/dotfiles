;;; my-rational-org.el --- org-mode configuration  -*- mode: emacs-lisp; mode: outline-minor; lexical-binding: t; -*-

;; Copyright (C) 2022

;; Author:  <vagrant@archlinux>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; My personal org-mode configuration.

;;; Code:
(define-key global-map (kbd "C-c o a") #'org-agenda)
(define-key global-map (kbd "C-c o t") #'org-capture)
(customize-set-variable 'org-adapt-indentation t)
(customize-set-variable 'org-agenda-include-diary t)
(customize-set-variable 'org-export-backends (quote (ascii html icalendar latex md odt)))
(unless (file-exists-p (expand-file-name "~/org"))
  (warn "Creating ~/org folder, this is usually a symlink to Dropbox/org")
  (dired-create-directory (expand-file-name "~/org")))
(customize-set-variable 'org-directory "~/org")
(customize-set-variable 'org-refile-allow-creating-parent-nodes 'confirm)
(customize-set-variable 'org-refile-targets (quote ((nil :maxlevel . 9)
                                                    (org-agenda-files :maxlevel . 9))))
(customize-set-variable 'org-refile-use-outline-path 'file)
(customize-set-variable 'org-outline-path-complete-in-steps nil)
(customize-set-variable 'org-todo-keyword-faces
                        '(("HOLD" . (:foreground "cyan" :weight "bold"))
                          ("READY_FOR_QA" . (:foreground "dark green" :underline t))
                          ("IN_PROGRESS" . (:foreground "DarkOliveGreen1" :weight "bold"))))
(customize-set-variable 'org-capture-templates
                        '(("n" "Note" entry (file+headline "~/.notes" "Notes")
                           "** %U %?\n%i")
                          ("t" "Todo" entry (file+headline "~/.notes" "Tasks")
                           "** TODO %?\n  %i\n  %a")
                          ("i" "Issue" entry (file+headline "~/.notes" "Issues")
                           "* IN_PROGRESS %?
:PROPERTIES:
:ONSET:
:DESCRIPTION:
:ACTION:
:COMPLETED:
:END:" :empty-lines 0)))

(with-eval-after-load "org"
  ;; N.B. This may need to be re-evaluated if another directory is
  ;; added to the org tree.
  ;;
  ;; N.B. 2 - This needs to be setq here rather than in the :custom
  ;; section as the org-agenda-file-regexp is not defined at the right
  ;; time for this to be used in :custom instead.
  ;;
  ;; Change the org-agenda-files to only reflect agenda from work
  ;; files. This should speed up both loading an org file as well as
  ;; the agenda view.
  (unless (file-exists-p (expand-file-name "work" org-directory))
    (warn "Creating ~/org/work folder, this is usually an existing folder linked from Dropbox/org")
    (dired-create-directory (expand-file-name "work" org-directory)))

  ;; Turning this off in preference for using
  ;; `org-agenda-file-to-front' (kbd "C-c [" ) and `org-remove-file'
  ;; (kbd "C-c ]")
  ;;
  ;; (setq org-agenda-files
  ;;       (directory-files-recursively (expand-file-name "work" org-directory)
  ;;                                    org-agenda-file-regexp))

  ;; verb mode is for http/REST testing in org mode
  (autoload 'verb-mode "verb" nil t)
  (define-key org-mode-map (kbd "C-c r") verb-command-map)

  ;; counsel-org-goto is *much* easier to use -- deprecated switching
  ;; to consult
  ;; (define-key org-mode-map [remap org-goto] #'counsel-org-goto)

  (define-key org-mode-map [remap org-goto] #'consult-org-headings)

  ;; #'org-mark-ring-goto is the corollary to
  ;; #'org-open-at-point, ie jump back to where we came from
  ;; when we pressed C-c C-o to follow an internal link
  ;;
  ;; This is bound to 'C-c &' by org mode, however, yas overwrites
  ;; this, using 'C-c &' as a leader prefix for yas related
  ;; functions. Moving this to org-mode-map from global-map since it
  ;; should needs to be bound there.
  (define-key org-mode-map (kbd "C-c b")   #'org-mark-ring-goto)

  ;; counsel-org-goto is *much* easier to use -- deprecated switching
  ;; to consult
  ;; (define-key org-mode-map [remap org-goto] #'counsel-org-goto)

  (define-key org-mode-map [remap org-goto] #'consult-org-heading)

  ;; language support in org source blocks
  (org-babel-do-load-languages 'org-babel-load-languages
                               '((awk . t)
                                 (emacs-lisp . t)
                                 (shell . t))))

;; org mode presentations. Use M-x org-present to start the
;; presentation.
(eval-after-load "org-present"
  '(progn
     (add-hook 'org-present-mode-hook
               (lambda ()
                 (org-present-big)
                 (org-present-read-only)))
     (add-hook 'org-present-mode-quit-hook
               (lambda ()
                 (org-present-small)
                 (org-present-read-write)))))

(provide 'my-rational-org)
;;; my-rational-org.el ends here
