# Overview

Vagrant configuration to setup a local dev box, usually used for Emacs
/ Crafted Emacs development.  Could be used for other things, but will
need to update the package list to include other languages / tools as
necessary.

# Features

- Vagrant file which scripts the entire configuration.
- Packages are controlled through the `pacmanfile-vagrant.txt` file
  which is copied to the appropriate location when the box is built.
- BSPWM and Polybar configuration provided.
- Emacs installed by default.

# Operation

Create a directory and copy the files from here to that directory. 

```shell
mkdir ~/Projects/dev
cp -r * ~/Projects/dev
cp .* ~/Projects/dev 
```

Start `vagrant` to bootstrap the box.

```shell
cd ~/Projects/dev
vagrant up
```

Wait a bit, when the process completes (ideally without errors!!),
then login to the box and complete the boostrap by installing the
packages defined in the pacmanfile-vagrant.txt.

```shell
vagrant ssh
pacmanfile sync
```

Once that is complete, you may need to reboot the box.  Press `ctrl-d`
to exit the ssh session and run these commands to "reboot".

```shell
vagrant halt
vagrant up
```

All done! 

